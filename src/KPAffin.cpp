#include <KPAffin.hpp>


void TAfin(cv::Mat&mtaf, double fact)
{

    mtaf = cv::Mat::zeros(1,6,CV_64F);
    double *taf = mtaf.ptr<double>(0);

    std::vector< cv::Mat_<double> >M(3);
    for(int m=0;m<3; m++)
        M[m] = cv::Mat_<double>::zeros(2,2);

    cv::Mat_<double>A = cv::Mat_<double>::zeros(2,2);


    double ri=-fact*3.1415927f/18.0f,rf=fact*3.1415927f/18.0f; //�ngulo de rotaci�n
    double escxi = -fact*0.1f, escxf = fact*0.1f, escyi = -fact*0.1f, escyf = fact*0.1f; //escalamiento
    double cxi = -fact*0.1f,cxf = fact*0.1f, cyi = -fact*0.1f,cyf = fact*0.1f; //cizallamiento
    double ui = -fact*10, uf = fact*10, vi = -fact*10,vf = fact*10; //desplazamiento
    double r,escx,escy,cx,cy,u,v;

    int Ind[6][3], i; //Guarda todas las 6 permutaciones que se pueden realizar con los 3 �ndices de las matrices

    //Genera cada una de las 6 permutaciones de los 3 �ndices
    Ind[0][0] = 0; Ind[0][1] = 1; Ind[0][2] = 2;
    Ind[1][0] = 0; Ind[1][1] = 2; Ind[1][2] = 1;
    Ind[2][0] = 1; Ind[2][1] = 0; Ind[2][2] = 2;
    Ind[3][0] = 1; Ind[3][1] = 2; Ind[3][2] = 0;
    Ind[4][0] = 2; Ind[4][1] = 0; Ind[4][2] = 1;
    Ind[5][0] = 2; Ind[5][1] = 1; Ind[5][2] = 0;



    //genera �ngulos de rotaci�n, factores de escala, cizallamiento y traslaci�n aleatorios
    r = ri+(rf-ri)*double(rand())/double(RAND_MAX);
    escx = 1.0f+escxi+(escxf-escxi)*double(rand())/double(RAND_MAX);
    escy = 1.0f+escyi+(escyf-escyi)*double(rand())/double(RAND_MAX);
    cx = cxi+(cxf-cxi)*double(rand())/double(RAND_MAX);
    cy = cyi+(cyf-cyi)*double(rand())/double(RAND_MAX);
    u = ui+(uf-ui)*double(rand())/double(RAND_MAX);
    v = vi+(vf-vi)*double(rand())/double(RAND_MAX);

    //Genera la matriz de transformaci�n af�n

    //Matriz de rotaci�n
    M[0][0][0] = cos(r); M[0][0][1] = -sin(r);
    M[0][1][0] = sin(r); M[0][1][1] = cos(r);

    //Matriz de cizallamiento
    M[1][0][0] = 1+cx*cy; M[1][0][1] = cx;
    M[1][1][0] = cy; M[1][1][1] = 1;

    //Matriz de escalamiento
    M[2][0][0] = escx; M[2][0][1] = 0;
    M[2][1][0] = 0; M[2][1][1] = escy;

    //Multiplica aleatoriamente las 3 matrices para
    i = rand()%6;
    std::vector<int>order(Ind[i],Ind[i] + sizeof(Ind[i]) / sizeof(int));

    A=M[order[0]]*M[order[1]]*M[order[2]];


    taf[0] = A[0][0]; taf[1] = A[0][1];	taf[2] = u;
    taf[3] = A[1][0]; taf[4] = A[1][1];	taf[5] = v;
}

void GenerateRandomAffin(const std::string&filename,int samples,double interval_begin,double interval_end,
                         double steap_size)
{
    double fact			= 0.0;
    int steaps			= int ((interval_end-interval_begin+1)/steap_size);
    cv::Mat_<double>mtaf;
    std::string str_filename;
    for(int i=0; i<steaps; i++)
    {
        str_filename = filename+Value_To_String(i) + std::string(".txt");
        std::ofstream out(str_filename.c_str(),ios::out);
        out<<samples<<std::endl;
        fact = interval_begin + (i*steap_size);
        for(int s = 0; s<samples; s++)
        {

            TAfin(mtaf,fact);
            const double *ptr_mat = mtaf[0];
            out<<s	<<' '<<ptr_mat[0]<<' '<<ptr_mat[1]<<' '<<ptr_mat[2]<<' '<<ptr_mat[3]<<' '<<ptr_mat[4]
              <<' '	<<ptr_mat[5]<<std::endl;
        }
        out.close();
    }

}


cv::Mat AffinT(const double&theta,const double&alpha,const double&betha,
	       const double&gamma,const double&delta,const double&tx,
	       const double&ty)
{
    const double angle 	= theta;
    cv::Mat rotation		= cv::Mat::eye(2,2,CV_64F);
    cv::Mat scaling		= cv::Mat::eye(2,2,CV_64F);
    cv::Mat shearing		= cv::Mat::eye(2,2,CV_64F);

    //Set rotation matrix
    rotation.at<double>(0,0) = rotation.at<double>(1,1) = std::cos(angle);
    rotation.at<double>(0,1) = -std::sin(angle);
    rotation.at<double>(1,0) = std::sin(angle);

    //Scaling matrix
    scaling.at<double>(0,0)=alpha;
    scaling.at<double>(1,1)=betha;

    //Shearing matrix
    cv::Mat temp1 = cv::Mat::eye(2,2,CV_64F); temp1.at<double>(0,1)=gamma;
    cv::Mat temp2 = cv::Mat::eye(2,2,CV_64F); temp2.at<double>(1,0)=delta;
    shearing = temp1*temp2;

    cv::Mat temp3 	= ( (rotation*scaling)*shearing);
    cv::Mat result 	= cv::Mat::zeros(2,3,CV_64F);

    //First row of result
    double *ptr_result	= result.ptr<double>(0);
    ptr_result[0]       = temp3.at<double>(0,0); ptr_result[1] = temp3.at<double>(0,1); ptr_result[2] = tx;

    //Second row of result
    ptr_result		= result.ptr<double>(1);
    ptr_result[0]	= temp3.at<double>(1,0); ptr_result[1] = temp3.at<double>(1,1); ptr_result[2] = ty;

    return result;
}


/**
    Devuelve una matriz de transformaci�n affin aleatoria cuyos valores esten
        comprendidos en los intervalos pasados en params
        @param const double angle[2]: vector con el rango para el angulo
        @param const double scaling[2]: vector con el rango para la escala
        @param const double shearing[2]: vector con el rango para el shearing
        @param const double traslation[2]: vector con el rango para la traslacion
*/
cv::Mat RandomAffineT(const double angle[2],const double scaling[2],const double shearing[2],
const double traslation[2],std::mt19937&eng)
{
    std::uniform_real_distribution<> dist_angle(angle[0],angle[1]);

    const double angleg 	= dist_angle(eng);
    const double theta 		= DEGTORAD(angleg);

    std::uniform_real_distribution<> dist_scaling(scaling[0],scaling[1]);
    const double alpha	= dist_scaling(eng);
    const double betha	= dist_scaling(eng);

    std::uniform_real_distribution<> dist_shearing(shearing[0],shearing[1]);
    const double gamma	= dist_shearing(eng);
    const double delta	= dist_shearing(eng);

    std::uniform_real_distribution<> dist_traslation(traslation[0],traslation[1]);
    const double tx		= dist_traslation(eng);
    const double ty		= dist_traslation(eng);

    return AffinT(theta,alpha,betha,gamma,delta,tx,ty);
}


/**
    Aplica una transformacion afin
        @param const cv::Mat&src imagen original
        @param cv::Mat&dst imagen de destino, ser� del mismo tipo y tama�o de src
        @param const cv::Mat&taffin: transformacion afin a aplicar
        @param cv::Vec<double,2>xc: coordenadas del centro de la imagen por si se desea transformr respecto al centro
        @param const long SplineDegree: grado del coeficiente de interpolacion
*/
void ApplyAffinT(const cv::Mat&src,cv::Mat&dst,const cv::Mat&taffin,const cv::Vec2d&xc,const long SplineDegree,
                 bool inverse)
{

    CV_Assert(src.type()==CV_64F);

    CV_Assert(taffin.rows==1&&taffin.cols==6&&taffin.type()==CV_64F);

    int rows = src.rows;
    int cols = src.cols;

    cv::Mat temp;

    if(inverse)
    {
        temp = cv::Mat::zeros(1,6,CV_64F);
        cv::Mat inverse_t = taffin.reshape(1,2);
        cv::invertAffineTransform(inverse_t,temp);
        temp = temp.reshape(1,1);
    }
    else
        taffin.copyTo(temp);

    const double *ptr_affin = temp.ptr<double>(0);
    dst = cv::Mat::zeros(rows,cols,src.type());

    cv::Mat matInterpolation;
    SamplesToCoefficients(src,matInterpolation);


    for(int i=0; i<rows; i++)
    {
        double *ptr_row = dst.ptr<double>(i);
        for(int j=0; j<cols; j++)
        {
            double x = (i-xc[0])*ptr_affin[0] + (j-xc[1])*ptr_affin[1] + ptr_affin[2]+xc[0];
            double y = (i-xc[0])*ptr_affin[3] + (j-xc[1])*ptr_affin[4] + ptr_affin[5]+xc[1];

            if(x>=0&&x<rows&&y>=0&&y<cols)
            {
                double value = InterpolatedValue(matInterpolation,y,x,SplineDegree); //Interpolacion
                ptr_row[j] = value;
            }
        }
    }
}


/**=================================================================================================
@fn	double AffinMeanError(const cv::Size&size,const cv::Mat&taffin_real, const cv::Mat&taffin,
cv::Vec<double,2>xc)

@brief	Affin mean error. 

@author	Aslan
@date	01/07/2010

@param	size			The size. 
@param	taffin_real	The taffin real. 
@param	taffin		The taffin. 
@param					The. 

@return	error mean value. 
 *===============================================================================================**/
double AffinMeanError(const cv::Size&size,const cv::Mat&taffin_real, const cv::Mat&taffin,cv::Vec<double,2>xc)
{
    int rows = size.height;
    int cols = size.width;

    CV_Assert(taffin_real.rows==1 && taffin_real.cols==6 && taffin_real.type()==CV_64F&&taffin.rows==1
              && taffin.cols==6 && taffin.type()==CV_64F);

    const double *ptr_affin_real = taffin_real.ptr<double>(0);
    const double *ptr_affin 	 = taffin.ptr<double>(0);

    double temp	= 0.0;
    int count	= 0;
    for(int i=0; i<rows; i++)
        for(int j=0; j<cols; j++)
        {
            double x1 = (i-xc[0])*ptr_affin_real[0] + (j-xc[1])*ptr_affin_real[1] + ptr_affin_real[2]+xc[0];
            double y1 = (i-xc[0])*ptr_affin_real[3] + (j-xc[1])*ptr_affin_real[4] + ptr_affin_real[5]+xc[1];

            double x2 = (i-xc[0])*ptr_affin[0] + (j-xc[1])*ptr_affin[1] + ptr_affin[2]+xc[0];
            double y2 = (i-xc[0])*ptr_affin[3] + (j-xc[1])*ptr_affin[4] + ptr_affin[5]+xc[1];

            if(x1>=0&&x1<rows&&x2>=0&&x2<rows&&y1>=0&&y1<cols&&y2>=0&&y2<cols)
            {
                temp += std::sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
                count++;
            }
        }
    double result = (count>0)?(temp/count):(size.area());
    return result;
}

/**=================================================================================================
  @brief	Border error.
  
  @author	Aslan
  @date	03/07/2010
  
  @param	borders1			The first borders.
  @param	samples			The samples.
  @param	borders2			The second borders.
  @param	taffin			The taffin.
  @param	xc					The xc.
  @param [in,out]	count	Number of.
  
  @return	The border error.
   *===============================================================================================**/
double BorderError(const cv::Mat&borders1,const cv::Mat&samples,const cv::Mat&borders2,
                   const cv::Mat&taffin,const cv::Vec2d&xc,int a, int b,int&count)
{
    CV_Assert(borders1.type()== CV_64F  && borders2.type() == CV_64F);

    CV_Assert(taffin.rows==1 || taffin.cols==6 ||taffin.type() == CV_64F);

    CV_Assert(samples.cols==2&&samples.type()==CV_32S);

    int i           = samples.at<int>(0,0);
    int j           = samples.at<int>(0,1);
    double value    = borders1.at<double>(i,j);

    CV_Assert(value==255.0);

    //Invertimos la transformacion affin
    cv::Mat taffin_inverse;
    cv::Mat temp_affin = taffin.reshape(1,2);
    cv::invertAffineTransform(temp_affin,taffin_inverse);
    taffin_inverse = taffin_inverse.reshape(1,1);

    // taffin.copyTo(taffin_inverse);

    //Punteros necesarios
    const int    *ptr_samples  = nullptr;
    const double *ptr_affin    = taffin_inverse.ptr<double>(0);


    int rows = borders2.rows;
    int cols = borders2.cols;


    int s           = 0;
    int count2      = 0;
    double sum      = 0.0;
    double temp1    = 0.0;
    double temp2    = 0.0;
    double temp3    = 0.0;
    count           = 0;

    cv::Mat temp= cv::Mat::zeros(128,128,CV_64F);

    for(s=0; s<samples.rows;s++)
    {
        ptr_samples = samples.ptr<int>(s);
        i           = ptr_samples[0];
        j           = ptr_samples[1];

        double r = i - a;
        double c = j - b;

        double x = (r-xc[0])*ptr_affin[0] + (c-xc[1])*ptr_affin[1] + ptr_affin[2] + xc[0];
        double y = (r-xc[0])*ptr_affin[3] + (c-xc[1])*ptr_affin[4] + ptr_affin[5] + xc[1];

        /*x -= a;
                                y -= b;*/

        temp1   = 0.0;
        temp2   = 0.0;
        temp3   = 0.0;

        temp1   = borders1.at<double>(i,j)/255.0;

        if(x>=0&&x<rows&&y>=0&&y<cols)
        {
            count2++;
            //temp1   = borders1.at<double>(i,j)/255.0;
            //CV_Assert(temp1==1.0);
            x = static_cast<int>(std::ceil(x));
            y = static_cast<int>(std::ceil(y));

            if(x==rows) x--;
            if(y==cols) y--;

            temp2   = borders2.at<double>(x,y)/255.0;

            temp.at<double>(x,y) = 255.0;

            if(temp3==0.0)
                count++;

        }
        temp3   = (temp1-temp2);
        sum+= temp3*temp3;
    }
    double result = sum;
    //double result = (count2!=0)?(sum/samples.rows):1.0;//(count/(double)count2);
    
    return result;
}

//============================================================================================================
void RegisterAffine(KPAffin&kp,cv::Mat&taffin,const double lambda,const int maxiters,const METHODS method)
{
    Optimization optim(maxiters);
    optim.setLambda(lambda);

    taffin = optim.Minimize(kp,taffin,method);
}

cv::Mat RegisterAffine(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,const double&factor,
                       const int npixs,const double&lambda,const int&maxiters,const int&pyr_levels)
{

    KPAffin kp(100,false);
    int nbins   = static_cast<int>(factor);

    double epsdiff = 0.01;

    //Matriz para contener la transformacion
    cv::Mat taffin;

    //Inicializamos los parametros
    kp.setAllParameters(npixs,nbins,factor,SKP2,epsdiff,sigmas);

    //Inicializamos la transformacion
    taffin = kp.GetInicializeT().reshape(1,1);

    //Craemos los nieveles piramidales para las imagenes de intensidades
    std::vector<cv::Mat>pyr_original;
    std::vector<cv::Mat>pyr_transform;

    cv::buildPyramid(original,pyr_original,pyr_levels);
    cv::buildPyramid(transform,pyr_transform,pyr_levels);

    //Ciclo para optimizar en cada nivel piramidal
    int level = 0;
    double _lambda = lambda;
    for(level = (pyr_original.size()-1); level>=0; level--)
    {
        kp.Inicialize(pyr_original[level],pyr_transform[level]);
        RegisterAffine(kp,taffin,_lambda);
        _lambda /= 2;
    }
    return taffin;
}

//============================================================================================================
/*
        Evalua La funcion SKP en una muestra
*/
double KPAffin::KPFunction(const cv::Mat&taffin,const cv::Mat&samples)
{
    const int maxiters 	= samples.rows;
    const int msize		= samples.rows;
    int aux					= 0;

    const int *ptr_samples	= nullptr;


    int rt		= 0;
    int ct		= 0;
    double re 	= 0.0;
    double co	= 0.0;

    cv::Mat M 	= cv::Mat::zeros(msize,2,CV_32S);
    int *ptrM	= nullptr;

    for(int i=0; i<maxiters;i++)
    {
        ptr_samples	= samples.ptr<int>(i);

        //Obtiene el pixel
        rt	= ptr_samples[0];
        ct	= ptr_samples[1];

        CordAffin(rt,ct,taffin,re,co); //Sustituye a las lineas anteriores

        ptrM	= M.ptr<int>(i);

        //Si el pixel esta dentro de la region de traslape interpolamos y evaluamos
        if(rt==-1||ct==-1)
        {
            ptrM[0]			= int(_default_value);
            ptrM[1]			= int(_default_value);
        }
        else if((re>=0)&& (re<=(sizeR.height-1))&&(co>=0)&&(co<=(sizeR.width-1)))
        {
            int tmpint   	= matBins.at<int>(rt,ct);
            ptrM[0]			= tmpint;
            double tmp    	= InterpolatedValue(CoeffIp,co,re,_spline_degree); //Interpolacion
            ptrM[1]			= EstimateBin(tmp,_nbins,_max_intensity); //Estima el bin al que pertenece
        }
        else //Pixeles fuera del �rea de traslape
        {
            aux++;
            int tmpint   	= matBins.at<int>(rt,ct);
            ptrM[0]			= tmpint;
            ptrM[1]			= EstimateBin(_default_value,_nbins,_max_intensity);
        }
    }

    _correct = 1.0/( (2.0*double(_npixs)) - double(aux) + 1.0);
    _inliers = (2*_npixs) - aux;

    //Si no se encontro ninguna muestar trasnformacion
    /*if(aux >= _npixs)
                return 1.0;*/

    //Evaluamos los Kernels de las marginales y las conjuntas
    cv::Mat kernels	= cv::Mat::zeros(1,3,CV_64F);

    //Seleccionamos el estimador
    int estimator = getEstimatorType();
    if(estimator == 1)		{EvalKernels(M,kernels);}
    else if (estimator == 2)	{EvalKernels2(M,kernels);}
    else if (estimator == 3)	{EvalKernels3(M,kernels);}
    else				{throw KP::Error("Estimator dont exist",__FUNCTION__);}


    //Aplicamos la f�rmula de KPs
    double result=0.0;
    switch (_type)
    {
        case 1:
            result = KPNormalized(kernels); //Art�culo original
            break;
        case 2:
            result = KPadd(kernels); //Nueva propuesta
            break;
        default:
            throw KP::Error("KP function dont exist",__FUNCTION__);
            break;
    }
    return result;

}


int KPAffin::takeSamplesGradient(std::vector<cv::Mat>&M,const cv::Mat&_Taffin)
{
    if(_Taffin.rows != 1 || (_Taffin.cols !=(int) _parameters))
        throw KP::Error("Tranformation invalid size",__FUNCTION__);
    const double *tafin = _Taffin.ptr<double>(0);

    //Size of array,matrix and iterations
    const int msize     = 2*_npixs;
    const int arrsize   = 2*_parameters;
    const int maxiters  = 10*_npixs;

    //Inicialize M
    M.clear();
    M.resize(arrsize);
    for(unsigned int i=0; i<M.size();i++)
        M[i]=cv::Mat::zeros(msize,2,CV_32S);

    cv::Mat matre(1,arrsize,CV_64F);
    cv::Mat matco(1,arrsize,CV_64F);
    double *re	= matre.ptr<double>(0);
    double *co	= matco.ptr<double>(0);

    int rt, ct, cont = 0, it = 0;
    double r, c, rxc, cxc, rer, cor;
    int ntr;
    int t;

    //number of points found it
    int found = 0;

    int C = _rect.x + _rect.width - 1;
    std::uniform_int_distribution<> samples_cols(_rect.x, C);

    int R = _rect.y + _rect.height - 1;
    std::uniform_int_distribution<> samples_rows(_rect.y, R);

    while( (cont<msize) && (!(it>maxiters)) )
    {
        //Pesca un pixel
        rt = samples_rows(kp_gen);
        ct = samples_cols(kp_gen);


        //Transforma a coordenadas de la imagen de referencia
        r = rt + _a;
        c = ct + _b;

        rxc = (r-_xc[0]);
        cxc = (c-_xc[1]);

        ntr = 0;

        //Transforma las coordenadas del pixel muestreado
        rer = rxc*tafin[0]+cxc*tafin[1]+tafin[2] + _xc[0] ;
        cor = rxc*tafin[3]+cxc*tafin[4]+tafin[5] + _xc[1];

        rxc *= _epsdiff;
        cxc *= _epsdiff;

        //Checa que est� en la intersecci�n de las 12 transformaciones
        for(t = 0; t < static_cast<int>(_parameters); t++)
        {
            switch(t)
            {
                case 0 :
                    {
                        re[0] = rer-rxc;
                        co[0] = cor;
                        re[1] = rer+rxc;
                        co[1] = cor;
                        break;
                    }
                case 1 :
                    {
                        re[2] = rer-cxc;
                        co[2] = cor;
                        re[3] = rer+cxc;
                        co[3] = cor;
                        break;
                    }

                    //El vector de traslaci�n se explora un poco m�s
                case 2 :
                    {
                        re[4] = rer-100 * _epsdiff;
                        co[4] = cor;
                        re[5] = rer+100 * _epsdiff;
                        co[5] = cor;
                        break;
                    }
                case 3 :
                    {
                        re[6] = rer;
                        co[6] = cor-rxc;
                        re[7] = rer;
                        co[7] = cor+rxc;
                        break;
                    }
                case 4 :
                    {
                        re[8] = rer;
                        co[8] = cor-cxc;
                        re[9] = rer;
                        co[9] = cor+cxc;
                        break;
                    }

                case 5 :
                    {
                        re[10] = rer;
                        co[10] = cor-100 * _epsdiff;
                        re[11] = rer;
                        co[11] = cor+100 * _epsdiff;
                        break;
                    }
            }
        }

        for(t = 0; t < arrsize; t++)
        {
            //Verifica que se encuentre en el traslape
            if( (re[t]>=0)&&(re[t]<=(sizeR.height-1))&&(co[t]>=0)&&(co[t]<=(sizeR.width-1)))
                ntr ++;
        }
        //Si la muestra se encuentra en la intersecci�n de las 12 transformaciones, se acepta
        if(ntr==arrsize)
        {
            found++;
            int tmpint		= matBins.at<int>(rt,ct);

            /*ptr_samples		= msamples.ptr<int>(cont);
                                        ptr_samples[0] = rt;
                                ptr_samples[1] = ct;*/

            int *ptrMint = nullptr;
            for(t = 0; t < arrsize; t++)
            {
                ptrMint			= M[t].ptr<int>(cont);
                ptrMint[0]		= tmpint;
                double tmp		= InterpolatedValue(CoeffIp,co[t],re[t],_spline_degree);
                ptrMint[1]		= EstimateBin(tmp,_nbins,_max_intensity);
            }
            cont++;
        }
        it++;
    }
    return found;
}

/* Toma muestras que cumplan con la transformacion
	@param cv::Mat&samples: matriz que contendr� las muestras
	@param const cv::Mat&taffin: transformacion afin
*/
int KPAffin::takeSamplesT(cv::Mat&samples,const cv::Mat&taffin,const int nsamples)
{

    int msize   = 2*_npixs;
    if(nsamples!=0)
    {
        CV_Assert(nsamples>2*_npixs);
        msize = nsamples;
    }

    samples 				= cv::Mat::zeros(msize,2,CV_32S);
    int *ptr_samples	= nullptr;

    int rt, ct;

    int C = _rect.x + _rect.width - 1;
    std::uniform_int_distribution<> samples_cols(_rect.x, C);

    int R = _rect.y + _rect.height - 1;
    std::uniform_int_distribution<> samples_rows(_rect.y, R);

    double re		= 0.0;
    double co		= 0.0;
    int counter 	= 0;
    int maxiters	= 5*msize;

    for(int i=0; i<maxiters;i++)
    {

        if(counter==msize) break;

        ptr_samples	= samples.ptr<int>(counter);

        //Pesca un pixel
        rt = samples_rows(kp_gen);
        ct = samples_cols(kp_gen);


        CordAffin(rt,ct,taffin,re,co);

        if( (re>=0)&&(re<=(sizeR.height-1))&&(co>=0)&&(co<=(sizeR.width-1)))
        {
            ptr_samples[0] = rt;
            ptr_samples[1] = ct;
            temp.at<int>(rt+_a,ct+_b)=255;
            counter++;
        }
    }
    return counter;
}

/** Toma muestras unicamente pixeles que cumplan con la transformacion dentro de los bordes.
	@param cv::Mat&samples: matriz que contendra las muestras
	@param const cv::Mat&border_samples: matriz de pixeles se�alados como bordes
	@param const cv::Mat&taffin: transformacion afin
*/
void KPAffin::takeSamplesTaffin(cv::Mat&samples,const cv::Mat&border_samples,const cv::Mat&taffin,
				bool verbose)
{

    const int msize = 2*_npixs;

    if(border_samples.rows<2*_npixs)
        throw KP::Error("Sin suficientes muestras en los bordes",__FUNCTION__);


    samples 				= cv::Mat::zeros(msize,2,CV_32S);
    int *ptr_samples	= nullptr;

    //Apuntador a la matriz de bordes
    const int *ptr_border_samples = nullptr;

    std::uniform_int_distribution<> samplesfborders(0, (border_samples.rows-1));

    const int iters = 5*_npixs;
    int counter = 0;
    for(int i=0; i<iters;i++)
    {

        if(counter==msize) break;

        ptr_samples	= samples.ptr<int>(counter);

        //Pesca un pixel
        int sample_row = samplesfborders(kp_gen);
        ptr_border_samples = border_samples.ptr<int>(sample_row);

        int rt = ptr_border_samples[0];
        int ct	= ptr_border_samples[1];

        double re = 0.0;
        double co = 0.0;

        CordAffin(rt,ct,taffin,re,co);

        if( (re>=0)&&(re<=(sizeR.height-1))&&(co>=0)&&(co<=(sizeR.width-1)))
        {
            ptr_samples[0] = rt;
            ptr_samples[1] = ct;
            temp.at<int>(rt,ct)=255;
            counter++;
        }
    }
    if(verbose)
        cv::imwrite("Muestras.jpg",temp);
}

int KPAffin::takeSamplesBetweenT(cv::Mat&samples,const cv::Mat&taffin1,const cv::Mat&taffin2)
{
    const int msize     = 2*_npixs;

    //samples 				= cv::Mat(msize,2,CV_32S,cv::Scalar(-1));;
    samples = cv::Mat::zeros(msize,2,CV_32S);
    int *ptr_samples	= nullptr;

    cv::Mat matre(1,2,CV_64F);
    cv::Mat matco(1,2,CV_64F);
    double *re	= matre.ptr<double>(0);
    double *co	= matco.ptr<double>(0);

    int C = _rect.x + _rect.width - 1;
    std::uniform_int_distribution<> samples_cols(_rect.x, C);

    int R = _rect.y + _rect.height - 1;
    std::uniform_int_distribution<> samples_rows(_rect.y, R);

    const int iters = 10*_npixs;
    int counter = 0;
    int ntr 		= 0;
    for(int i=0; i<iters;i++)
    {

        if(counter==msize) break;

        ptr_samples	= samples.ptr<int>(counter);

        int rt = samples_rows(kp_gen);
        int ct = samples_cols(kp_gen);

        CordAffin(rt,ct,taffin1,re[0],co[0]);
        CordAffin(rt,ct,taffin2,re[1],co[1]);

        ntr = 0;
        for(int t = 0; t < 2; t++)
        {
            //Verifica que se encuentre en el traslape
            if( (re[t]>=0)&&(re[t]<=(sizeR.height-1))&&(co[t]>=0)&&(co[t]<=(sizeR.width-1)))
                ntr ++;
        }

        if( ntr == 2)
        {
            ptr_samples[0] = rt;
            ptr_samples[1] = ct;
            temp.at<int>(rt,ct)=255;
            counter++;
        }
    }
    if(_verbose)
    {
        cv::imwrite("Muestras.jpg",temp);
    }
    return counter;
}

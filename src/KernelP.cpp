#include <KernelP.hpp>

KernelP::KernelP(const unsigned long seed,const bool verbose)
{
    kp_gen.seed(seed);
    setVerbose(verbose);
    setPixs(0);
    setBins(0);
    setImax(255);
    setEpsDiff(1.0e-2);
    setSigmas(cv::Vec2d(0.0,0.0));
    setDefaultValue(0.0);
    setSplineDegree(3);
    setType(1);
    setRect(cv::Rect(0,0,0,0));
    setCentralCoor(cv::Vec2d(0.0,0.0));
    _a = _b = 0;
}


KernelP::~KernelP(void)
{
}


void KernelP::setAllParameters(const int npixs,const int nbins,const double factor,const int type,const double epsdiff,
                               const cv::Vec2d&sigmas,const int estimator,const long splindegree)
{
    setPixs(npixs);
    setBins(nbins);
    setImax(factor);
    setType(type);
    setEpsDiff(epsdiff);
    setSigmas(sigmas);
    setEstimatorType(estimator);
    setSplineDegree(splindegree);

    ValidateParameters();
}


/*
        Lee los parametros de KP's de un archivo
*/
void KernelP::ReadParametersfromFile(const std::string&filenames)
{
    std::ifstream in(filenames.c_str(),ios::in);
    if(!in.is_open()) throw KP::Error ("No pudo abrirse el archivo",__FUNCTION__);

    std::string param;
    int count	= 0;

    in>>param;
    if(param == "KP_Parameters")
    {
        in>>param; if(param=="npixs") {in>>_npixs; count++;}
        in>>param; if(param=="nbins") {in>>_nbins; count++;}
        in>>param; if(param=="splin") {in>>_spline_degree; count++;}
        in>>param; if(param=="Imax")  {in>>_max_intensity; count++;}
        in>>param; if(param=="type")  {in>>_type; count++;}
        in>>param; if(param=="eps")   {in>>_epsdiff; count++;}
        in>>param; if(param=="sigmas") {in>>_sigmam>>_sigmac; count++;}
        in>>param;
        if(param=="rect")
        {
            in>>_rect.x;
            in>>_rect.y;
            in>>_rect.width;
            in>>_rect.height;
            count++;
        }
    }
    else
    {throw KP::Error("Los parametros de KP necesarios no fueron especificados",__FUNCTION__);}

    //Si no se leyeron todos los parametros de KP lanzamos un error
    if(count!= 8) throw KP::Error("No se leyeron todos los parametros de KP",__FUNCTION__);

    in.close();
    setNumberofParameters();
}

/*
	Verifica que los parametros de KP's sean correctos
	antes de realizar cualquier operacion de calculo de
	la funci�n o estimaci�n del gradiente
*/
void KernelP::ValidateParameters()
{
    if(_max_intensity == 0.0 )
        throw KP::Error("Max intensity must be different from zero",__FUNCTION__);

    if(_epsdiff == 0.0 )
        throw KP::Error("EPSDIFF must be different from zero",__FUNCTION__);

    if(_spline_degree <= 0) _spline_degree = 3;

    if(_type <= 0) _type = 1;

    if(_nbins <= 0 ) _nbins = static_cast<int>(_max_intensity);

    //Asignamos el valor por default
    _default_value = EstimateBin(0.0,_nbins,_max_intensity);

    //Numero de parametros
    setNumberofParameters();
}

void KernelP::Inicialize(	const cv::Mat&img_original,const cv::Mat&img_transform, const int npixs,
				const int nbins,const double Imax,const double epsdiff, const cv::Vec2d&sigmas,
				const unsigned int spline_degree,const unsigned int type,const cv::Rect&rect,
				const cv::Vec2d&xc)
{
    //Asignamos los parametros
    setPixs(npixs);
    setBins(nbins);
    setImax(Imax);
    setEpsDiff(epsdiff);
    setSigmas(sigmas);
    setSplineDegree(spline_degree);
    setType(type);
    setRect(rect);
    setCentralCoor(xc);
    setEstimatorType(1);
    Inicialize(img_original,img_transform);
}

/*

*/
void KernelP::Inicialize(const cv::Mat&img_original,const cv::Mat&img_transform)
{
    //Set the images sizes
    sizeR = img_original.size();
    sizeT = img_transform.size();

    //Region de interes del muestreo
    _rect = cv::Rect(0,0,sizeT.width,sizeT.height);

    if( _npixs > (sizeR.width*sizeR.height))
        _npixs = (sizeR.width*sizeR.height);

    if( ((_rect.x+_rect.width)>sizeR.width) || ( (_rect.y+_rect.height)>sizeR.height) )
        throw KP::Error("Mismatch image size",__FUNCTION__);

    //Si las imagenes tienen diferente tama�o
    _a=_b=0;
    if(sizeR!=sizeT)
    {
        _a = (img_original.size() - img_transform.size()).height;
        _b = (img_original.size() - img_transform.size()).width;
        _a/=2;
        _b/=2;
    }

    //Estimamos los centros de las imagenes
    _xc = cv::Vec2d(img_original.rows/2.0,img_original.cols/2.0);

    //Validate parameters
    ValidateParameters();

    //Llena los kernels
    FillKernels(KM,KC,_sigmam,_sigmac,_nbins,_max_intensity,_verbose);
    SamplesToCoefficients(img_original,CoeffIp);
    FillBinsMatrix(img_transform,matBins,_nbins,_max_intensity);

    //Matrices temporales de observacion
    temp = cv::Mat(img_original.size(),CV_32S,cv::Scalar(0));

}

/* Llena las matrices KM y KC
	en base a los parametros _nbins,_Imax,
	_sigmam,_sigmac
*/
void KernelP::FillKernels(cv::Mat&km,cv::Mat&kc,const double&sigmam,const double&sigmac,const int&nbins,
                          const double factor,const bool verbose)
{
    if( (sigmam == 0.0) || (sigmac == 0.0) )
        throw KP::Error("Sigmas must be different to zero",__FUNCTION__);

    int difx=0, dify=0;
    double fact[2]={1.0,1.0};
    double x=0., x2=0., y=0., y2=0.;

    km = cv::Mat::zeros(1,nbins,CV_64F);
    kc = cv::Mat::zeros(nbins,nbins,CV_64F);

    double tmp1 = 2.0*sigmam*sigmam;
    double tmp2 = 2.0*sigmac*sigmac;

    //Apuntador a la primera fila de KM para usarlo como vector
    double *ptrKM = km.ptr<double>(0);

    for(difx = 0; difx < nbins; difx++)
    {
        x	= factor*difx/double(nbins-1);
        x2	= x*x;

        ptrKM[difx] = fact[0]*exp(-x2/(tmp1));

        //Apuntador para el segundo kernel
        double *ptrKC = kc.ptr<double>(difx);

        for(dify = 0; dify < nbins; dify++)
        {
            y = factor*dify/double(nbins-1);
            y2 = y*y;
            ptrKC[dify] = fact[1]*exp(-(x2+y2)/(tmp2));
        }
    }
    if(verbose)
    {
        std::string KM_Name =  std::string("KM_")+Value_To_String(_sigmam)+std::string(".txt");
        std::string KC_Name =  std::string("KC_")+Value_To_String(_sigmac)+std::string(".txt");
        cv::WriteinFile<double>(km,KM_Name);
        cv::WriteinFile<double>(kc,KC_Name);
    }
}

/* Retorna una matriz que contiene los bins de la distribucion propuesta
	@param const cv::Mat&src: imagen original
	@param[out] cv::Mat& image_bins: imagen que contiene los bins de las intensidades
*/
void KernelP::FillBinsMatrix(const cv::Mat&src, cv::Mat& image_bins,const int&nbins,const double&factor)
{
    int cols = src.cols, rows = src.rows;
    int i       = 0;
    int j       = 0;
    image_bins  = cv::Mat::zeros(rows,cols,CV_32S);

    if(src.isContinuous() && image_bins.isContinuous())
    {
        cols *= rows;
        rows = 1;
    }
    for(i = 0; i < rows; i++)
    {
        int             *ptrIbins       = image_bins.ptr<int>(i);
        const double    *ptrimage       = src.ptr<double>(i);
        for(j = 0; j < cols; j++)
            ptrIbins[j] = EstimateBin(ptrimage[j],nbins,factor);
    }
}

/*
    Calcula las evaluaciones de los kerneles
    @param[in] const cv::Mat&M: matriz de enteros conteniendo las muestras
    @param[out] cv::Mat&pred: matriz de doubles con los valores de salida
*/
void KernelP::EvalKernels(const cv::Mat&M,cv::Mat&pred)
{
    if(M.rows < (2*_npixs))
        throw KP::Error("Mismatch M size",__FUNCTION__);

    int i, j;
    int dist[2]={0,0};

    double       *ptrPred = pred.ptr<double>(0);
    const double *ptrKM   = KM.ptr<double>(0);

    //Apuntadores a las filas de M
    const int *ptrMi   = nullptr;
    const int *ptrMj   = nullptr;

    ptrPred[0] = 0.0;
    ptrPred[1] = 0.0;
    ptrPred[2] = 0.0;

    int samplessize = 2*_npixs;
    for(i = 0; i < _npixs; i++)
        for(j = _npixs; j < samplessize; j++)
        {
            //Asigamos los punteros
            ptrMi       = M.ptr<int>(i);
            ptrMj       = M.ptr<int>(j);

            //Calcula la distancia entre las muestras
            dist[0]     = std::abs(ptrMi[0]-ptrMj[0]);
            dist[1]     = std::abs(ptrMi[1]-ptrMj[1]);

            ptrPred[0]  += ptrKM[dist[0]];
            ptrPred[1]  += ptrKM[dist[1]];
            ptrPred[2]  += KC.at<double>(dist[0],dist[1]);
        }
}


/*
    Calcula las evaluaciones de los kerneles
    @param[in] const cv::Mat&M: matriz de enteros conteniendo las muestras
    @param[out] cv::Mat&pred: matriz de doubles con los valores de salida
*/
void KernelP::EvalKernels2(const cv::Mat&M,cv::Mat&pred)
{
    if(M.rows < (2*_npixs))
        throw KP::Error("Mismatch M size",__FUNCTION__);

    int samples = 2*_npixs;

    int i, j;
    int dist[2]={0,0};

    double       *ptrPred = pred.ptr<double>(0);
    const double *ptrKM   = KM.ptr<double>(0);

    //Apuntadores a las filas de M
    const int *ptrMi   = nullptr;
    const int *ptrMj   = nullptr;

    ptrPred[0] = 0.0;
    ptrPred[1] = 0.0;
    ptrPred[2] = 0.0;

    for(i = 0; i < samples; i++)
        for(j = 0; j < samples; j++)
        {
            //Asigamos los punteros
            ptrMi       = M.ptr<int>(i);
            ptrMj       = M.ptr<int>(j);

            //Calcula la distancia entre las muestras
            dist[0]     = std::abs(ptrMi[0]-ptrMj[0]);
            dist[1]     = std::abs(ptrMi[1]-ptrMj[1]);

            ptrPred[0]  += ptrKM[dist[0]];
            ptrPred[1]  += ptrKM[dist[1]];
            ptrPred[2]  += KC.at<double>(dist[0],dist[1]);
        }
}

/*
    Calcula las evaluaciones de los kerneles
    @param[in] const cv::Mat&M: matriz de enteros conteniendo las muestras
    @param[out] cv::Mat&pred: matriz de doubles con los valores de salida
*/
void KernelP::EvalKernels3(const cv::Mat&M,cv::Mat&pred)
{
    if(M.rows < (2*_npixs))
        throw KP::Error("Mismatch M size",__FUNCTION__);

    int samples = 2*_npixs;

    int i, j;
    int dist[2]={0,0};

    double       *ptrPred = pred.ptr<double>(0);
    const double *ptrKM   = KM.ptr<double>(0);

    //Apuntadores a las filas de M
    const int *ptrMi   = nullptr;
    const int *ptrMj   = nullptr;

    ptrPred[0] = 0.0;
    ptrPred[1] = 0.0;
    ptrPred[2] = 0.0;

    for(i = 0; i < samples-1; i++)
        for(j = i+1; j < samples; j++)
        {
            //Asigamos los punteros
            ptrMi       = M.ptr<int>(i);
            ptrMj       = M.ptr<int>(j);

            //Calcula la distancia entre las muestras
            dist[0]     = std::abs(ptrMi[0]-ptrMj[0]);
            dist[1]     = std::abs(ptrMi[1]-ptrMj[1]);

            ptrPred[0]  += ptrKM[dist[0]];
            ptrPred[1]  += ptrKM[dist[1]];
            ptrPred[2]  += KC.at<double>(dist[0],dist[1]);
        }
}


/* Estima el gradiente en base a las muestras que se intersectan en las transformaciones
	@param[in] std::vector<cv::Mat>&M: conjunto de bins de las intersecciones
	@return estimacion del gradiente
*/
cv::Mat KernelP::EstimateGradient(std::vector<cv::Mat>&M)
{

    if( (M.size() != (2*_parameters)) || (M[0].rows != 2*_npixs) )
        throw KP::Error("Mismatch M size",__FUNCTION__);

    int i 					= 0;
    cv::Mat predad		= cv::Mat::zeros(1,3,CV_64F);
    cv::Mat predat		= cv::Mat::zeros(1,3,CV_64F);
    cv::Mat gradient		= cv::Mat::zeros(1,_parameters,CV_64F);
    double simat			= 0.0;
    double simad			= 0.0;

    //Aproxima las derivadas direccionales mediante diferencias centrales
    double *grad         = gradient.ptr<double>(0);

    for(i = 0; i < _parameters; i++)
    {
        EvalKernels(M[2*i],predat);
        EvalKernels(M[2*i+1],predad);

        switch (_type)
        {
            case 1:
                simad	= KPNormalized(predad);
                simat	= KPNormalized(predat);
                break;
            case 2:
                simad	= KPadd(predad);
                simat	= KPadd(predat);
                break;
            default:
                throw KP::Error("KP function dont exist",__FUNCTION__);
                break;
        }
        grad[i] =  (simad-simat)/(2.0*_epsdiff);
    }
    return gradient;
}

/* Obtiene un conjunto de muestras para evaluar KP
*/
void KernelP::takeSamples(cv::Mat&samples,const int nsamples)
{
    int msize   = 2*_npixs;

    if(nsamples!=0)
    {
        if(nsamples>2*_npixs)
            msize = nsamples;
    }

    samples 			= cv::Mat::zeros(msize,2,CV_32S);
    int *ptr_samples	= nullptr;

    int rt, ct;

    int C = _rect.x + _rect.width - 1;
    std::uniform_int_distribution<> samples_cols(_rect.x, C);

    int R = _rect.y + _rect.height - 1;
    std::uniform_int_distribution<> samples_rows(_rect.y, R);


    for(int i=0; i<msize;i++)
    {
        ptr_samples	= samples.ptr<int>(i);

        //Pesca un pixel
        rt = samples_rows(kp_gen);
        ct = samples_cols(kp_gen);

        ptr_samples[0] = rt;
        ptr_samples[1] = ct;
        temp.at<int>(rt+_a,ct+_b)=255;
    }
    if(_verbose)
    {
        cv::imwrite("Muestras.jpg",temp);
    }
}

/**
 * @brief KernelP::takeSamples
 * @param samples
 * @param border_samples
 */
void KernelP::takeSamples(cv::Mat&samples,const cv::Mat&border_samples)
{

    if(border_samples.rows<2*_npixs)
        throw KP::Error("Sin suficientes muestras en los bordes",__FUNCTION__);

    const int msize		= 2*_npixs;
    samples 					= cv::Mat::zeros(msize,2,CV_32S);
    int *ptr_samples		= nullptr;

    //Apuntador a la matriz de bordes
    const int *ptr_border_samples = nullptr;

    std::uniform_int_distribution<> samplesfborders(0, (border_samples.rows-1));

    for(int i=0; i<msize;i++)
    {
        ptr_samples	= samples.ptr<int>(i);

        //Pesca un pixel
        int row = samplesfborders(kp_gen);
        ptr_border_samples = border_samples.ptr<int>(row);

        ptr_samples[0] = ptr_border_samples[0];
        ptr_samples[1] = ptr_border_samples[1];
        temp.at<int>(ptr_samples[0]+_a,ptr_samples[1]+_b)=255;
    }

    if(_verbose)
    {
        cv::imwrite("Muestras.jpg",temp);
    }
}

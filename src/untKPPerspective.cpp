
#include <untKPPerspective.hpp>

void ApplyPerspectiveTransform(const cv::Mat&src,cv::Mat&dst,const cv::Mat&perspective_transformation,
                               const long spline_degree,const bool inverse)
{
  if(src.type() != CV_64F)
    throw KP::Error("Mistmatch type",__FUNCTION__);

  cv::Mat transform;
  if( (perspective_transformation.rows==3)&&(perspective_transformation.cols==3) )
    {
      transform = perspective_transformation.reshape(1,1);
    }
  else if((perspective_transformation.rows==1)&&(perspective_transformation.cols==9))
    {
      transform = perspective_transformation;
    }
  else
    {
      throw KP::Error("Mistmatch perspective transform size",__FUNCTION__);
    }

  if(inverse)
    {
      cv::Mat temp = transform.reshape(1,3);
      cv::invert(temp,transform);
      transform = transform.reshape(1,1);
    }

  cv::Vec2d xc(src.rows/2,src.cols/2);

  int rows = src.rows;
  int cols = src.cols;

  dst = cv::Mat::zeros(rows,cols,CV_64F);

  //Apuntadores necesarios
  double *ptr_dst = nullptr;

  //Apuntador a la transformacion
  const double *ptr_p	= perspective_transformation.ptr<double>(0);
  double x = 0.0;
  double y = 0.0;

  //Matriz para la interpolacion
  cv::Mat matInterpolation;
  SamplesToCoefficients(src,matInterpolation);

  for(int r=0; r<rows; r++)
    {
      ptr_dst = dst.ptr<double>(r);
      for(int c=0; c<cols; c++)
        {
          //Aplicamos la transformaci�n
          double den	= ((r-xc[0])*ptr_p[7]) + ((c-xc[1])*ptr_p[6])  + ptr_p[8];
          y 				= ((r-xc[0])*ptr_p[4]) + (((c-xc[1])*ptr_p[3]) + ptr_p[5])/den + xc[0];
          x 				= ((r-xc[0])*ptr_p[1]) + (((c-xc[1])*ptr_p[0]) + ptr_p[2])/den + xc[1];

          if(y>=0&&y<rows&&x>=0&&x<cols)
            {
              //Interpolacion
              double value = InterpolatedValue(matInterpolation,x,y,spline_degree);
              ptr_dst[c] = value;
            }
        }
    }

}

double PerspectiveMeanError(const cv::Size&size,const cv::Mat&perspective1,const cv::Mat&perspective2)
{

  if( (perspective1.rows !=1) || (perspective2.rows!=1))
    throw KP::Error("Mistmatch size transformation",__FUNCTION__);

  if( (perspective1.cols != 9) || (perspective2.cols!=9))
    throw KP::Error("Mistmatch size transformation",__FUNCTION__);

  int rows = size.height;
  int cols = size.width;

  //Valores de la transformacion
  double x1 = 0.0;
  double y1 = 0.0;
  double x2 = 0.0;
  double y2 = 0.0;

  double temp = 0.0;
  int inliers = 0;

  for(int r=0; r<rows; r++)
    {
      for(int c=0; c<cols; c++)
        {
          //Aplicamos la transformaci�n
          TransformCoordenatesPerspective(r,c,perspective1,x1,y1);
          TransformCoordenatesPerspective(r,c,perspective2,x2,y2);

          if(y1>=0&&y1<rows&&x1>=0&&x1<cols&&y2>=0&&y2<rows&&x2>=0&&x2<cols)
            {
              temp += std::sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
              inliers++;
            }
        }
    }
  double result = (inliers>0)?(temp/inliers):(size.area());
  return result;
}


double FindPerspectiveTransform(const cv::Mat&src,const cv::Mat&transform,cv::Mat&tperspective,
				const int type,const int npixs,const cv::Vec2d&sigmas,
				const int method,const int estimator)
{

  //Creamos la clase KPPerspective
  unsigned seed = 100;
  KPPerspective kp(seed,true);

  //Convertimos las imagenes a formato de double
  cv::Mat_<double> src_tmp(src);
  cv::Mat_<double> transform_tmp(transform);

  //Suavizamos las imagenes para eliminar ruido
  cv::GaussianBlur(src_tmp,src_tmp,cv::Size(3,3),0.5);
  cv::GaussianBlur(transform_tmp,transform_tmp,cv::Size(3,3),0.5);

  //Normalizamos los rangos de las imagenes
  double image_min,image_max;
  double image_t_min,image_t_max;

  cv::minMaxLoc(src_tmp,&image_min,&image_max);
  cv::minMaxLoc(transform_tmp,&image_t_min,&image_t_max);

  double Imax =  100;std::max(image_max,image_t_max);
  int nbins	=  static_cast<int>(Imax);

  src_tmp 			= cv::NormalizeRange<double,double>(src_tmp,0.0,Imax);
  transform_tmp	= cv::NormalizeRange<double,double>(transform_tmp,0.0,Imax);

  //Construimos las piramides para la imagen original y transformada
  int pyr_levels = 5;

  std::vector<cv::Mat>pyr_image;
  cv::buildPyramid(src_tmp,pyr_image,pyr_levels);

  std::vector<cv::Mat>pyr_image_t;
  cv::buildPyramid(transform_tmp,pyr_image_t,pyr_levels);

  //Variables necesarias
  int level							= 0;

  //Matrices necesarias
  cv::Mat samples; //matriz para muestras
  cv::Mat perspective_new;
  cv::Mat perspective_old = kp.GetInicializeT();

  //Configuramos skp
  kp.setImax(Imax);
  kp.setBins(nbins);
  kp.setPixs(npixs);
  kp.setType(type);
  kp.setEstimatorType(estimator);
  kp.setDefaultValue(Imax);
  kp.setSigmas(sigmas);

  double l		= -0.1;
  double eps	= 0.01;
  int ITMAX	= 300;

  double *ptr_result	= perspective_old.ptr<double>(0);
  for(level = (pyr_image.size()-1); level>=0; level--)
    {
      kp.ClearRect();
      cv::Vec2d xci(pyr_image[level].rows/2.0,pyr_image[level].cols/2.0);
      kp.setCentralCoor(xci);
      kp.setEpsDiff(eps);
      if(level!=0)
        {
          kp.Inicialize(pyr_image[level],pyr_image_t[level]);
        }
      else
        {
          kp.Inicialize(src_tmp,transform_tmp);
        }

      for(int iter=0; iter<ITMAX; iter++)
        {
          cv::Mat gradient_t;
          int samples = kp.df(perspective_old,gradient_t);
          double *ptr_gradient = gradient_t.ptr<double>(0);

          if(samples == 0) continue;

          if(iter == ITMAX-50)
            l /= 10.0;

          for(int i = 0; i < 8; i++)
            {
              if(i > 5)
                {
                  ptr_result[i] += 0.001*l*ptr_gradient[i];
                }
              else
                {
                  ptr_result[i] += l*ptr_gradient[i];
                }
            }
        }

      if(level>0)
        {
          ptr_result[4] *= 2.0;
          ptr_result[5] *= 2.0;
        }

      l		/= 2.0;
      eps	/= 2.0;

    }
  //ConvertTPerspective2CVFormat(perspective_old,tperspective);
  return 0;
}

double KPPerspective::KPFunction(const cv::Mat&tperspective,const cv::Mat&samples)
{
  const int maxiters 	= samples.rows;
  const int msize		= samples.rows;
  int aux					= 0;

  const int *ptr_samples	= nullptr;


  int rt		= 0;
  int ct		= 0;
  double x 	= 0.0;
  double y		= 0.0;

  cv::Mat M 	= cv::Mat::zeros(msize,2,CV_32S);
  int *ptrM	= nullptr;

  for(int i=0; i<maxiters;i++)
    {
      ptr_samples	= samples.ptr<int>(i);

      //Obtiene el pixel
      rt	= ptr_samples[0];
      ct	= ptr_samples[1];

      CordPerspective(rt,ct,tperspective,x,y); //Sustituye a las lineas anteriores

      ptrM	= M.ptr<int>(i);

      //Si el pixel esta dentro de la region de traslape interpolamos y evaluamos
      if(rt==-1||ct==-1)
        {
          ptrM[0]			= int(_default_value);
          ptrM[1]			= int(_default_value);
        }
      else if((y>=0)&& (y<=(sizeR.height-1))&&(x>=0)&&(x<=(sizeR.width-1)))
        {
          int tmpint   	= matBins.at<int>(rt,ct);
          ptrM[0]			= tmpint;
          double tmp    	= InterpolatedValue(CoeffIp,x,y,_spline_degree); //Interpolacion
          ptrM[1]			= EstimateBin(tmp,_nbins,_max_intensity); //Estima el bin al que pertenece
        }
      else //Pixeles fuera del �rea de traslape
        {
          aux++;
          //continue;
          int tmpint   	= matBins.at<int>(rt,ct);
          ptrM[0]			= tmpint;
          ptrM[1]			= int(_default_value);
        }
    }

  _correct = 1.0/( (2.0*double(_npixs)) - double(aux) + 1.0);
  _inliers = (2*_npixs) - aux;

  //Si no se encontro ninguna muestar trasnformacion
  /*if(aux >= _npixs)
                return 1.0;*/

  //Evaluamos los Kernels de las marginales y las conjuntas
  cv::Mat kernels	= cv::Mat::zeros(1,3,CV_64F);

  //Seleccionamos el estimador
  int estimator = getEstimatorType();
  if(estimator == 1)			{EvalKernels(M,kernels);}
  else if (estimator == 2)	{EvalKernels2(M,kernels);}
  else if (estimator == 3)	{EvalKernels3(M,kernels);}
  else                          {throw KP::Error("Estimator dont exist",__FUNCTION__);}


  //Aplicamos la f�rmula de KPs
  double result=0.0;
  switch (_type)
    {
    case 1:
      result = KPNormalized(kernels); //Art�culo original
      break;
    case 2:
      result = KPadd(kernels); //Nueva propuesta
      break;
    default:
      throw KP::Error("KP function dont exist",__FUNCTION__);
      break;
    }
  return result;


}

void KPPerspective::GetSamples(const cv::Mat&tperspective,const int rt, const int ct,cv::Mat&mre,cv::Mat&mco)
{

  const double *ptr_t	= tperspective.ptr<double>(0);
  double *re		= mre.ptr<double>(0);
  double *co		= mco.ptr<double>(0);

  double r		= 0, c	= 0;
  double rxc	= 0, cxc = 0;

  double h			= 0.0;
  double num_rer = 0.0;
  double num_cor = 0.0;

  double rer = 0.0;
  double cor = 0.0;

  //Transforma a coordenadas de la imagen de referencia
  r = rt + _a;
  c = ct + _b;

  rxc = (r - _xc[0]);
  cxc = (c - _xc[1]);

  h		= rxc*ptr_t[6]+cxc*ptr_t[7]+1.0f;
  num_rer	= rxc*ptr_t[0]+cxc*ptr_t[1]+ptr_t[4];
  num_cor	= rxc*ptr_t[2]+cxc*ptr_t[3]+ptr_t[5];

  rer		= num_rer/h + _xc[0];
  cor		= num_cor/h + _xc[1];

  rxc *= _epsdiff;
  cxc *= _epsdiff;

  //Checa que est� en la intersecci�n de las 16 transformaciones
  for(int t = 0; t < (2*_parameters-1); t++)
    {
      switch(t)
        {
        case 0 :
          {
            re[0] = (num_rer-rxc)/h + _xc[0];
            co[0] = cor;
            re[1] = (num_rer+rxc)/h + _xc[0];
            co[1] = cor;
            break;
          }
        case 1 :
          {
            re[2] = (num_rer-cxc)/h + _xc[0];
            co[2] = cor;
            re[3] = (num_rer+cxc)/h + _xc[0];
            co[3] = cor;
            break;
          }
        case 2 :
          {
            re[4] = rer;
            co[4] = (num_cor-rxc)/h + _xc[1];
            re[5] = rer;
            co[5] = (num_cor+rxc)/h + _xc[1];
            break;
          }
        case 3 :
          {
            re[6] = rer;
            co[6] = (num_cor-cxc)/h + _xc[1];
            re[7] = rer;
            co[7] = (num_cor+cxc)/h + _xc[1];
            break;
          }

          //El vector de traslaci�n se explora un poco m�s
        case 4 :
          {
            re[8] = (num_rer-100*_epsdiff)/h + _xc[0];
            co[8] = cor;
            re[9] = (num_rer+100*_epsdiff)/h + _xc[0];
            co[9] = cor;
            break;
          }
        case 5 :
          {
            re[10] = rer;
            co[10] = (num_cor-100*_epsdiff)/h+ _xc[1];
            re[11] = rer;
            co[11] = (num_cor+100*_epsdiff)/h+ _xc[1];
            break;
          }
        case 6 :
          {
            double h_Meps = h+rxc/100.0f;
            double h_meps = h-rxc/100.0f;
            re[12] = num_rer/h_meps + _xc[0];
            co[12] = num_cor/h_meps + _xc[1];
            re[13] = num_rer/h_Meps + _xc[0];
            co[13] = num_cor/h_Meps + _xc[1];
            break;
          }
        case 7 :
          {
            double h_Meps = h+cxc/100.0f;
            double h_meps = h-cxc/100.0f;
            re[14] = num_rer/h_meps + _xc[0];
            co[14] = num_cor/h_meps + _xc[1];
            re[15] = num_rer/h_Meps + _xc[0];
            co[15] = num_cor/h_Meps + _xc[1];
            break;
          }
        }
    }

}

int KPPerspective::takeSamplesGradient(std::vector<cv::Mat>&M,const cv::Mat&tperspective)
{
  if( (tperspective.rows != 1) || (tperspective.cols != (_parameters)))
    throw KP::Error("Transformation invalid size",__FUNCTION__);

  //Size of array,matrix and iterations
  const int msize     = 2*_npixs;
  const int arrsize   = 2*(_parameters);
  const int maxiters  = 10*_npixs;

  //Inicialize M
  M.clear();
  M.resize(arrsize);
  for(unsigned int i=0; i<M.size();i++)
    M[i]=cv::Mat::zeros(msize,2,CV_32S);

  cv::Mat matre	= cv::Mat::zeros(1,arrsize,CV_64F);
  cv::Mat matco	= cv::Mat::zeros(1,arrsize,CV_64F);
  double *re		= matre.ptr<double>(0);
  double *co		= matco.ptr<double>(0);

  int rt, ct, cont = 0, it = 0;
  int ntr;
  int t;

  //number of points found it
  int found = 0;

  int C = _rect.x + _rect.width - 1;
  std::uniform_int_distribution<> samples_cols(_rect.x, C);

  int R = _rect.y + _rect.height - 1;
  std::uniform_int_distribution<int> samples_rows(_rect.y, R);

  while( (cont<msize) && (!(it>maxiters)) )
    {

      //Pesca un pixel
      rt = samples_rows(kp_gen);
      ct = samples_cols(kp_gen);

      //Calcula las coordenadas para las 16 transformaciones
      GetSamples(tperspective,rt,ct,matre,matco);

      ntr = 0;
      for(t = 0; t < arrsize; t++)
        {
          //Verifica que se encuentre en el traslape
          if( (re[t]>=0)&&(re[t]<=(sizeR.height-1))&&(co[t]>=0)&&(co[t]<=(sizeR.width-1)))
            ntr ++;
        }

      //Si la muestra se encuentra en la intersecci�n de las 16 transformaciones, se acepta
      if(ntr==arrsize)
        {
          temp.at<int>(rt,ct)=255;
          found++;
          int tmpint   = matBins.at<int>(rt,ct);
          int *ptrMint = nullptr;
          for(t = 0; t < arrsize; t++)
            {
              ptrMint 			= M[t].ptr<int>(cont);
              ptrMint[0]    	= tmpint;
              double tmp    	= InterpolatedValue(CoeffIp,co[t],re[t],_spline_degree);
              ptrMint[1]    	= EstimateBin(tmp,_nbins,_max_intensity);
            }
          cont++;
        }
      it++;
    }
  //cv::imwrite("Samples.jpg",temp);
  return found;
}

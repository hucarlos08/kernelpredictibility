#include <untOptimization.hpp>



// ================================ FUNCIONES GENERALES DE OPTIMIZACION ==============

void Register(KernelP&kp,Optimization&optim,const cv::Mat&image,const cv::Mat&transform,
              cv::Mat&taffin,int MAXITERS,const double lambda,const int method)
{
    optim.setMaxIterations(MAXITERS);
    optim.setLambda(lambda);
    taffin = optim.Minimize(kp,taffin,method);
}


// ================================ FUNCIONES DE LA CLASE OPTIMIZACION==================

cv::Mat Optimization::Minimize(KernelP&kp,const cv::Mat&transformation,const int method)
{
    cv::Mat result;
    cv::Mat samples;
    double ftol = 1.0e-12;
    clock_t start, finish;
    start = clock();
    switch(method)
    {
        case POWELL:
            {
                //Obtenemos nuevas muestras
                kp.takeSamples(samples);
                kp.setSamples(samples);
                Powell<KernelP>optim(kp,3.0e-8,_ITMAX);
                result 			= optim.minimize(transformation);
                _iterations 	= optim.iter;
                _min_function	= optim.fret;
            }
            break;
        case SIMPLEX:
            {
                //Obtenemos nuevas muestras
                kp.takeSamples(samples);
                kp.setSamples(samples);
                Simplex simplex(ftol);
                result = simplex.minimize(transformation,0.1,kp);
                _iterations 	= simplex.nfunc;
                _min_function	= simplex.fmin;
            }
            break;
        case DGE:
            {
                DG<KernelP>optim(kp,_lambda,_ITMAX);
                result 			= optim.minimize(transformation);
                _iterations 	= optim.iter;
                _min_function	= optim.fret;
            }
            break;
        case CG:
            {
                //Obtenemos nuevas muestras
                kp.takeSamples(samples);
                kp.setSamples(samples);
                Frprmn<KernelP>optim(kp,3.0e-8,_ITMAX);
                result 			= optim.minimize(transformation);
                _iterations 	= optim.iter;
                _min_function	= optim.fret;
            }
            break;
        default:
            throw KP::Error("Not implemented method",__FUNCTION__);
            break;
    }
    finish	= clock();
    _time		= (double)(finish - start) / CLOCKS_PER_SEC;
    return result;
}

#include <untKPDistance.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void RegisterDistance(KPDistance&kp,cv::Mat&taffin,const double lambda,const int maxiters,
/// const int method)
///
/// @brief	Register distance. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @param [in,out]	kp		The kp. 
/// @param [in,out]	taffin	The taffin. 
/// @param	lambda			The lambda. 
/// @param	maxiters		The maxiters. 
/// @param	method			The method. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void RegisterDistance(KPDistance&kp,cv::Mat&taffin,const double lambda,const int maxiters,const int method)
{
  Optimization optim(maxiters);
  optim.setLambda(lambda);

  taffin = optim.Minimize(kp,taffin,method);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	cv::Mat RegisterDistance(const cv::Mat&original,const cv::Mat&transform,
/// const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD, const double&factor,const double&factorD,
/// const int npixs,const double&lambda, const int&maxiters,const int&pyr_levels)
///
/// @brief	Register distance. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @param	original    The original.
/// @param	transform	The transform. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	factor		The factor. 
/// @param	factorD		The factor d. 
/// @param	npixs       The npixs.
/// @param	lambda		The lambda. 
/// @param	maxiters    The maxiters.
/// @param	pyr_levels	The pyr levels. 
///
/// @return	. 
////////////////////////////////////////////////////////////////////////////////////////////////////

cv::Mat RegisterDistance(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,
                         const cv::Vec2d&sigmasD,const double&factor,const double&factorD,const int npixs,
                         const double&lambda,const int&maxiters,const int&pyr_levels,const int membresia,
                         const double alpha,const int apertureSize, const double threshold1,
                         const double threshold2,const Border_Type b_type)
{
  KPDistance kp(unsigned(time(NULL)),true,apertureSize,threshold1,threshold2,b_type);

  Membrecy *membrecy = setMembrecy(membresia,alpha);
  kp.setMembrecy(membrecy);

  int nbins		= static_cast<int>(factor);
  int nbinsD		= static_cast<int>(factorD);

  double epsdiff = 0.01;

  //Matriz para contener la transformacion
  cv::Mat taffin;

  //Inicializamos los parametros
  kp.setAllParameters(npixs,nbins,factor,SKP1,epsdiff,sigmas);
  kp.setDistanceParameters(nbinsD,sigmasD,factorD);

  //Inicializamos la transformacion
  taffin = kp.GetInicializeT().reshape(1,1);

  //Craemos los nieveles piramidales para las imagenes de intensidades
  std::vector<cv::Mat>pyr_original;
  std::vector<cv::Mat>pyr_transform;

  cv::buildPyramid(original,pyr_original,pyr_levels);
  cv::buildPyramid(transform,pyr_transform,pyr_levels);

  //Ciclo para optimizar en cada nivel piramidal
  int level = 0;
  double _lambda = lambda;
  for(level = (pyr_original.size()-1); level>=0; level--)
    {
      kp.InitializeDistance(pyr_original[level],pyr_transform[level]);
      RegisterDistance(kp,taffin,lambda);
      _lambda /= 2;
    }
  return taffin;
}




//============================== FUNCIONES MIEMBRO DE KPDISTANCE ================

void KPDistance::setDistanceParameters(const int nbinsD,const cv::Vec2d&sigmasD,const double maxdistance)
{
  setBinsD(nbinsD);
  setSigmasDistance(sigmasD);
  setMaxDistance(maxdistance);
}



void KPDistance::InitializeDistance(const cv::Mat&original,const cv::Mat&transform)
{

  KernelP::Inicialize(original,transform);

  //Estimamos los bordes de cada imagen
  cv::Mat_<uchar> border_original;
  cv::Mat_<uchar> border_transform;


  std::string str_par =	std::string("_t1_")        + Value_To_String(_threshold1) +
      std::string("_t2_")	+ Value_To_String(_threshold2) +
      std::string("_a_")    + Value_To_String(_apertureSize) +
      std::string("_b_")    + Value_To_String(_b_type);
  std::string extbmp(".jpg");
  std::string exttxt(".txt");


  cv::BordersFromType<double,uchar>(original,border_original,_apertureSize,_threshold1,_threshold2,_b_type);
  cv::BordersFromType<double,uchar>(transform,border_transform,_apertureSize,_threshold1,_threshold2,_b_type);

  if(_verbose)
    {
      std::string str_original  = std::string("border_original") + str_par + extbmp;
      std::string str_transform = std::string("border_transform") + str_par + extbmp;
      cv::imwrite(str_original,border_original);
      cv::imwrite(str_transform,border_transform);
    }

  //Creamos las matrices de distancia
  cv::Mat_<double> distance_original;
  cv::Mat_<double> distance_transform;

  unsigned char umb = 255;

  cv::Distance(border_original,distance_original,umb);
  cv::Distance(border_transform,distance_transform,umb);

  if(_verbose)
    {
      std::string str_original  = std::string("distance_original") + str_par + exttxt;
      std::string str_transform = std::string("distance_transform") + str_par + exttxt;
      cv::WriteinFile<double>(distance_original,str_original);
      cv::WriteinFile<double>(distance_transform,str_transform);
      cv::Mat temp1 = cv::NormalizeRange<double,uchar>(distance_original,0,255.0);
      cv::imwrite("EDT.jpg",temp1);
    }

  //	double minDO,maxDO;
  //	double minDT,maxDT;

  //	cv::minMaxLoc(distance_original,&minDO,&maxDO);
  //	cv::minMaxLoc(distance_transform,&minDT,&maxDT);

  //Aplicamos la funcion de membresia
  distance_original    = cv::ToneFunction<double,Membrecy>(distance_original,*_membrecy);
  distance_transform	= cv::ToneFunction<double,Membrecy>(distance_transform,*_membrecy);

  if(_verbose)
    {
      std::string str_original  = std::string("membrecy_original") + str_par + exttxt;
      std::string str_transform = std::string("membrecy_transform") + str_par + exttxt;
      cv::WriteinFile<double>(distance_original,str_original);
      cv::WriteinFile<double>(distance_transform,str_transform);
    }

  distance_original    = cv::NormalizeRange<double,double>(distance_original,0.0,_max_distance);
  distance_transform	= cv::NormalizeRange<double,double>(distance_transform,0.0,_max_distance);

  //Inicializamos los kernels
  KernelP::FillKernels(KMD,KCD,_sigmamD,_sigmacD,_nbinsD,_max_distance,_verbose);

  //Generamos la matris de bins para la transformada
  KernelP::FillBinsMatrix(distance_transform,matBinsD,_nbinsD,_max_distance);

  //Llenamos la matriz de interpolacion
  SamplesToCoefficients(distance_original,CoeffIpD);

  if(_verbose)
    {
      std::string str_original  = std::string("norm_range_membrecy_original") + str_par + extbmp;
      std::string str_transform = std::string("norm_range_membrecy_transform") + str_par + extbmp;
      distance_original		= cv::NormalizeRange<double,uchar>(distance_original);
      distance_transform	= cv::NormalizeRange<double,uchar>(distance_transform);
      cv::imwrite(str_original,distance_original);
      cv::imwrite(str_transform,distance_transform);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	int KPDistance::takeSamplesGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2,
/// const cv::Mat&taffin)
///
/// @brief	Take samples gradient. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param [in,out]	M1	The first std::vector<cv::Mat>&. 
/// @param [in,out]	M2	The second std::vector<cv::Mat>&. 
/// @param	taffin		The taffin. 
///
/// @return	. 
////////////////////////////////////////////////////////////////////////////////////////////////////

int KPDistance::takeSamplesGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2,const cv::Mat&taffin)
{
  if(taffin.rows != 1 || (taffin.cols != _parameters))
    throw KP::Error("Tranformation invalid size",__FUNCTION__);

  const double *tafin = taffin.ptr<double>(0);

  //Size of array,matrix and iterations
  const int msize     = 2*_npixs;
  const int arrsize   = 2*_parameters;
  const int maxiters  = 10*_npixs;

  //Inicialize M1 and M2
  M1.clear();
  M1.resize(arrsize);
  for(unsigned int i=0; i<M1.size();i++)
    M1[i]=cv::Mat::zeros(msize,2,CV_32S);

  M2.clear();
  M2.resize(arrsize);
  for(unsigned int i=0; i<M2.size();i++)
    M2[i]=cv::Mat::zeros(msize,2,CV_32S);

  cv::Mat matre(1,arrsize,CV_64F);
  cv::Mat matco(1,arrsize,CV_64F);

  double *re	= matre.ptr<double>(0);
  double *co	= matco.ptr<double>(0);

  int rt, ct, cont = 0, it = 0;
  double r, c, rxc, cxc, rer, cor;
  int ntr;
  int t;

  //number of points found it
  int found = 0;

  int C = _rect.x + _rect.width - 1;
  std::uniform_int_distribution<> samples_cols(_rect.x, C);

  int R = _rect.y + _rect.height - 1;
  std::uniform_int_distribution<> samples_rows(_rect.y, R);

  while( (cont<msize) && (!(it>maxiters)) )
    {
      //Pesca un pixel
      rt = samples_rows(kp_gen);
      ct = samples_cols(kp_gen);


      //Transforma a coordenadas de la imagen de referencia
      r = rt + _a;
      c = ct + _b;

      rxc = (r - _xc[0]);
      cxc = (c - _xc[1]);

      ntr = 0;

      //Transforma las coordenadas del pixel muestreado
      rer = rxc*tafin[0]+cxc*tafin[1]+tafin[2] + _xc[0] ;
      cor = rxc*tafin[3]+cxc*tafin[4]+tafin[5] + _xc[1];

      rxc *= _epsdiff;
      cxc *= _epsdiff;

      //Checa que est� en la intersecci�n de las 12 transformaciones
      for(t = 0; t < (int)_parameters; t++)
        {
          switch(t)
            {
            case 0 :
              {
                re[0] = rer-rxc;
                co[0] = cor;
                re[1] = rer+rxc;
                co[1] = cor;
                break;
              }
            case 1 :
              {
                re[2] = rer-cxc;
                co[2] = cor;
                re[3] = rer+cxc;
                co[3] = cor;
                break;
              }

              //El vector de traslaci�n se explora un poco m�s
            case 2 :
              {
                re[4] = rer-100 * _epsdiff;
                co[4] = cor;
                re[5] = rer+100 * _epsdiff;
                co[5] = cor;
                break;
              }
            case 3 :
              {
                re[6] = rer;
                co[6] = cor-rxc;
                re[7] = rer;
                co[7] = cor+rxc;
                break;
              }
            case 4 :
              {
                re[8] = rer;
                co[8] = cor-cxc;
                re[9] = rer;
                co[9] = cor+cxc;
                break;
              }

            case 5 :
              {
                re[10] = rer;
                co[10] = cor-100 * _epsdiff;
                re[11] = rer;
                co[11] = cor+100 * _epsdiff;
                break;
              }
            }
        }

      for(t = 0; t < arrsize; t++)
        {
          //Verifica que se encuentre en el traslape
          if( (re[t]>=0)&&(re[t]<=(sizeR.height-1))&&(co[t]>=0)&&(co[t]<=(sizeR.width-1)))
            ntr ++;
        }

      //Si la muestra se encuentra en la intersecci�n de las 12 transformaciones, se acepta
      if(ntr==arrsize)
        {
          found++;

          int tmpint1		= matBins.at<int>(rt,ct);
          int *ptrMint1	= nullptr;

          int tmpint2		= matBinsD.at<int>(rt,ct);
          int *ptrMint2	= nullptr;

          for(t = 0; t < arrsize; t++)
            {
              //Valores para las intensidades
              ptrMint1			= M1[t].ptr<int>(cont);
              ptrMint1[0]		= tmpint1;

              double tmp1		= InterpolatedValue(CoeffIp,co[t],re[t],_spline_degree);
              ptrMint1[1]		= EstimateBin(tmp1,_nbins,_max_intensity);

              //Valores para la distancia
              ptrMint2			= M2[t].ptr<int>(cont);
              ptrMint2[0]		= tmpint2;

              double tmp2		= InterpolatedValue(CoeffIpD,co[t],re[t],_spline_degree);
              ptrMint2[1]		= EstimateBin(tmp2,_nbinsD,_max_distance);

            }
          cont++;
        }
      it++;
    }
  return found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void KPDistance::EvalKernels(const cv::Mat&M1,const cv::Mat&M2,cv::Mat&pred)
///
/// @brief	Eval kernels. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param	M1				The first const cv::Mat&. 
/// @param	M2				The second const cv::Mat&. 
/// @param [in,out]	pred	The pred. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void KPDistance::EvalKernels(const cv::Mat&M1,const cv::Mat&M2,cv::Mat&pred)
{
  const int nsamples = 2*_npixs;
  if( (M1.rows < nsamples) || (M2.rows<nsamples) )
    throw KP::Error("Mismatch M size",__FUNCTION__);

  int i, j;
  int dist1[2]={0,0};
  int dist2[2]={0,0};

  //Apuntadores para los valores de intensidades
  double       *ptr_pred	= pred.ptr<double>(0);
  const double *ptr_KM		= KM.ptr<double>(0);
  const double *ptr_KMD	= KMD.ptr<double>(0);

  //Apuntadores a las filas de M1
  const int *ptr_M1i   = nullptr;
  const int *ptr_M1j   = nullptr;

  //Apuntadores a las filas de M1
  const int *ptr_M2i   = nullptr;
  const int *ptr_M2j   = nullptr;

  ptr_pred[0] = 0.0;
  ptr_pred[1] = 0.0;
  ptr_pred[2] = 0.0;

  for(i = 0; i < _npixs; i++)
    for(j = _npixs; j < nsamples; j++)
      {
        //Asigamos los punteros
        ptr_M1i       = M1.ptr<int>(i);
        ptr_M1j       = M1.ptr<int>(j);

        ptr_M2i       = M2.ptr<int>(i);
        ptr_M2j       = M2.ptr<int>(j);

        //Calcula la distancia entre las muestras
        dist1[0]     = std::abs(ptr_M1i[0]-ptr_M1j[0]);
        dist1[1]     = std::abs(ptr_M1i[1]-ptr_M1j[1]);

        dist2[0]     = std::abs(ptr_M2i[0]-ptr_M2j[0]);
        dist2[1]     = std::abs(ptr_M2i[1]-ptr_M2j[1]);

        ptr_pred[0]  += ptr_KM[dist1[0]] * ptr_KMD[dist2[0]];
        ptr_pred[1]  += ptr_KM[dist1[1]] * ptr_KMD[dist2[1]];

        ptr_pred[2]  += KC.at<double>(dist1[0],dist1[1]) * KCD.at<double>(dist2[0],dist2[1]);
      }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	cv::Mat KPDistance::EstimateGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2)
///
/// @brief	Estimate gradient. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param [in,out]	M1	The first std::vector<cv::Mat>&. 
/// @param [in,out]	M2	The second std::vector<cv::Mat>&. 
///
/// @return	. 
////////////////////////////////////////////////////////////////////////////////////////////////////

cv::Mat KPDistance::EstimateGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2)
{

  const int nsamples		= 2* _npixs;
  const int nparameters	= 2*_parameters;

  if( (M1.size() != M2.size()) )
    throw KP::Error("Mismatch M's size",__FUNCTION__);

  if( (M1.size() != (unsigned int) nparameters) || (M1[0].rows != nsamples) )
    throw KP::Error("Mismatch M size",__FUNCTION__);

  int i 					= 0;
  cv::Mat predad			= cv::Mat::zeros(1,3,CV_64F);
  cv::Mat predat			= cv::Mat::zeros(1,3,CV_64F);
  cv::Mat gradient		= cv::Mat::zeros(1,_parameters,CV_64F);
  double simat			= 0.0;
  double simad			= 0.0;

  //Aproxima las derivadas direccionales mediante diferencias centrales
  double *grad         = gradient.ptr<double>(0);

  for(i = 0; i < _parameters; i++)
    {
      KPDistance::EvalKernels(M1[2*i],M2[2*i],predat);
      KPDistance::EvalKernels(M1[2*i+1],M2[2*i+1],predad);

      switch (_type)
        {
        case 1:
          simad	= KPNormalized(predad);
          simat	= KPNormalized(predat);
          break;
        case 2:
          simad	= KPadd(predad);
          simat	= KPadd(predat);
          break;
        default:
          throw KP::Error("KP function dont exist",__FUNCTION__);
          break;
        }
      grad[i] =  (simad-simat)/(2.0*_epsdiff);
    }
  return gradient;
}

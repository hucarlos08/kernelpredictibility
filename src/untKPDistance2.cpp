#include <untKPDistance2.hpp>



cv::Mat RegisterDistance2(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,
                          const cv::Vec2d&sigmasD,const double&factor,const double&factorD,const int npixs,
                          const double&lambda,const int&maxiters,const int&pyr_levels,const int membresia,
                          const double alpha,const int apertureSize, const double threshold1,
                          const double threshold2,const Border_Type b_type)
{
	unsigned long seed = static_cast<unsigned long>(time(NULL));
   KPAffin kp(seed,true);
	KPDistance2 kp2(seed,apertureSize,threshold1,threshold2,  b_type,true);

	Membrecy *membrecy = setMembrecy(membresia,alpha);
	kp2.setMembrecy(membrecy);

	//Inicializamos el esquema piramidal
	std::vector< cv::Mat >pyr_image;
	std::vector< cv::Mat >pyr_transform;

	cv::Mat transformation = kp.GetInicializeT().reshape(1,1);

	cv::buildPyramid(original,pyr_image,pyr_levels);
	cv::buildPyramid(transform,pyr_transform,pyr_levels);

	//Iteraciones sobre los niveles piramidales
	for(int level = (pyr_image.size()-1); level>=0; level--)
	{
		kp2.Inicialize(pyr_image[level],pyr_transform[level],sigmas,sigmasD,factor,factorD,npixs,lambda);
		DG<KPDistance2>optim(kp2,lambda,maxiters);
		transformation 	= optim.minimize(transformation);
	}
	
	return transformation;
}



void KPDistance2::Inicialize(const cv::Mat&image,const cv::Mat&transform,const cv::Vec2d&sigmas,
							const cv::Vec2d&sigmasD,const double&factor,const double&factorD,
							const int npixs,const double epsdiff)
{
	
	int nbins		= static_cast<int>(factor);
	int nbinsD		= static_cast<int>(factorD);

	
	//Inicializamos el kp de intensidades
	_kp.setAllParameters(npixs,nbins,factor,SKP1,epsdiff,sigmas);
	_kp.Inicialize(image,transform);

	//Construimos las imagenes de distancia y funcion de membresia
	
	//Estimamos los bordes de cada imagen 
	cv::Mat_<uchar> border_original;
	cv::Mat_<uchar> border_transform;


	std::string str_par =	std::string("_t1_")	+ Value_To_String(_threshold1) +
							std::string("_t2_")	+ Value_To_String(_threshold2) +
							std::string("_a_")	+ Value_To_String(_apertureSize);

	std::string extbmp(".bmp");
	std::string exttxt(".txt");

	cv::BordersFromType<double,uchar>(image,border_original,_apertureSize,_threshold1,_threshold2,_b_type);
   cv::BordersFromType<double,uchar>(transform,border_transform,_apertureSize,_threshold1,_threshold2,_b_type);


	if(_verbose)
	{
		std::string str_original  = std::string("border_original") + str_par + extbmp;
		std::string str_transform = std::string("border_transform") + str_par + extbmp;
		cv::imwrite(str_original,border_original);
		cv::imwrite(str_transform,border_transform);
	}

	//Creamos las matrices de distancia
	cv::Mat_<double> distance_original;
	cv::Mat_<double> distance_transform;

	unsigned char umb = 255;

	cv::Distance(border_original,distance_original,umb);
	cv::Distance(border_transform,distance_transform,umb);

	if(_verbose)
		{
			std::string str_original  = std::string("distance_original") + str_par + exttxt;
			std::string str_transform = std::string("distance_transform") + str_par + exttxt;
			cv::WriteinFile<double>(distance_original,str_original);
			cv::WriteinFile<double>(distance_transform,str_transform);
		}

	double minDO,maxDO;
	double minDT,maxDT;

	cv::minMaxLoc(distance_original,&minDO,&maxDO);
	cv::minMaxLoc(distance_transform,&minDT,&maxDT);

	//Aplicamos la funcion de membresia
	distance_original		= cv::ToneFunction<double,Membrecy>(distance_original,*(_membrecy));
	distance_transform	= cv::ToneFunction<double,Membrecy>(distance_transform,*(_membrecy));

	if(_verbose)
	{
		std::string str_original  = std::string("membrecy_original") + str_par + exttxt;
		std::string str_transform = std::string("membrecy_transform") + str_par + exttxt;
		cv::WriteinFile<double>(distance_original,str_original);
		cv::WriteinFile<double>(distance_transform,str_transform);
	}

	distance_original		= cv::NormalizeRange<double,double>(distance_original,0.0,factorD);
	distance_transform	= cv::NormalizeRange<double,double>(distance_transform,0.0,factorD);


	//Inicializamos el kp de membresia
	_kpD.setAllParameters(npixs,nbinsD,factorD,SKP1,epsdiff,sigmasD);
	_kpD.Inicialize(distance_original,distance_transform);


	if(_verbose)
		{
			std::string str_original  = std::string("norm_range_membrecy_original") + str_par + extbmp;
			std::string str_transform = std::string("norm_range_membrecy_transform") + str_par + extbmp;
			distance_original		= cv::NormalizeRange<double,uchar>(distance_original);
			distance_transform	= cv::NormalizeRange<double,uchar>(distance_transform);
			cv::imwrite(str_original,distance_original);
			cv::imwrite(str_transform,distance_transform);
		}

}
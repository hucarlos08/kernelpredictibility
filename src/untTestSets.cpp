#include <untTestSets.hpp>

//#define median_filter 0

const cv::Rect roi(19,19,90,90);

const static int size_window = 7;
const static double stc_sigma_blur = 1.0;
const static cv::Size stc_size_blur(size_window,size_window);

const static std::string str_sigma_blur	  = std::string("_sigma_") + Value_To_String(stc_sigma_blur);
const static std::string str_window_blur  = std::string("_wsize_") + Value_To_String(size_window);

// ================================= VERSIONES DE LAS FUNCIONES EN PARALELO ============================

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void ParallelSetDistanceTest(StructTestDirectories&set,const std::vector<int>&samples,
/// const cv::Vec2d&sigmas,const cv::Vec2d sigmasD, const double lambda,const int pyr_levels,
/// const int maxiters,const double epsdiff,const int estimator, const unsigned long seed,
/// const double alpha)
///
/// @brief	Tests parallel set distance. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @param [in,out]	set	The set. 
/// @param	samples		The samples. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	lambda		The lambda. 
/// @param	pyr_levels	The pyr levels. 
/// @param	maxiters	The maxiters. 
/// @param	epsdiff		The epsdiff. 
/// @param	estimator	The estimator. 
/// @param	seed		The seed. 
/// @param	alpha		The alpha. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void ParallelSetDistanceTest(const StructTestDirectories&set,const std::vector<int>&samples,
			     const cv::Vec2d&sigmas,const cv::Vec2d sigmasD,
			     const double lambda,const int pyr_levels,
			     const int maxiters,const double epsdiff,const int estimator,
			     const unsigned long seed,const int membresia,const double alpha,
			     const int apertureSize, const double threshold1,const double threshold2,
			     const Border_Type b_type)
{

    const int test_number = static_cast<int>(samples.size());
    int iters = 0;

    /*SetDistanceTest(set,samples[0],sigmas,sigmasD,SKP1,estimator,lambda,pyr_levels,maxiters,epsdiff);*/

    //Pruebas del tipo SKP1
#pragma omp parallel for  shared(iters,sigmas)
    for(iters=0; iters<test_number; iters++)
    {
        SetDistanceTest(set,seed,samples[iters],sigmas,sigmasD,SKP1,estimator,lambda,pyr_levels,maxiters,epsdiff,
                        membresia,alpha,apertureSize,threshold1,threshold2,b_type);
    };
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void ParallelSetDistanceTest(StructTestDirectories&set,const std::vector<int>&samples,
/// const cv::Vec2d&sigmas,const cv::Vec2d sigmasD, const double lambda,const int pyr_levels,
/// const int maxiters,const double epsdiff,const int estimator, const unsigned long seed,
/// const double alpha)
///
/// @brief	Tests parallel set distance. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @param [in,out]	set	The set. 
/// @param	samples		The samples. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	lambda		The lambda. 
/// @param	pyr_levels	The pyr levels. 
/// @param	maxiters	The maxiters. 
/// @param	epsdiff		The epsdiff. 
/// @param	estimator	The estimator. 
/// @param	seed		The seed. 
/// @param	alpha		The alpha. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void ParallelSetDistance2Test(const StructTestDirectories&set,const std::vector<int>&samples,
			      const cv::Vec2d&sigmas,const cv::Vec2d sigmasD,
			      const double lambda,const int pyr_levels,
			      const int maxiters,const double epsdiff,const int estimator,
			      const unsigned long seed,const int membresia,const double alpha,
			      const int apertureSize, const double threshold1,const double threshold2,
			      const Border_Type b_type)
{

    const int test_number = static_cast<int>(samples.size());
    int iters = 0;

    //Pruebas del tipo SKP1
#pragma omp parallel for  shared(iters,sigmas)
    for(iters=0; iters<test_number; iters++)
    {
        SetDistance2Test(set,seed,samples[iters],sigmas,sigmasD,SKP1,estimator,lambda,pyr_levels,maxiters,epsdiff,
                         membresia,alpha,apertureSize,threshold1,threshold2,b_type);
    };
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void ParallelSetSKPTest(StructTestDirectories&set,const std::vector<int>&samples,
/// const cv::Vec2d&sigmas,const double lambda,const int pyr_levels, const int maxiters,
/// const double epsdiff,const int estimator, const unsigned long seed)
///
/// @brief	Tests parallel set skp. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @param [in,out]	set	The set. 
/// @param	samples		The samples. 
/// @param	sigmas		The sigmas. 
/// @param	lambda		The lambda. 
/// @param	pyr_levels	The pyr levels. 
/// @param	maxiters	The maxiters. 
/// @param	epsdiff		The epsdiff. 
/// @param	estimator	The estimator. 
/// @param	seed		The seed. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void ParallelSetSKPTest(const StructTestDirectories&set,const std::vector<int>&samples,
                        const cv::Vec2d&sigmas,const double lambda,const int pyr_levels,
                        const int maxiters,const double epsdiff,const int estimator,
                        const unsigned long seed)
{

    const int test_number = static_cast<int>(samples.size());
    int iters         = 0;
    int total_iters   = 0;

    //creamos los vectores con los valores ordenados
    std::vector<int>v_samples;
    std::vector<int>v_type;

    for(int i=0; i<test_number; i++)
    {
        v_samples.push_back(samples[i]);
        v_samples.push_back(samples[i]);
        v_type.push_back(SKP1);
        v_type.push_back(SKP2);
    }

    //   std::cout<<v_samples[0]<<' '<<v_samples[1]<<' '<<v_samples[2]<<std::endl;
    //   std::cout<<v_type[0]<<' '<<v_type[1]<<' '<<v_type[2]<<std::endl;


    //Pruebas del tipo SKP
    const int nsamples = v_samples.size();
    for(int method = POWELL; method<= DGE; method++)
    {
#pragma omp parallel for shared(iters,v_samples,v_type,set) private(total_iters)
        for (iters=0; iters < nsamples; iters++)
        {
            total_iters =  (method == SIMPLEX)? 33333:maxiters; //SIMPLEX case
            SetSKPTest(set,seed,method,v_samples[iters],sigmas,v_type[iters],estimator,lambda,pyr_levels,
                       total_iters,epsdiff);

        };
    }

}


// ================================= VERSIONES PARA EVALUAR UN SOLO CONJUNTO ==========================

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void SetSKPTest(StructTestDirectories&set,const int seed,const int method,const int npixs,
/// const cv::Vec2d&sigmas, const int type,const int estimator,const double lambda,
/// const int pyr_levels, const int maxiters,const double epsdiff)
///
/// @brief	Tests set skp. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param [in,out]	set	The set. 
/// @param	seed		The seed. 
/// @param	method		The method. 
/// @param	npixs		The npixs. 
/// @param	sigmas		The sigmas. 
/// @param	type		The type. 
/// @param	estimator	The estimator. 
/// @param	lambda		The lambda. 
/// @param	pyr_levels	The pyr levels. 
/// @param	maxiters	The maxiters. 
/// @param	epsdiff		The epsdiff. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void SetSKPTest(const StructTestDirectories&set,const int seed,const int method,const int npixs,
                const cv::Vec2d&sigmas,const int type,const int estimator,const double lambda,
                const int pyr_levels,const int maxiters,const double epsdiff)
{
    //KP y optimization class
    KPAffin kp(seed,false);

    //Variables utiles
    bool finded	= false;
    int test		= 0;
    int TEST		= 0;
    int tt		= 0;
    int nbins	= 0;
    int level	= 0;
    int scape	= 0;
    const int maxscape = 200;
    double imax = 0.0;
    double image_min,image_max;
    double image_t_min,image_t_max;

    //Coordenadas del centro de la imagen y subregion
    cv::Vec2d xc;


    //Variables niveles piramidales
    std::vector<cv::Mat>pyr_image;
    std::vector<cv::Mat>pyr_image_t;

    //Variables para analizar
    int exitos					= 0;
    int total_iters			= 0;
    int iters_level			= 0;
    int scape_iters			= 0;
    int real_iters				= 0;
    double inicial_affin		= 0.0;
    double inicial_function = 0.0;
    double end_affin			= 1.0;
    double end_function		= 0.0;
    double duration			= 0.0;

    cv::Mat_<double>real_t = cv::Mat_<double>::zeros(1,6);
    cv::Mat_<double>taffin_result;

    //Apuntador leer transformacion real
    double *ptr_real_t = real_t.ptr<double>(0);

    //Variables preprocesamiento de las imagenes
    double sigma_blur = 1.0;
    cv::Size size_blur(3,3);

    //Configuramos las entradas de cadena necesarias
    const std::string result("result");
    const std::string extbmp(".bmp");

    //Abrimos el archivo de transformaciones
    std::string str_in = set.getFileTName();
    std::ifstream in(str_in.c_str(),ios::in);
    if( !in.is_open() ) KP::Error(CombineStrings(NOT_LOAD_FILE,str_in),__FUNCTION__);
    //Leemos el numero de transformaciones
    in>>TEST;

    //Abrimos el archivo de escritura
    std::string str_out = OutFileName(set.getOutputFileName(),npixs,"_pyramidal_",method,type,estimator);
    std::ofstream out(str_out.c_str(),ios::out);
    if( !out.is_open() )
        throw KP::Error(CombineStrings(NOT_LOAD_FILE,str_out),__FUNCTION__);

    //Leemos la imagen original
    std::string str_image		= set.getSetDirectory() + KP::DIAG + std::string("Original.bmp");
    cv::Mat_<double>original	= cv::imread(str_image,0);
    if( original.empty() )
        throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_image),__FUNCTION__);

    //Preprocesamiento de la imagen original:
    cv::GaussianBlur(original,original,size_blur,sigma_blur);
    cv::minMaxLoc(original,&image_min,&image_max);
    xc = cv::Vec2d(original.rows/2.0,original.cols/2.0);

    //Ciclo principal para encontrar las transformaciones
    for(test=0; test<TEST; test++)
    {
        //Leemos la transformacion original
        in>>tt>>ptr_real_t[0]>>ptr_real_t[1]>>ptr_real_t[2]>>ptr_real_t[3]>>ptr_real_t[4]>>ptr_real_t[5];

        //Leemos la imagen con la que nos vamos a comparar
        std::string str_transform	= set.getSetDirectory() + KP::DIAG + result + Value_To_String(test) + extbmp;
        cv::Mat_<double> image_t	= cv::imread(str_transform,0);
        if( image_t.empty() )
            throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_transform),__FUNCTION__);

        //Seleccionamos una subregion
        image_t = cv::Mat_<double>(image_t,roi);

        //Removemos ruido de la imagen transformada
        cv::GaussianBlur(image_t,image_t,size_blur,sigma_blur);

        //Calculamos la intensidad maxima entre las imagenes y lo usamos para normalizar entre 0.0-Imax
        cv::minMaxLoc(image_t,&image_t_min,&image_t_max);

        imax	= std::max(image_max,image_t_max);
        nbins	= static_cast<int>(imax);

        //Estimamos los primeros valores
        taffin_result = kp.GetInicializeT().reshape(1,1);
        inicial_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);

        //Normalizamos el rango dinamico
        original 	= cv::NormalizeRange<double,double>(original,0.0,imax);
        image_t		= cv::NormalizeRange<double,double>(image_t,0.0,imax);

        //Creamos los niveles piramidales
        cv::buildPyramid(original,pyr_image,pyr_levels);
        cv::buildPyramid(image_t,pyr_image_t,pyr_levels);

        //Inicializamos los parametros
        kp.setAllParameters(npixs,nbins,imax,type,epsdiff,sigmas,estimator);

        //Iteramos para cada nivel de la piramide
        int level_begin = pyr_image.size()-1;

        //Variables a medir
        duration		= 0.0;
        total_iters = 0;
        finded		= false;
        for(level = level_begin; level >= 0; level--)
        {
            kp.Inicialize(pyr_image[level],pyr_image_t[level]);
            real_iters  = maxiters;
            iters_level = 0;
            for(scape=0; scape<=maxscape; scape++)
            {

                Optimization optim(real_iters);
                optim.setLambda(lambda);
                taffin_result = optim.Minimize(kp,taffin_result,method);

                //Guardamos los valores
                duration		+= optim.getExecutionTime();
                total_iters += (iters_level = optim.getIterations());
                real_iters	-= iters_level;
                end_affin	= AffinMeanError(original.size(),real_t,taffin_result,xc);

                if(end_affin <= 1.0)
                {
                    finded = true;
                    break;
                }

                if(real_iters <= 0 ) break;


            }

            if(finded == true)
            {
                break;
            }

        }

        //Condicion de exitos
        if(end_affin <= 1.0) exitos++;

        //Escribimos los resultados
        out	<<test
               <<'\t'	<<inicial_affin
              <<'\t'	<<inicial_function
             <<'\t'	<<end_affin
            <<'\t'	<<end_function
           <<'\t'	<<total_iters
          <<'\t'	<<scape_iters
         <<'\t'	<<duration
        <<'\t'	<<cv::norm(taffin_result-real_t)
        <<'\t'	<<exitos
        <<std::endl;
    }

    in.close();
    out.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void SetDistanceTest(StructTestDirectories&set,const int seed, const int npixs,
/// const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD, const int type,const int estimator,
/// const double lambda,const int pyr_levels, const int maxiters,const double epsdiff,
/// const double alpha)
///
/// @brief	Tests set distance. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param [in,out]	set	The set. 
/// @param	seed		The seed. 
/// @param	npixs		The npixs. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	type		The type. 
/// @param	estimator	The estimator. 
/// @param	lambda		The lambda. 
/// @param	pyr_levels	The pyr levels. 
/// @param	maxiters	The maxiters. 
/// @param	epsdiff		The epsdiff. 
/// @param	alpha		The alpha. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void SetDistanceTest(const StructTestDirectories&set,const int seed,
		     const int npixs,const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD,
		     const int type,const int estimator,const double lambda,const int pyr_levels,
		     const int maxiters,const double epsdiff,const int membresia,const double alpha,
		     const int apertureSize, const double threshold1,const double threshold2,
		     const Border_Type b_type)
{
    //KP y optimization class
    KPDistance kp(seed,false,apertureSize,threshold1,threshold2,b_type);

    //Creamos la funcion de membresia
    Membrecy *membrecy = setMembrecy(membresia,alpha);
    kp.setMembrecy(membrecy);

    //Variables utiles
    int test		= 0;
    int TEST		= 0;
    int tt		= 0;
    int nbins	= 0;
    int level	= 0;
    double imax = 0.0;
    double image_min,image_max;
    double image_t_min,image_t_max;

    //Coordenadas del centro de la imagen y subregion
    cv::Vec2d xc;

    //Variables niveles piramidales
    std::vector<cv::Mat>pyr_image;
    std::vector<cv::Mat>pyr_image_t;

    //Factores de normalizacion
    const double factorD	= 255.0;
    const int	 nbinsD	= static_cast<int>(factorD);

    //Variables para analizar
    int exitos					= 0;
    int total_iters			= 0;
    int iters_level			= 0;
    int scape_iters			= 0;
    double inicial_affin		= 0.0;
    double inicial_function = 0.0;
    double end_affin			= 0.0;
    double end_function		= 0.0;
    double duration			= 0.0;

    cv::Mat_<double>real_t = cv::Mat_<double>::zeros(1,6);
    cv::Mat_<double>taffin_result;

    //Apuntador leer transformacion real
    double *ptr_real_t = real_t.ptr<double>(0);

    //Configuramos las entradas de cadena necesarias
    const std::string result("result");
    const std::string extbmp(".bmp");
    const std::string str_alpha = std::string("alpha") + KP::GUION + Value_To_String(alpha)
            +KP::GUION+std::string("m")+Value_To_String(membresia)+str_sigma_blur+str_window_blur;

    //Abrimos el archivo de transformaciones
    std::string str_in = set.getFileTName();
    std::ifstream in(str_in.c_str(),ios::in);
    if( !in.is_open() ) KP::Error(CombineStrings(NOT_LOAD_FILE,str_in),__FUNCTION__);
    //Leemos el numero de transformaciones
    in>>TEST;


    //Abrimos el archivo de escritura
    std::string aux = set.getOutputFileName() + std::string("_D_") + str_alpha;
    std::string str_out = OutFileName2(aux,npixs,"_pyramidal_",DGE,type,estimator,apertureSize,
                                       threshold1,threshold2,b_type);
    std::ofstream out(str_out.c_str(),ios::out);
    if( !out.is_open() )
        throw KP::Error(CombineStrings(NOT_LOAD_FILE,str_out),__FUNCTION__);

    //Leemos la imagen original
    std::string str_image		= set.getSetDirectory() + KP::DIAG + std::string("Original.bmp");
    cv::Mat_<double>original	= cv::imread(str_image,0);
    if( original.empty() )
        throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_image),__FUNCTION__);

    //Preprocesamiento de la imagen original:

#ifdef median_filter
    cv::Mat_<uchar>uchar_original(original);
    cv::medianBlur(uchar_original,uchar_original,size_window);
    original = cv::Mat_<double>(uchar_original);
#else
    cv::GaussianBlur(original,original,stc_size_blur,stc_sigma_blur);
#endif
    cv::minMaxLoc(original,&image_min,&image_max);
    xc = cv::Vec2d(original.rows/2.0,original.cols/2.0);


    //Ciclo principal para encontrar las transformaciones
    for(test=0; test<TEST; test++)
    {
        //Leemos la transformacion original
        in>>tt>>ptr_real_t[0]>>ptr_real_t[1]>>ptr_real_t[2]>>ptr_real_t[3]>>ptr_real_t[4]>>ptr_real_t[5];

        //Leemos la imagen con la que nos vamos a comparar
        std::string str_transform	= set.getSetDirectory() + KP::DIAG + result + Value_To_String(test) + extbmp;
        cv::Mat_<double> image_t	= cv::imread(str_transform,0);
        if( image_t.empty() )
            throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_transform),__FUNCTION__);

        //Seleccionamos una subregion
        image_t = cv::Mat_<double>(image_t,roi);

        //Removemos ruido de la imagen transformada
#ifdef median_filter
	cv::Mat_<uchar>uchar_image_t(image_t);
	cv::medianBlur(uchar_image_t,uchar_image_t,size_window);
	image_t = cv::Mat_<double>(uchar_image_t);
#else
	cv::GaussianBlur(image_t,image_t,stc_size_blur,stc_sigma_blur);
#endif

	//Calculamos la intensidad maxima entre las imagenes y lo usamos para normalizar entre 0.0-Imax
	cv::minMaxLoc(image_t,&image_t_min,&image_t_max);

	imax	= std::max(image_max,image_t_max);
	nbins	= static_cast<int>(imax);

	//Estimamos los primeros valores
	taffin_result = kp.GetInicializeT().reshape(1,1);
	inicial_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);

	//Normalizamos el rango dinamico
	original 	= cv::NormalizeRange<double,double>(original,0.0,imax);
	image_t		= cv::NormalizeRange<double,double>(image_t,0.0,imax);

	//Creamos los niveles piramidales
	cv::buildPyramid(original,pyr_image,pyr_levels);
	cv::buildPyramid(image_t,pyr_image_t,pyr_levels);

	//Inicializamos los parametros
	kp.setAllParameters(npixs,nbins,imax,type,epsdiff,sigmas);
	kp.setDistanceParameters(nbinsD,sigmasD,factorD);

	//Iteramos para cada nivel de la piramide
	int level_begin = pyr_image.size()-1;

	//Variables a medir
	duration		= 0.0;
	total_iters = 0;

	for(level = level_begin; level >= 0; level--)
	{
	    kp.InitializeDistance(pyr_image[level],pyr_image_t[level]);
	    Optimization optim(maxiters);
	    optim.setLambda(lambda);
	    taffin_result = optim.Minimize(kp,taffin_result,DGE);

	    //Guardamos los valores
	    duration		+= optim.getExecutionTime();
	    total_iters += (iters_level = optim.getIterations());

	}

	//Escribimos los resultados
	end_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);
	if(end_affin <= 1.0) exitos++;

	//Escribimos los resultados
	out	<<test
	       <<'\t'	<<inicial_affin
	      <<'\t'	<<inicial_function
	     <<'\t'	<<end_affin
	    <<'\t'	<<end_function
	   <<'\t'	<<total_iters
	  <<'\t'	<<scape_iters
	 <<'\t'	<<duration
	<<'\t'	<<cv::norm(taffin_result-real_t)
	<<'\t'	<<exitos
	<<std::endl;
    }
    delete membrecy;
    in.close();
    out.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void SetDistance2Test(StructTestDirectories&set,const int seed, const int npixs,
/// const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD, const int type,const int estimator,
/// const double lambda,const int pyr_levels, const int maxiters,const double epsdiff,
/// const double alpha)
///
/// @brief	Tests set distance 2. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param [in,out]	set	The set. 
/// @param	seed		The seed. 
/// @param	npixs		The npixs. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	type		The type. 
/// @param	estimator	The estimator. 
/// @param	lambda		The lambda. 
/// @param	pyr_levels	The pyr levels. 
/// @param	maxiters	The maxiters. 
/// @param	epsdiff		The epsdiff. 
/// @param	alpha		The alpha. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void SetDistance2Test(const StructTestDirectories&set,const int seed,
		      const int npixs,const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD,
		      const int type,const int estimator,const double lambda,const int pyr_levels,
		      const int maxiters,const double epsdiff,const int membresia,const double alpha,
		      const int apertureSize,const double threshold1,const double threshold2,
		      const Border_Type b_type,const std::vector<double>betas)
{
    //KP
    KPAffin kp(seed,false);
    KPDistance2 kp2(seed,apertureSize,threshold1,threshold2,b_type);


    //Creamos la funcion de membresia
    Membrecy *membrecy = setMembrecy(membresia,alpha);
    kp2.setMembrecy(membrecy);

    //Variables utiles
    int test		= 0;
    int TEST		= 0;
    int tt		= 0;
    int nbins	= 0;
    int level	= 0;
    double imax = 0.0;
    double image_min,image_max;
    double image_t_min,image_t_max;
    clock_t start, finish;
    start = clock();

    //Coordenadas del centro de la imagen y subregion
    cv::Vec2d xc;

    //Variables niveles piramidales
    std::vector<cv::Mat>pyr_image;
    std::vector<cv::Mat>pyr_image_t;

    //Factores de normalizacion
    const double factorD	= 255.0;
    const int	 nbinsD		= static_cast<int>(factorD);

    //Variables para analizar
    int exitos					= 0;
    int total_iters			= 0;
    int scape_iters			= 0;
    double inicial_affin		= 0.0;
    double inicial_function = 0.0;
    double end_affin			= 0.0;
    double end_function		= 0.0;
    double duration			= 0.0;

    cv::Mat_<double>real_t = cv::Mat_<double>::zeros(1,6);
    cv::Mat_<double>taffin_result;

    //Apuntador leer transformacion real
    double *ptr_real_t = real_t.ptr<double>(0);

    //Configuramos las entradas de cadena necesarias
    const std::string result("result");
    const std::string extbmp(".bmp");
    const std::string str_alpha = std::string("alpha") + KP::GUION + Value_To_String(alpha)
            +KP::GUION+std::string("m")+Value_To_String(membresia)+str_sigma_blur+str_window_blur;

    //Abrimos el archivo de transformaciones
    std::string str_in = set.getFileTName();
    std::ifstream in(str_in.c_str(),ios::in);
    if( !in.is_open() ) KP::Error(CombineStrings(NOT_LOAD_FILE,str_in),__FUNCTION__);
    //Leemos el numero de transformaciones
    in>>TEST;


    //Abrimos el archivo de escritura
    std::string aux = set.getOutputFileName() + std::string("_D2_") + str_alpha;
    std::string str_out = OutFileName2(aux,npixs,"_pyramidal_",DGE,type,estimator,apertureSize,
                                       threshold1,threshold2,b_type);
    std::ofstream out(str_out.c_str(),ios::out);
    if( !out.is_open() )
        throw KP::Error(CombineStrings(NOT_LOAD_FILE,str_out),__FUNCTION__);

    //Leemos la imagen original
    std::string str_image		= set.getSetDirectory() + KP::DIAG + std::string("Original.bmp");
    cv::Mat_<double>original	= cv::imread(str_image,0);
    if( original.empty() )
        throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_image),__FUNCTION__);

    //Preprocesamiento de la imagen original:
    cv::GaussianBlur(original,original,stc_size_blur,stc_sigma_blur);
    cv::minMaxLoc(original,&image_min,&image_max);
    xc = cv::Vec2d(original.rows/2.0,original.cols/2.0);


    //Ciclo principal para encontrar las transformaciones
    for(test=0; test<TEST; test++)
    {
        //Leemos la transformacion original
        in>>tt>>ptr_real_t[0]>>ptr_real_t[1]>>ptr_real_t[2]>>ptr_real_t[3]>>ptr_real_t[4]>>ptr_real_t[5];

        //Leemos la imagen con la que nos vamos a comparar
        std::string str_transform	= set.getSetDirectory() + KP::DIAG + result + Value_To_String(test) + extbmp;
        cv::Mat_<double> image_t	= cv::imread(str_transform,0);
        if( image_t.empty() )
            throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_transform),__FUNCTION__);

        //Seleccionamos una subregion
        image_t = cv::Mat_<double>(image_t,roi);

        //Removemos ruido de la imagen transformada
        cv::GaussianBlur(image_t,image_t,stc_size_blur,stc_sigma_blur);

        //Calculamos la intensidad maxima entre las imagenes y lo usamos para normalizar entre 0.0-Imax
        cv::minMaxLoc(image_t,&image_t_min,&image_t_max);

        imax	= std::max(image_max,image_t_max);
        nbins	= static_cast<int>(imax);

        //Estimamos los primeros valores
        taffin_result = kp.GetInicializeT().reshape(1,1);
        inicial_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);

        //Normalizamos el rango dinamico
        original 	= cv::NormalizeRange<double,double>(original,0.0,imax);
        image_t		= cv::NormalizeRange<double,double>(image_t,0.0,imax);

        //Creamos los niveles piramidales
        cv::buildPyramid(original,pyr_image,pyr_levels);
        cv::buildPyramid(image_t,pyr_image_t,pyr_levels);

        //Iteramos para cada nivel de la piramide
        int level_begin = pyr_image.size()-1;

        //Variables a medir
        duration		= 0.0;
        total_iters = 0;

        for(level = level_begin; level >= 0; level--)
        {
            kp2.Inicialize(pyr_image[level],pyr_image_t[level],sigmas,sigmasD,imax,nbinsD,npixs,epsdiff);

            if(!betas.empty())
            {
                kp2.setBeta(betas[level]);
            }
            DG<KPDistance2>optim(kp2,lambda,maxiters);


            start = clock();
            taffin_result 	= optim.minimize(taffin_result);
            finish	= clock();

            //Guardamos los valores
            duration		+= (double)(finish - start) / CLOCKS_PER_SEC;
            total_iters		+= maxiters;

        }

        //Escribimos los resultados
        end_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);
        if(end_affin <= 1.0) exitos++;

        //Escribimos los resultados
        out	<<test
               <<'\t'	<<inicial_affin
              <<'\t'	<<inicial_function
             <<'\t'	<<end_affin
            <<'\t'	<<end_function
           <<'\t'	<<total_iters
          <<'\t'	<<scape_iters
         <<'\t'	<<duration
        <<'\t'	<<cv::norm(taffin_result-real_t)
        <<'\t'	<<exitos
        <<std::endl;
    }
    delete membrecy;
    in.close();
    out.close();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void SetDistance2Test(StructTestDirectories&set,const int seed, const int npixs,
/// const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD, const int type,const int estimator,
/// const double lambda,const int pyr_levels, const int maxiters,const double epsdiff,
/// const double alpha)
///
/// @brief	Tests set distance 2.
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @exception	Error	Thrown when error.
///
/// @param [in,out]	set	The set.
/// @param	seed		The seed.
/// @param	npixs		The npixs.
/// @param	sigmas		The sigmas.
/// @param	sigmasD		The sigmas d.
/// @param	type		The type.
/// @param	estimator	The estimator.
/// @param	lambda		The lambda.
/// @param	pyr_levels	The pyr levels.
/// @param	maxiters	The maxiters.
/// @param	epsdiff		The epsdiff.
/// @param	alpha		The alpha.
////////////////////////////////////////////////////////////////////////////////////////////////////
void SetDistance3Test(const StructTestDirectories&set,const int seed,
                      const int npixs,const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD,
                      const int type,const int estimator,const double lambda,const int pyr_levels,
                      const int maxiters,const double epsdiff,const int membresia,const double alpha,
                      const int apertureSize, const double threshold1,const double threshold2,
                      const Border_Type b_type)
{
    //KP y optimization class
    KPDistance3 kp(seed,false,apertureSize,threshold1,threshold2,b_type);

    //Creamos la funcion de membresia
    Membrecy *membrecy = setMembrecy(membresia,alpha);
    kp.setMembrecy(membrecy);

    //Variables utiles
    int test		= 0;
    int TEST		= 0;
    int tt		= 0;
    int nbins	= 0;
    int level	= 0;
    double imax = 0.0;
    double image_min,image_max;
    double image_t_min,image_t_max;

    //Coordenadas del centro de la imagen y subregion
    cv::Vec2d xc;

    //Variables niveles piramidales
    std::vector<cv::Mat>pyr_image;
    std::vector<cv::Mat>pyr_image_t;

    //Factores de normalizacion
    const double factorD	= 255.0;
    const int	 nbinsD	= static_cast<int>(factorD);

    //Variables para analizar
    int exitos					= 0;
    int total_iters			= 0;
    int iters_level			= 0;
    int scape_iters			= 0;
    double inicial_affin		= 0.0;
    double inicial_function = 0.0;
    double end_affin			= 0.0;
    double end_function		= 0.0;
    double duration			= 0.0;

    cv::Mat_<double>real_t = cv::Mat_<double>::zeros(1,6);
    cv::Mat_<double>taffin_result;

    //Apuntador leer transformacion real
    double *ptr_real_t = real_t.ptr<double>(0);

    //Configuramos las entradas de cadena necesarias
    const std::string result("result");
    const std::string extbmp(".bmp");
    const std::string str_alpha = std::string("alpha") + KP::GUION + Value_To_String(alpha)
            +KP::GUION+std::string("m")+Value_To_String(membresia)+str_sigma_blur+str_window_blur;

    //Abrimos el archivo de transformaciones
    std::string str_in = set.getFileTName();
    std::ifstream in(str_in.c_str(),ios::in);
    if( !in.is_open() ) KP::Error(CombineStrings(NOT_LOAD_FILE,str_in),__FUNCTION__);
    //Leemos el numero de transformaciones
    in>>TEST;


    //Abrimos el archivo de escritura
    std::string aux = set.getOutputFileName() + std::string("_D3_") + str_alpha;
    std::string str_out = OutFileName2(aux,npixs,"_pyramidal_",DGE,type,estimator,apertureSize,
                                       threshold1,threshold2,b_type);
    std::ofstream out(str_out.c_str(),ios::out);
    if( !out.is_open() )
        throw KP::Error(CombineStrings(NOT_LOAD_FILE,str_out),__FUNCTION__);

    //Leemos la imagen original
    std::string str_image		= set.getSetDirectory() + KP::DIAG + std::string("Original.bmp");
    cv::Mat_<double>original	= cv::imread(str_image,0);
    if( original.empty() )
        throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_image),__FUNCTION__);

    //Preprocesamiento de la imagen original:

#ifdef median_filter
    cv::Mat_<uchar>uchar_original(original);
    cv::medianBlur(uchar_original,uchar_original,size_window);
    original = cv::Mat_<double>(uchar_original);
#else
    cv::GaussianBlur(original,original,stc_size_blur,stc_sigma_blur);
#endif
    cv::minMaxLoc(original,&image_min,&image_max);
    xc = cv::Vec2d(original.rows/2.0,original.cols/2.0);


    //Ciclo principal para encontrar las transformaciones
    for(test=0; test<TEST; test++)
    {
        //Leemos la transformacion original
        in>>tt>>ptr_real_t[0]>>ptr_real_t[1]>>ptr_real_t[2]>>ptr_real_t[3]>>ptr_real_t[4]>>ptr_real_t[5];

        //Leemos la imagen con la que nos vamos a comparar
        std::string str_transform	= set.getSetDirectory() + KP::DIAG + result + Value_To_String(test) + extbmp;
        cv::Mat_<double> image_t	= cv::imread(str_transform,0);
        if( image_t.empty() )
            throw KP::Error(CombineStrings(NOT_LOAD_IMAGE,str_transform),__FUNCTION__);

        //Seleccionamos una subregion
        image_t = cv::Mat_<double>(image_t,roi);

        //Removemos ruido de la imagen transformada
#ifdef median_filter
	cv::Mat_<uchar>uchar_image_t(image_t);
	cv::medianBlur(uchar_image_t,uchar_image_t,size_window);
	image_t = cv::Mat_<double>(uchar_image_t);
#else
	cv::GaussianBlur(image_t,image_t,stc_size_blur,stc_sigma_blur);
#endif

	//Calculamos la intensidad maxima entre las imagenes y lo usamos para normalizar entre 0.0-Imax
	cv::minMaxLoc(image_t,&image_t_min,&image_t_max);

	imax	= std::max(image_max,image_t_max);
	nbins	= static_cast<int>(imax);

	//Estimamos los primeros valores
	taffin_result = kp.GetInicializeT().reshape(1,1);
	inicial_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);

	//Normalizamos el rango dinamico
	original 	= cv::NormalizeRange<double,double>(original,0.0,imax);
	image_t		= cv::NormalizeRange<double,double>(image_t,0.0,imax);

	//Creamos los niveles piramidales
	cv::buildPyramid(original,pyr_image,pyr_levels);
	cv::buildPyramid(image_t,pyr_image_t,pyr_levels);

	//Inicializamos los parametros
	kp.setAllParameters(npixs,nbins,imax,type,epsdiff,sigmas);
	kp.setDistanceParameters(nbinsD,sigmasD,factorD);

	//Iteramos para cada nivel de la piramide
	int level_begin = pyr_image.size()-1;

	//Variables a medir
	duration		= 0.0;
	total_iters = 0;

	for(level = level_begin; level >= 0; level--)
	{
	    kp.InitializeDistance(pyr_image[level],pyr_image_t[level]);
	    Optimization optim(maxiters);
	    optim.setLambda(lambda);
	    taffin_result = optim.Minimize(kp,taffin_result,DGE);

	    //Guardamos los valores
	    duration		+= optim.getExecutionTime();
	    total_iters += (iters_level = optim.getIterations());

	}

	//Escribimos los resultados
	end_affin = AffinMeanError(original.size(),real_t,taffin_result,xc);
	if(end_affin <= 1.0) exitos++;

	//Escribimos los resultados
	out	<<test
	       <<'\t'	<<inicial_affin
	      <<'\t'	<<inicial_function
	     <<'\t'	<<end_affin
	    <<'\t'	<<end_function
	   <<'\t'	<<total_iters
	  <<'\t'	<<scape_iters
	 <<'\t'	<<duration
	<<'\t'	<<cv::norm(taffin_result-real_t)
	<<'\t'	<<exitos
	<<std::endl;
    }
    delete membrecy;
    in.close();
    out.close();
}

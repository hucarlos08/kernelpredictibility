#include <iostream>
#include <time.h>
#include <random>
#include <vector>


//OpenCV includes
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv4/opencv2/core.hpp>

#include <KPAffin.hpp>
#include <untImageOperations.hpp>
#include <untErrors.hpp>
#include <untTestSets.hpp>
#include <untMatFunctions.hpp>


using namespace std;
using namespace KP;

int main(int argc, char** argv)
{
    if(argc<2)
    {
        cerr<<"Set the image name"<<std::endl;
        return -1;
    }
    try
    {

        std::vector<std::string> argList(argv + 1, argv + argc);
        cv::Mat_<float>img    = cv::imread(argList[0], cv::IMREAD_GRAYSCALE );

        // Noise reduction
        cv::Mat_<float> filter;
        cv::bilateralFilter(img, filter,5, 20, 20);

        cv::Mat_<double>src;
        filter.convertTo(src, CV_64F);

        cv::Mat_<double>norm     = cv::NormalizeRange<double,double>(src,0.0,100);
        cv::Mat_<double>timage   = cv::ToneFunction<double>(norm, Tone_Function2);


        // Create an affine transformation with parameters
        const double theta  = DEGTORAD(37);
        const double alpha  = 0.8;
        const double betha  = 0.8;
        const double gamma  = 0.23;
        const double delta  = 0.23;
        const double tx     = 10.0;
        const double ty     = 10.0;

        //  Create the affine transformation and apply it to the image
        cv::Mat_<double>affin    = AffinT(theta, alpha, betha, gamma, delta, tx, ty);
        cv::Mat_<double>warp_dst = cv::Mat::zeros( timage.rows, timage.cols, timage.type() );

        affin = affin.reshape(0,1); // Reshape only for functions parameter options
        const cv::Vec2d xc(src.rows/2.0,src.cols/2.0); // Aplly the transformation around image center
        ApplyAffinT(timage, warp_dst, affin, xc);


        //Creamos los parametros empleados en la transformacion
        cv::Vec2d sigmas(10.0,10.0);
        int npixs	= 50;
        int maxiters	= 500;
        int pyr_levels	= 3;
        double lambda   = 0.1;
        double factor   = 100.0;

        //Normalizamos las imagenes al rango dinamico 0-factor
        src       = cv::NormalizeRange<double,double>(src,0.0,factor);
        warp_dst  = cv::NormalizeRange<double,double>(warp_dst,0.0,factor);
        timage    = cv::NormalizeRange<double,double>(timage,0.0,factor);

        // Select the ROI
        cv::Size roi_size(300,300);
        int r_x = static_cast<int>(xc(0) - (roi_size.width/2));
        int r_y = static_cast<int>(xc(1) - (roi_size.height/2));

        cv::Point r_org(r_x, r_y);
        cv::Rect rect(r_org, roi_size);

        cv::Mat_<double>src_roi = src(rect);
        cv::Mat_<double>warp_roi = warp_dst(rect);

        cv::Mat_<double> taffin_result = RegisterAffine(src_roi,warp_roi,sigmas,factor,npixs,lambda,maxiters,pyr_levels);

        // Apply transformation and select ROI
        cv::Mat_<double>result = cv::Mat::zeros( src.rows, src.cols, src.type() );
        ApplyAffinT(src, result, taffin_result, xc);
        cv::Mat_<double>result_roi = result(rect);

        // Show the results
        std::cout<<"Original: "<<affin<<std::endl;
        std::cout<<"KP transformation: "<<taffin_result<<endl;

        std::vector<cv::Mat_<double> >images;

        images.push_back(src_roi);
        images.push_back(warp_roi);
        images.push_back(result_roi);

        cv::Mat_<double>display;
        cv::hconcat(images, display);

        display   = cv::NormalizeRange<double,double>(display,0.0,1.0);

        //cv::normalize(result, result, 1.0, 0.0, cv::NORM_MINMAX);
        cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE ); // Create a window for display.
        cv::imshow( "Display window", display);                // Show our image inside it.
        cv::waitKey(0); // Wait for a keystroke in the window
        cv::destroyAllWindows();

    }
    catch(Error&error)
    {
        error.getError(std::cout);
        return -1;
    }
    return 0;
}


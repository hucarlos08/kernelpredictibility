#include <iostream>
#include <time.h>
#include <random>
#include <vector>


//OpenCV includes
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv4/opencv2/core.hpp>

#include <KPAffin.hpp>
#include <untImageOperations.hpp>
#include <untErrors.hpp>
#include <untTestSets.hpp>
#include <untMatFunctions.hpp>


using namespace std;
using namespace KP;

int main(int argc, char** argv)
{
    if(argc<2)
    {
        cerr<<"Set the image name"<<std::endl;
        return -1;
    }
    try
    {

        std::vector<std::string> argList(argv + 1, argv + argc);
        cv::Mat_<double>src    = cv::imread(argList[0], cv::IMREAD_GRAYSCALE );

        // Create an affine transformation with parameters
        const double theta  = DEGTORAD(41);
        const double alpha  = 1.0;
        const double betha  = 1.0;
        const double gamma  = 0.0;
        const double delta  = 0.0;
        const double tx     = 10.0;
        const double ty     = 10.0;

        //  Create the affine transformation and apply it to the image
        cv::Mat_<double>affin = AffinT(theta, alpha, betha, gamma, delta, tx, ty);
        cv::Mat_<double>warp_dst = cv::Mat::zeros( src.rows, src.cols, src.type() );

        affin = affin.reshape(0,1); // Reshape only for functions parameter options
        const cv::Vec2d xc(src.rows/2.0,src.cols/2.0); // Aplly the transformation around image center
        ApplyAffinT(src, warp_dst, affin, xc);


        //Creamos los parametros empleados en la transformacion
        cv::Vec2d sigmas(10.0,10.0);
        int npixs	= 200;
        int maxiters	= 600;
        int pyr_levels	= 3;
        double lambda   = 0.2;
        double factor   = 255.0;
        double sigma    = 1.0;
        cv::Size wsize  = cv::Size(3,3);

        cv::Mat_<double> timage = cv::ToneFunction<double>(src, Tone_Function0);

        //Normalizamos las imagenes al rango dinamico 0-255
        src       = cv::NormalizeRange<double,double>(src,0.0,factor);
        warp_dst  = cv::NormalizeRange<double,double>(warp_dst,0.0,factor);
        timage    = cv::NormalizeRange<double,double>(timage,0.0,factor);

        cv::Mat_<double> taffin_result = RegisterAffine(timage,warp_dst,sigmas,factor,npixs,lambda,maxiters,pyr_levels);


        cv::Mat_<double>warp_dst2 = cv::Mat::zeros( src.rows, src.cols, src.type() );

        ApplyAffinT(timage, warp_dst2, taffin_result, xc);

        // Show the results
        std::cout<<"Original: "<<affin<<std::endl;
        std::cout<<"KP transformation: "<<taffin_result<<endl;

        cv::Mat_<double>result;
        cv::hconcat(timage, warp_dst2, result);

        cv::normalize(result, result, 1.0, 0.0, cv::NORM_MINMAX);
        cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE ); // Create a window for display.
        cv::imshow( "Display window", result);                // Show our image inside it.
        cv::waitKey(0); // Wait for a keystroke in the window
        cv::destroyAllWindows();

    }
    catch(Error&error)
    {
        error.getError(std::cout);
        return -1;
    }
    return 0;
}


using StaticArrays
using Interpolations

function affineT(params::AbstractArray, offset::AbstractArray)

    θ   = params[1]
    sx  = params[2]
    sy  = params[3]
    tx  = params[4]
    ty  = params[5]

    off  = @SVector[offset[1], offset[2]]

    tras = @SVector[tx, ty]
    rot  = SMatrix{2, 2}([ cos(θ), sin(θ),
                          -sin(θ), cos(θ)])

    scale = SMatrix{2, 2}([sx, zero(eltype(params)), zero(eltype(params)), sy])

    M = scale * rot

    t = off - M*off + tras

    return M, t

end

function mywarp(params::Vector{Float64}, original::Matrix{Float64})

    w, h        = size(original)
    M, v        = affineT(params, [w/2., h/2.])
    image_warp  = warp(original, AffineMap(M, v), axes(original))

    return image_warp

end

function affineTransform!(params::AbstractArray, itp::AbstractInterpolation, out::AbstractMatrix)

    rows, cols  = size(itp)

    off_x       = convert(eltype(params), rows/2.0)
    off_y       = convert(eltype(params), cols/2.0)

    M, t        = affineT(params, [off_x, off_y])

    # Cosntruct the inverse transform
    Mi = inv(M)
    ti = -Mi*t

    tform = AffineMap(Mi, ti)

    nan = convert(eltype(params), NaN)


    @simd for I in CartesianIndices(axes(out))
        x,y = trunc.(Int64, tform(SVector(I.I)))
        if (1<=x<=rows) && (1<=y<=cols)
            @inbounds out[I] = itp(x,y)
        else
            @inbounds out[I] = nan
        end
    end

end


function parallel_affineTransform!(params::AbstractArray, itp::AbstractInterpolation, out::AbstractMatrix)

    rows, cols  = size(itp)

    off_x       = convert(eltype(params), rows/2.0)
    off_y       = convert(eltype(params), cols/2.0)

    M, t        = affineT(params, [off_x, off_y])

    # Cosntruct the inverse transform
    Mi = inv(M)
    ti = -Mi*t

    tform = AffineMap(Mi, ti)

    nan = convert(eltype(params), NaN)

    Threads.@threads for I in CartesianIndices(axes(out))
        x,y = trunc.(Int64, tform(SVector(I.I)))
        if (1<=x<=rows) && (1<=y<=cols)
            @inbounds out[I] = itp(x,y)
        else
            @inbounds out[I] = nan
        end
    end

end


function affineTransform(params::AbstractArray, image::AbstractMatrix)

    itp = interpolate(image, BSpline(Linear()))
    out = zeros(eltype(image), size(image))

    affineTransform!(params, itp, out)

    return out

end



function affineError(real::AbstractArray, estimated::AbstractArray, rows::Int64, cols::Int64)

    # Create the affine transformation
    off_x       = convert(eltype(real), rows/2.0)
    off_y       = convert(eltype(real), cols/2.0)

    M_real, t_real  = affineT(real, [off_x, off_y])
    M_est, t_est    = affineT(estimated, [off_x, off_y])

    error = zero(eltype(real))

    tform1 = AffineMap(M_real, t_real)
    tform2 = AffineMap(M_est,  t_est)

    @simd for I in CartesianIndices((rows,cols))
        x_real, y_real = trunc.(Int64, tform1(SVector(I.I)))
        x_est,  y_est  = trunc.(Int64, tform2(SVector(I.I)))

        error += (x_real - x_est)*(x_real - x_est) + (y_real - y_est)*(y_real - y_est)
    end

    return error/(rows*cols)

end

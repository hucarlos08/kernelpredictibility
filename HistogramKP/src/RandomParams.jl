module RandomParameters

export randomInterval, randomParameters

function randomInterval(bounds::Tuple{AbstractFloat, AbstractFloat}, N::Int64)
    a = bounds[1]
    b = bounds[2]
    return ((b-a) .* rand(eltype(bounds[1]), N)) .+ a;
end



function randomParameters(bounds::Vector{Tuple{Float64, Float64}}, N::Int64)

    params_count = size(bounds)[1]

    result = zeros(Float64, params_count, N)
    for (index, bound) in enumerate(bounds)
        result[index, :] = randomInterval(bound, N)
    end
    return result
end

function randomParameters(bounds::Vector{Tuple{Float32, Float32}}, N::Int64)

    params_count = size(bounds)[1]

    result = zeros(Float32, params_count, N)
    for (index, bound) in enumerate(bounds)
        result[index, :] = randomInterval(bound, N)
    end
    return result
end


end # module

using Distributed

@inline function minmax3(v::AbstractFloat, vmin::AbstractFloat, vmax::AbstractFloat)

    return isnan(v) ?  (vmin, vmax) :  (min(v, vmin), max(v,vmax))
end

function findMinMax(array::AbstractArray)
    #Find minim and maximum
    minv =  convert(eltype(array),  Inf)
    maxv =  convert(eltype(array), -Inf)
    @inbounds for value in array
        minv, maxv = minmax3(value, minv, maxv)
    end
    return minv, maxv
end

function findMinMax(array1::AbstractArray, array2::AbstractArray)

    # Find minimum and maximun for every input
    xmin =  convert(eltype(array),  Inf)
    xmax =  convert(eltype(array), -Inf)

    ymin =  convert(eltype(array),  Inf)
    ymax =  convert(eltype(array), -Inf)

    @inbounds for i in eachindex(array1)
        xmin, xmax = minmax3(array1[i], xmin, xmax)
        ymin, ymax = minmax3(array2[i], ymin, ymax)

    end

    return (xmin, xmax), (ymin, ymax)
end

@inline function getbin(v::AbstractFloat, min::AbstractFloat, max::AbstractFloat, norm::AbstractFloat)
    bin = -1
    if !isnan(v) && (min <= v <= max)
        fbin  = ((v - min) * norm) + 1.0
        bin   = trunc(Int64, fbin)
    end
    return bin
end


function histogram1D!(array::AbstractArray, weights::AbstractArray, nbins::Int64,
    min::AbstractFloat, max::AbstractFloat)

    fill!(weights, zero(eltype(array)))
    norm    = nbins / (max - min) ;

    @simd for value in array
        bin = getbin(value, min, max, norm)
        if bin != -1
            @inbounds weights[bin] += 1.;
        end
    end

end


function histogram1D!(array::AbstractArray, weights::AbstractArray, nbins::Int64)

    min, max = findMinMax(array)
    histogram1D!(array, weights, nbins, min, max)

end

function histogram1D(array::AbstractArray, nbins::Int64, min::AbstractFloat, max::AbstractFloat)

    weights = zeros(eltype(array), nbins)
    histogram1D!(array, weights, nbins, min, max)

    return weights

end


function histogram1D(array::AbstractArray, nbins::Int64)

    weights = zeros(eltype(array), nbins)
    histogram1D!(array, weights, nbins)
    return weights

end


function jointHistogram!(array1::AbstractArray, array2::AbstractArray, weights::AbstractMatrix, nbins::Int64,
    xmin::AbstractFloat,
    xmax::AbstractFloat,
    ymin::AbstractFloat,
    ymax::AbstractFloat)


    fill!(weights, zero(eltype(weights)))

    normx = nbins/(xmax-xmin)
    normy = nbins/(ymax-ymin)

    indexx = 0
    indexy = 0

    @simd for i in eachindex(array1)

        indexx = getbin(array1[i], xmin, xmax, normx)
        indexy = getbin(array2[i], ymin, ymax, normy)

        if (indexx != -1) && (indexy != -1)
            @inbounds weights[indexx, indexy] += 1.0
        end
    end

end


function jointHistogram!(array1::AbstractArray, array2::AbstractArray, weights::AbstractMatrix, nbins::Int64)

    (xmin, xmax), (ymin, ymax) = findMinMax(array1, array2)
    jointHistogram!(array1, array2, weights, nbins, xmin, xmax, ymin, ymax)

end

function jointHistogram(array1::AbstractArray, array2::AbstractArray, nbins::Int64,
    xmin::AbstractFloat,
    xmax::AbstractFloat,
    ymin::AbstractFloat,
    ymax::AbstractFloat)

    weights = zeros(eltype(array1), nbins, nbins)
    jointHistogram!(array1, array2, weights, nbins, xmin, xmax, ymin, ymax)
    return weights

end

function jointHistogram(array1::AbstractArray, array2::AbstractArray, nbins::Int64)

    weights = zeros(eltype(array1), nbins, nbins)
    jointHistogram!(array1, array2, weights, nbins)
    return weights

end


function multipleHistograms!(array1::AbstractArray, array2::AbstractArray,
    weights1::AbstractArray,
    weights2::AbstractArray,
    joint::AbstractMatrix,
    nbins::Int64,
    xmin::AbstractFloat,
    xmax::AbstractFloat,
    ymin::AbstractFloat,
    ymax::AbstractFloat
    )

    fill!(weights1, zero(eltype(array1)))
    fill!(weights2, zero(eltype(array2)))
    fill!(joint,    zero(eltype(joint)))

    normx = nbins/(xmax-xmin)
    normy = nbins/(ymax-ymin)

    indexx = 0
    indexy = 0

    @simd for i in eachindex(array1)

        indexx = getbin(array1[i], xmin, xmax, normx)
        indexy = getbin(array2[i], ymin, ymax, normy)

        if indexx != -1
            @inbounds weights1[indexx] += 1.0
        end

        if indexy != -1
            @inbounds weights2[indexy] += 1.0
        end


        if (indexx != -1) && (indexy != -1)
            @inbounds joint[indexx, indexy] += 1.0
        end
    end
end


function multipleHistograms(array1::AbstractArray, array2::AbstractArray, nbins::Int64,
    xmin::AbstractFloat,
    xmax::AbstractFloat,
    ymin::AbstractFloat,
    ymax::AbstractFloat)


    weight_ref  = zeros(eltype(array1), nbins)
    weight_tar  = zeros(eltype(array2), nbins)
    joint       = zeros(eltype(array1), nbins, nbins)

    multipleHistograms!(array1, array2, weight_ref, weight_tar, joint, nbins, xmin, xmax, ymin, ymax)

    return weight_ref, weight_tar, joint

end

function multipleHistograms(array1::AbstractArray, array2::AbstractArray, nbins::Int64)

    (xmin, xmax), (ymin, ymax) = findMinMax(array1, array2)

    return multipleHistograms(array1, array2, nbins, xmin, xmax, ymin, ymax)
end


function parallel_histogram1D(array::AbstractArray, nbins::Int64, min::AbstractFloat, max::AbstractFloat)

    nt          = Threads.nthreads()
    len, rem    = divrem(length(array), nt)
    chunks      = [view(array,(1:len) .+ (t-1)*len) for  t in 1:nt]

    partials    = pmap(x->histogram1D(x, nbins, min, max), chunks, distributed=false)


    return sum(partials)

end

function parallel_jointHistogram(array1::AbstractArray, array2::AbstractArray, nbins::Int64,
    xmin::AbstractFloat,
    xmax::AbstractFloat,
    ymin::AbstractFloat,
    ymax::AbstractFloat)

    nt          = Threads.nthreads()
    len, rem    = divrem(length(array1), nt)

    chunks      = [(view(array1,(1:len) .+ (t-1)*len), view(array2, (1:len) .+ (t-1)*len)) for  t in 1:nt]

    g(x)        = jointHistogram(x[1], x[2], nbins, xmin, xmax, ymin, ymax)


    partials    = pmap(g, chunks, distributed=false)

    return sum(partials)

end

function parallel_multipleHistograms(array1::AbstractArray, array2::AbstractArray, nbins::Int64,
    xmin::AbstractFloat,
    xmax::AbstractFloat,
    ymin::AbstractFloat,
    ymax::AbstractFloat)

    nt          = Threads.nthreads()
    len, rem    = divrem(length(array1), nt)

    chunks      = [(view(array1,(1:len) .+ (t-1)*len), view(array2, (1:len) .+ (t-1)*len)) for  t in 1:nt]

    g(x)         = multipleHistograms(x[1], x[2], nbins, xmin, xmax, ymin, ymax)

    partials    = pmap(g, chunks, distributed=false)

    return sum(partials)

end

module HistogramKP

using CoordinateTransformations
using Images
using Interpolations
using Reexport

include("Histogram.jl")
include("AffineTransformation.jl")
include("RandomParams.jl")
@reexport using .RandomParameters

export

    # Histograms
    findMinMax,
    minmax3,
    getbin,
    histogram1D!,
    histogram1D,
    jointHistogram!,
    jointHistogram,
    multipleHistograms!,
    multipleHistograms,
    parallel_histogram1D,
    parallel_jointHistogram,
    parallel_multipleHistograms,


    # Affine Transformations
    affineT,
    mywarp,
    affineTransform!,
    parallel_affineTransform!,
    affineTransform,
    affineError,

    # Histogram KP
    similarityHKP,
    similarityIntensity!,
    similarityIntensity,
    similarityPhase!,
    similarityPhase,
    similarityAdditive,
    createSKPMemory,

    # For sampling over test
    randomInterval,
    randomParameters


# Outter constructor
function createSKPMemory(reference::AbstractMatrix, nbins::Int64, out::AbstractMatrix,
    wr::AbstractVector,
    wj::AbstractVector,
    joint::AbstractMatrix,
    itp::AbstractInterpolation)

    out     = zeros(eltype(reference), size(reference))

    wr      = zeros(eltype(reference),  nbins)
    wt      = zeros(eltype(reference),     nbins)
    joint   = zeros(eltype(reference),  nbins, nbins)

    itp     = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

end


function similarityHKP(weights1::AbstractArray, weights2::AbstractArray, joint::AbstractMatrix, nbins::Int64)

    norm_reference  = zero(eltype(weights1))
    norm_target     = zero(eltype(weights2))
    norm_joint      = zero(eltype(joint))

    result_reference    = zero(eltype(weights1))
    result_target       = zero(eltype(weights2))
    result_joint        = zero(eltype(joint))

    val_reference   = zero(eltype(weights1))
    val_target      = zero(eltype(weights2))
    val_joint       = zero(eltype(joint))

    @simd for j = 1:nbins
        @inbounds val_reference   = weights1[j]
        @inbounds val_target      = weights2[j]

        norm_reference  += val_reference
        norm_target     += val_target

        result_reference    += (val_reference * val_reference) - val_reference
        result_target       += (val_target * val_target) - val_target

        @simd for i = 1:nbins

            @inbounds val_joint      = joint[i, j]
            norm_joint              += val_joint
            result_joint            += (val_joint * val_joint) - val_joint
        end
    end

    hkp_reference   = result_reference / (norm_reference * norm_reference)
    hkp_target      = result_target / (norm_target * norm_target)
    hkp_joint       = result_joint / (norm_joint * norm_joint)

    return hkp_reference + hkp_target - (hkp_joint + hkp_joint)
end


function similarityIntensity!(
    params::AbstractArray,
    reference::AbstractMatrix,
    target::AbstractMatrix,
    nbins::Int64,
    # Avoid memory re-allocation for histograms
    weights_ref::AbstractArray,
    weights_tar::AbstractArray,
    joint::AbstractMatrix,
    bounds::Vector{Tuple{Float64, Float64}},
    # Avoid memory re-allocation for image warp
    itp::AbstractInterpolation,
    out::AbstractMatrix
)
    # Create the affine transformation
    parallel_affineTransform!(params, itp, out)

    xmin, xmax = bounds[1]
    ymin, ymax = bounds[2]

    multipleHistograms!(vec(out), vec(target),
    weights_ref, weights_tar, joint, nbins,
    xmin, xmax, ymin, ymax)

    return similarityHKP(weights_ref, weights_tar, joint, nbins)

end


function similarityIntensity(
    params::AbstractArray,
    reference::AbstractMatrix,
    target::AbstractMatrix,
    nbins::Int64,
    bounds::Vector{Tuple{Float64, Float64}},
    itp::AbstractInterpolation
    )

    # Create all the necessary StaticArrays
    weight_ref  = zeros(eltype(reference), nbins)
    weight_tar  = zeros(eltype(target),    nbins)
    joint       = zeros(eltype(target), nbins, nbins)

    out         = zeros(eltype(reference), size(reference))

    xmin, xmax = bounds[1]
    ymin, ymax = bounds[2]

    # Create the affine transformation
    affineTransform!(params, itp, out)
    multipleHistograms!(vec(out), vec(target), weight_ref, weight_tar, joint, nbins,xmin, xmax, ymin, ymax)

    return similarityHKP(weight_ref, weight_tar, joint, nbins)

end


function similarityPhase!(
    params::AbstractArray,
    reference::AbstractMatrix,
    target_phase::AbstractMatrix,
    nbins::Int64,
    # Avoid memory re-allocation for histograms
    weights_ref::AbstractArray,
    weights_tar::AbstractArray, # Border images
    joint::AbstractMatrix,
    bounds::Vector{Tuple{Float64, Float64}},
    # Avoid memory re-allocation for image warp
    itp::AbstractInterpolation,
    out::AbstractMatrix
    )

    # Create the affine transformation
    parallel_affineTransform!(params, itp, out)
    dx_ref, dy_ref  = imgradients(out, KernelFactors.ando5, "replicate")
    img_phase_ref   = phase(dx_ref, dy_ref)

    xmin, xmax = bounds[1]
    ymin, ymax = bounds[2]

    multipleHistograms!(vec(img_phase_ref), vec(target_phase), weights_ref, weights_tar, joint, nbins,
    xmin, xmax, ymin, ymax)

    return similarityHKP(weights_ref, weights_tar, joint, nbins)

end


function similarityPhase(
    params::AbstractArray,
    reference::AbstractMatrix,
    target_phase::AbstractMatrix,
    nbins::Int64,
    bounds::Vector{Tuple{Float64, Float64}},
    itp::AbstractInterpolation,
)

    # Create all the necessary StaticArrays
    weights_ref  = zeros(eltype(reference),    nbins)
    weights_tar  = zeros(eltype(target_phase), nbins)
    joint        = zeros(eltype(target_phase), nbins, nbins)

    out          = zeros(eltype(reference), size(reference))


    # Create the affine transformation
    affineTransform!(params, itp, out)
    dx_ref, dy_ref  = imgradients(out, KernelFactors.ando5, "replicate")
    img_phase_ref   = phase(dx_ref, dy_ref)


    multipleHistograms!(vec(img_phase_ref), vec(target_phase), weights_ref, weights_tar, joint, nbins,
    bounds[1][1], bounds[1][2], bounds[2][1], bounds[2][2])

    return similarityHKP(weights_ref, weights_tar, joint, nbins)
end


function similarityAdditive(
    params::AbstractArray,
    reference::AbstractMatrix,
    target::AbstractMatrix,
    target_phase::AbstractMatrix,
    nbins::Int64,
    bounds::Vector{Tuple{Float64, Float64}},
    itp::AbstractInterpolation
    )

    # Create all the necessary StaticArrays
    weight_ref  = zeros(Float64, nbins)
    weight_tar  = zeros(Float64, nbins)
    joint       = zeros(Float64, nbins, nbins)

    out         = zeros(Float64, size(reference))

    # SKP intensity

    # Same bound for all the intensity calculations
    xmin, xmax = bounds[1]
    ymin, ymax = bounds[1]

    affineTransform!(params, itp, out)
    multipleHistograms!(vec(out), vec(target), weight_ref, weight_tar, joint, nbins,
    xmin, xmax, ymin, ymax)


    skpint = similarityHKP(weight_ref, weight_tar, joint, nbins)


    # SKP phase
    # Create the phase image for target
    dx_ref, dy_ref  = imgradients(out, KernelFactors.ando5, "replicate")
    img_phase_ref   = phase(dx_ref, dy_ref)

    xmin, xmax = bounds[2]
    ymin, ymax = bounds[2]

    multipleHistograms!(vec(img_phase_ref), vec(target_phase), weight_ref, weight_tar, joint, nbins,
    xmin, xmax, ymin, ymax)

    skpphase = similarityHKP(weight_ref, weight_tar, joint, nbins)

    return skpint + skpphase

end

end # module

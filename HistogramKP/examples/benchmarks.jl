using Random
using Images
using Plots
using BlackBoxOptim
using LinearAlgebra
using Interpolations
using CoordinateTransformations
using BenchmarkTools
using HistogramKP
using StaticArrays
using SharedArrays
using Distributed

samples_count = 10000000

nbins = 100
samples = rand(Float64, samples_count)


@benchmark histogram1D($samples,nbins, 0.0, 1.0)
@benchmark parallel_histogram1D($samples, nbins, 0.0, 1.0)

@benchmark jointHistogram($samples, $samples, nbins, 0.0, 1.0, 0.0, 1.0)
@benchmark parallel_jointHistogram($samples, $samples, nbins, 0.0, 1.0, 0.0, 1.0)


# Check performance and consitency of multipleHistograms
weight_ref  = zeros(Float64, nbins)
weight_tar  = zeros(Float64, nbins)
joint       = zeros(Float64, nbins, nbins)

@code_warntype multipleHistograms!(samples, samples, weight_ref, weight_tar, joint, nbins, 0.0, 1.0, 0.0, 1.0)
@benchmark multipleHistograms!(samples, samples, weight_ref, weight_tar, joint, nbins, 0.0, 1.0, 0.0, 1.0)

@code_warntype parallel_multipleHistograms(samples, samples, nbins, 0.0, 1.0, 0.0, 1.0)

@benchmark parallel_multipleHistograms(samples, samples, nbins, 0.0, 1.0, 0.0, 1.0)

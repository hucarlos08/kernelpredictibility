##
using Random
using Images
using Plots
using BlackBoxOptim
using LinearAlgebra
using HistogramKP
using Interpolations
using FileIO

plotly()

##
# Load the image and convert to gray scale
img = (load("images/128/lena.jpeg"))
img = Gray.(img)

# Definition of vectorized tone function
tone(i) = @. (1.0 - i)^(1.35)

target = tone(img)

#Create a sampling interval
imgbounds =    [
                (deg2rad(-50.0), deg2rad(50.0)),    # θ parameter
                (0.5, 1.5),                     # λₓ
                (0.5, 1.5),                     # λ𝚈
                (-50.0, 50.0),                  # dx
                (-50.0, 50.0)                   # dy
            ]


affine_params = randomParameters(imgbounds, 1)

# Apply the affine transformation
target = affineTransform(affine_params[:,1], tone(img))

reference   = Float64.(img)
target      = Float64.(target)
nbins       = 100

Gray.(target)

##
# Create all the necessary StaticArrays
weight_ref  = zeros(Float64, nbins)
weight_tar  = zeros(Float64, nbins)
joint       = zeros(Float64, nbins, nbins)

bounds_int  = [(0.0,1.0),(0.0,1.0)]

itp         = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

out         = zeros(Float64, size(img))

w, h        = size(img)

f1(x) = similarityIntensity!(x,reference, target, nbins, weight_ref, weight_tar, joint, bounds_int, itp, out)
@time result1 = bboptimize(
    f1;
    SearchRange = imgbounds,
    Method = :de_rand_2_bin_radiuslimited,
    MaxTime = 30.0,
    PopulationSize=50,
)

mean_error1  = affineError(best_candidate(result1), affine_params[:,1], w, h)
diff1        = norm(best_candidate(result1) - affine_params[:,1])


##
# Create the phase image for target
dx_tar, dy_tar  = imgradients(target, KernelFactors.ando5, "replicate")
img_phase_tar   = phase(dx_tar, dy_tar)
bounds_phase    = [(-pi, convert(Float64, pi)), (-pi, convert(Float64, pi))]


f2(x) = similarityPhase!(x,reference, img_phase_tar, nbins, weight_ref, weight_tar, joint, bounds_phase, itp, out)

@time result2 = bboptimize(
    f2;
    SearchRange = imgbounds,
    Method = :de_rand_2_bin_radiuslimited,
    MaxTime = 30.0,
    PopulationSize=50
)

mean_error2  = affineError(best_candidate(result2), affine_params[:,1], w, h)
diff2        = norm(best_candidate(result2) - affine_params[:,1])


##
f3(x) = f1(x) + f2(x)

@time result3 = bboptimize(
    f3;
    SearchRange = imgbounds,
    Method = :de_rand_2_bin_radiuslimited,
    MaxTime = 30.0,
    PopulationSize=50
)

mean_error3  = affineError(best_candidate(result3), affine_params[:,1], w, h)
diff3        = norm(best_candidate(result3) - affine_params[:,1])

##

println("Error SKPI: $mean_error1, SPKP: $mean_error2, (SKP1 +SKPP): $mean_error3")

imgs = Gray.(hcat(target, out))

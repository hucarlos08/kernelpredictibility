using Random
using Images
using Plots
using Optim
using LinearAlgebra
using HistogramKP

plotly()

##
# Load the image and convert to gray scale
img = (load("images/128/lena.jpeg"))
img = Gray.(img)

# Definition of vectorized tone function
tone(i) = @. (1.0 - i)^(1.35)

target = tone(img)

#Create a sampling interval
imgbounds =    [
                (deg2rad(-50.0), deg2rad(50.0)),    # θ parameter
                (0.5, 1.5),                     # λₓ
                (0.5, 1.5),                     # λ𝚈
                (-50.0, 50.0),                  # dx
                (-50.0, 50.0)                   # dy
            ]


affine_params = randomParameters(imgbounds, 1)

# Apply the affine transformation
target = affineTransform(affine_params[:,1], tone(img))

reference   = Float64.(img)
target      = Float64.(target)
nbins       = 100

Gray.(target)

##
# Create all the necessary StaticArrays
weight_ref  = zeros(Float64, nbins)
weight_tar  = zeros(Float64, nbins)
joint       = zeros(Float64, nbins, nbins)

bounds_int  = [(0.0,1.0),(0.0,1.0)]

itp         = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

out         = zeros(Float64, size(img))

w, h        = size(img)

##

f1(x) = similarityIntensity!(x,reference, target, nbins, weight_ref, weight_tar, joint, bounds_int, itp, out)

##
# Create the phase image for target
dx_tar, dy_tar  = imgradients(target, KernelFactors.ando5, "replicate")
img_phase_tar   = phase(dx_tar, dy_tar)
bounds_phase    = [(-pi, convert(Float64, pi)), (-pi, convert(Float64, pi))]

f2(x) = similarityPhase!(x,reference, img_phase_tar, nbins, weight_ref, weight_tar, joint, bounds_phase, itp, out)

##
f3(x) = f1(x) + f2(x)

##
x0 = [0.0, 1.0, 1.0, 0.0, 0.0]
res = optimize(f3,
               x0,
               NewtonTrustRegion(),
               Optim.Options(g_tol = 1e-12,
                             iterations = 500,
                             store_trace = true,
                             show_trace = true,
                             allow_f_increases=true))
Optim.minimizer(res)

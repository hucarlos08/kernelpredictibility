using Random
using Images
using HistogramKP
using Interpolations


# Load the image and convert to gray scale
img = (load("images/128/Lena.jpeg"))
img = Gray.(img)

# Definition of vectorized tone function
tone(i) = @. (1.0 - i)^(1.35)

target = tone(img)

#Create a sampling interval
imgbounds =    [
                (deg2rad(-20), deg2rad(20)),    # θ parameter
                (0.9, 1.1),                     # λₓ
                (0.9, 1.1),                     # λ𝚈
                (-20.0, 20.0),                  # dx
                (-20.0, 20.0)                   # dy
            ]

# Apply the affine transformation
affine_params = randomParameters(imgbounds, 1)

target = affineTransform(affine_params[:,1], tone(img))

target      = Float64.(target)
reference   = Float64.(img)
nbins       = 100

itp         = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

# Crete N random transformation to compare
N       = 100
points  = randomParameters(imgbounds, N)

## Test Similarity based on intensities

println("Test for parallel Similarity based on intensities")
fi(x)    = similarityIntensity(x,reference, target, nbins, [(0.0, 1.0), (0.0, 1.0)], itp)

# Serial test
println("\t Serial time")
@time single_threaded = map(i -> fi(points[:, i]), 1:N)

# multi_threaded test
println("\t Parallel time")
multi_threaded = zeros(Float64, N)
@time Threads.@threads for i in 1:N
    multi_threaded[i] = fi(points[:, i])
end

@assert sum((single_threaded .- multi_threaded).^2) == 0


## Test Similarity based on gradient

println("Test for parallel Similarity based on phase")

fp(x) = similarityPhase(x,reference, target, nbins, [(0.0, 1.0), (0.0, 1.0)], itp)

# Serial test
println("\t Serial time")
@time single_threaded = map(i -> fp(points[:, i]), 1:N)

# multi_threaded test
println("\t Parallel time")
multi_threaded = zeros(Float64, N)
@time Threads.@threads for i in 1:N
    multi_threaded[i] = fp(points[:, i])
end

@assert sum((single_threaded .- multi_threaded).^2) == 0

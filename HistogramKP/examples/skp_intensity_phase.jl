using Random
using Images
using Plots
using BlackBoxOptim
using LinearAlgebra
using HistogramKP
using StatsPlots
using Interpolations

pyplot()

path = "images/128/"
image_name = "lena.jpeg"

clearconsole()

println("Running example for image: $image_name")

# Load the image and convert to gray scale
img = (load(path * image_name))
img = Gray.(img)

# Definition of vectorized tone function
tone(i) = @. (1.0 - i)^(1.35)

target = tone(img)

#Create a sampling interval
imgbounds =    [
                (deg2rad(-50), deg2rad(50)),    # θ parameter
                (0.5, 1.5),                     # λₓ
                (0.5, 1.5),                     # λ𝚈
                (-50.0, 50.0),                  # dx
                (-50.0, 50.0)                   # dy
            ]


reference   = Float64.(img)
target      = Float64.(target)
nbins       = 100 # Number of bins
max_t       = 30 # Maximum optimization time

N           = 50 # Number of samples for test

p_size      = 50

# Create the random transformations
random_affine  = randomParameters(imgbounds, N)

##
function stadisticsAffine(input::Matrix{Float64}, similarity::Function, bounds::Vector{Tuple{Float64, Float64}},
    ftransform::Function)

    rows, cols  = size(input)
    diff        = zeros(cols)

    itp         = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

    Threads.@threads for i in 1:cols

        # Create the random image
        target_i = ftransform(input[:,i], target)

        f(x)     = similarity(x,reference, target_i, nbins, bounds, itp) # For test intensity or phase

        result   = bboptimize(f; SearchRange = imgbounds, MaxTime = max_t,
                    TraceMode = :silent, PopulationSize=p_size)

        min_val = best_candidate(result)
        diff[i] = norm(min_val-input[:, i])

        tid = Threads.threadid()
        println("Finish experiment $i, from thread $tid")
    end
    return diff
end

function imageIntensity(input::AbstractVector,img::AbstractMatrix)

    return result  = affineTransform(input[:,i], target)
end


function imagePhase(input::AbstractVector,img::AbstractMatrix)

    target_i = affineTransform(input, img)

    # Create the phase image for target
    dx_tar, dy_tar  = imgradients(target_i, KernelFactors.ando5, "replicate")
    result          = phase(dx_tar, dy_tar)

    return result

end


bound_int       = [(0.0, 1.0), (0.0, 1.0)]
bounds_phase    = [(-pi, convert(Float64, pi)), (-pi, convert(Float64, pi))]

@time result_phase = stadisticsAffine(random_affine, similarityPhase, bounds_phase, imagePhase)
println("Finishing example for image: $image_name")

#box_color = boxplot(result_color, outliers=true)
box_phase = boxplot(result_phase, outliers=false)


out_prefix = "boxplot_128_"
plot(box_phase, outliers=true)
savefig(out_prefix*split(image_name,'.')[1])

##

# Check any special transformation
indexs = findall(x->x>1, result_phase)
id = indexs[1]

target_i    = affineTransform(random_affine[:,id], target)
itp         = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

Gray.(target_i)

# Create all the necessary StaticArrays
weight_ref  = zeros(Float64, nbins)
weight_tar  = zeros(Float64, nbins)
joint       = zeros(Float64, nbins, nbins)

itp         = interpolate(reference, BSpline(Cubic(Periodic(OnGrid()))))

out         = zeros(Float64, size(reference))

# Create the phase image for target
dx_tar, dy_tar   = imgradients(target_i, KernelFactors.ando5)
img_phase_tar_i  = phase(dx_tar, dy_tar)

Gray.(img_phase_tar_i)


f1(x)    = similarityIntensity!(x,reference, target_i, nbins, weight_ref, weight_tar, joint, bound_int, itp, out)
f2(x)    = similarityPhase!(x,reference, img_phase_tar_i, nbins, weight_ref, weight_tar, joint, bounds_phase, itp, out)

result  = bboptimize(f1; SearchRange = imgbounds, Method = :de_rand_2_bin_radiuslimited, MaxTime = 30,
            PopulationSize=50)

min_val = best_candidate(result)
diff_t  = norm(min_val-random_affine[:, id])

bounds_add = [(0.0, 1.0),  (-pi, convert(Float64, pi))]
f4(x) = similarityAdditive(x, reference, target_i, img_phase_tar_i, nbins, bounds_add, itp)

result  = bboptimize(f4; SearchRange = imgbounds, Method = :de_rand_2_bin_radiuslimited, MaxTime = 30,
            PopulationSize=50)


min_val = best_candidate(result)
diff_t  = norm(min_val-random_affine[:, id])

Gray.(hcat(target_i, out))

##

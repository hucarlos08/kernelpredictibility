#ifndef UNTOPTIMIZATION_H_INCLUDED
#define UNTOPTIMIZATION_H_INCLUDED

//Standar includes
#include <iostream>
#include <fstream>
#include <ctime>

//OpenCV includes
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

//Kernels includes
#include <KernelP.hpp>
//#include <KPAffin.hpp>
//#include <untKPDistance.hpp>

//Optimization includes
#include <untPowell.hpp>
#include <Simplex.hpp>

enum METHODS {POWELL=0,SIMPLEX,DGE,CG,MBFGS};

//Forward declaration
class Optimization;

void Register(KernelP&kp,Optimization&optim,const cv::Mat&image,const cv::Mat&transform);



class Optimization
{
	public:
		Optimization(const int itmax = 500): _ITMAX(itmax) 
                {
                    _iterations     = 0;
                    _min_function   = 0.0;
                    _lambda         = 0.1;
                    _time           = 0.0;
                }

		//Funciones relacionadas con la optimizacion
		cv::Mat Minimize(KernelP&kp,const cv::Mat&transformation,const int method);

		inline void setLambda(const double lambda) {_lambda=lambda;}
		inline void setMaxIterations(const int maxiterations) {_ITMAX=maxiterations;}



		//Funciones para obtener los parametros
		inline unsigned int 	getIterations() 	const {return _iterations;}
		inline int		getMaxIterations()	const {return _ITMAX;}
		inline double           getMinFunction()	const {return _min_function;}
		inline double		getExecutionTime()	const {return _time;}

                inline std::string 	getMethodName(const int method) const
                {
                std::string _method_name;
                switch(method)
                {
                    case POWELL:	{_method_name= std::string("_Powell");}		break;
                    case SIMPLEX:	{_method_name= std::string("_SIMPLEX");}	break;
                    case DGE:	{_method_name= std::string("_DG");}		break;
                    case CG:	{_method_name= std::string("_CG");}		break;
                    case MBFGS:     {_method_name= std::string("_BFGS");}		break;
                }
                return _method_name;
}

	private:
		int _iterations;				//Numero de iteraciones que realizo el metodo
		int _ITMAX;						//Numero m�ximo de iteraciones internas para cada m�todo
		double	_min_function;		//Valor m�nimo de la funci�n obtenido
		double	_lambda;				//Descenso de gradiente
		double	_time;				//Medir el tiempo de los algoritmos
};

#endif

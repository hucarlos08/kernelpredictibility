#ifndef UNTTESTSETS_H_INCLUDED
#define UNTTESTSETS_H_INCLUDED

#include <iostream>
#include <fstream>
#include <time.h>
#include <random>

#include <untMatFunctions.hpp>
#include <KernelP.hpp>
#include <KPAffin.hpp>
#include <untKPDistance.hpp>
#include <untKPDistance2.hpp>
#include <KPdistance3.h>
#include <untErrors.hpp>

inline double Tone_Function0(const double value)
{
    return value;
}

inline double Tone_Function1(const double value)
{
    return 100.0*std::pow((value/100.0),1.35);
}

inline double Tone_Function2(const double value)
{
    return 100*std::pow((1.0-(value/100)),1.35);
}

inline double Tone_Function3(const double value)
{
    return std::sqrt(value);
}

template<class F>
void BuilSet(const std::string&path,const std::string&srcname,const string&transname,F&tone_function,
            const double dynamic_range[2])
{

    cv::Mat image = cv::Mat_<double>(cv::imread(srcname,0));
    
    if(image.empty())
        throw Error(NOT_LOAD_IMAGE,__FUNCTION__);

    std::string str_original = path + std::string("Original.bmp");
    cv::imwrite(str_original,image);


    ifstream in(transname.c_str(),ios::in);
    if(!in.is_open())
        {throw Error(NOT_LOAD_FILE,__FUNCTION__);}

    //Leemos el numero de transformaciones que contiene el archivo
    int ntransformations=10;
    in>>ntransformations;

    cv::Mat transformation = cv::Mat::zeros(1,6,cv::DataType<double>::type);
    double *ptr_t = transformation.ptr<double>(0);

    //Aplicamos la transformacion respecto al centro de la imagen
    cv::Vec2d xc(image.rows/2.0,image.cols/2.0);

    for(int i=0;i<ntransformations;i++)
    {
        //Leemos la transformacion
        int iter= 0;
        in>>iter>>ptr_t[0]>>ptr_t[1]>>ptr_t[2]>>ptr_t[3]>>ptr_t[4]>>ptr_t[5];

        //Convertirmos el rango dinamico
//        cv::Mat image_r	= cv::NormalizeRange<double,double>(image,dynamic_range[0],dynamic_range[1]);
        cv::Mat image_r	= image;
        
        //Aplicamos la transformacion
        cv::Mat image_r_t;
        ApplyAffinT(image_r,image_r_t,transformation,xc,3,false);

        //Aplicamos la función de transferencia de tonos
        //image_r_t = cv::ToneFunction<double>(image_r_t,tone_function);

        //Guardamos los resultados
        std::string resultname = path + KP::DIAG + std::string("result")+Value_To_String(i)+std::string(".bmp");
        cv::imwrite(resultname,image_r_t);
    }
    in.close();
}

//Fordwar declaration
class StructTestDirectories;

inline std::string CombineStrings(const std::string&str1,const std::string&str2)
{
    return str1 + str2;
}

  
inline std::string getMethodName(const int method)
{
    std::string _method_name;
    switch(method)
    {
        case POWELL:	{_method_name	= std::string("_Powell");}	break;
        case SIMPLEX:	{_method_name	= std::string("_SIMPLEX");}	break;
        case DGE:	{_method_name	= std::string("_DG");}		break;
        case CG:	{_method_name	= std::string("_CG");}		break;
        case MBFGS:	{_method_name	= std::string("_BFGS");}	break;
    }
    return _method_name;
}

inline std::string OutFileName(const std::string&resultname,const int npixs,
                                const std::string&scheme,const int method,
                                const int type,const int estimator)
{

    std::string name 	=   resultname +
                            scheme +
                            Value_To_String(npixs) +
                            std::string("_") +
                            getMethodName(method) +
                            std::string("_type_") +
                            Value_To_String(type) +
                            std::string("_estimator_")+
                            Value_To_String(estimator)+
                            std::string(".txt");
    return name;
}

inline std::string OutFileName2(const std::string&resultname,const int npixs,
                                const std::string&scheme,const int method,
                                const int type,const int estimator,const int apertureSize,
                                const double threshold1,const double threshold2,const Border_Type b_type)
{

    std::string name 	=   resultname +
                            scheme +
                            Value_To_String(npixs) +
                            std::string("_") +
                            getMethodName(method) +
                            std::string("_type_") +
                            Value_To_String(type) +
                            std::string("_estimator_")+
                            Value_To_String(estimator)+
                            std::string("_asize_") +
                            Value_To_String(apertureSize)+
                            std::string("_tau1_") +
                            Value_To_String(threshold1)+
                            std::string("_tau2_") +
                            Value_To_String(threshold2) +
                            std::string("_border_") +
                            Value_To_String(b_type) +
                            std::string(".txt");
    return name;
}

// =================================== FUNCIONES SKP NORMAL ===============================================
void SetSKPTest(const StructTestDirectories&set,const int seed,const int method,
                const int npixs,const cv::Vec2d&sigmas,const int type,
                const int estimator,const double lambda,const int pyr_levels,
                const int maxiters,const double epsdiff);

void ParallelSetSKPTest(const StructTestDirectories&set,const std::vector<int>&samples,
                        const cv::Vec2d&sigmas,const double lambda,const int pyr_levels,
                        const int maxiters,const double epsdiff,const int estimator,
                        const unsigned long seed);

// =================================== FUNCIONES SKP BORDES ================================================
void SetDistanceTest(const StructTestDirectories&set,const int seed,
                        const int npixs,const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD,
                        const int type,const int estimator,const double lambda,const int pyr_levels,
                        const int maxiters,const double epsdiff,const int membresia,const double alpha,
                        const int apertureSize, const double threshold1,const double threshold2,
                        const Border_Type b_type);

void SetDistance2Test(const StructTestDirectories&set,const int seed,
                        const int npixs,const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD,
                        const int type,const int estimator,const double lambda,const int pyr_levels,
                        const int maxiters,const double epsdiff,const int membresia,const double alpha,
                        const int apertureSize,const double threshold1,const double threshold2,
                        const Border_Type b_type,const std::vector<double>betas=std::vector<double>(0));

void SetDistance3Test(const StructTestDirectories&set,const int seed,
                        const int npixs,const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD,
                        const int type,const int estimator,const double lambda,const int pyr_levels,
                        const int maxiters,const double epsdiff,const int membresia,const double alpha,
                        const int apertureSize, const double threshold1,const double threshold2,
                        const Border_Type b_type);


void ParallelSetDistanceTest(const StructTestDirectories&set,const std::vector<int>&samples,
                            const cv::Vec2d&sigmas,const cv::Vec2d sigmasD,
                            const double lambda,const int pyr_levels,
                            const int maxiters,const double epsdiff,const int estimator,
                            const unsigned long seed,const int membresia,const double alpha,
                            const int apertureSize, const double threshold1,const double threshold2,
                            const Border_Type b_type);
/**
 * 
 */
void ParallelSetDistance2Test(const StructTestDirectories&set,const std::vector<int>&samples,
                                const cv::Vec2d&sigmas,const cv::Vec2d sigmasD,
                                const double lambda,const int pyr_levels,
                                const int maxiters,const double epsdiff,const int estimator,
                                const unsigned long seed,const int membresia,const double alpha,
                                const int apertureSize,const double threshold1,const double threshold2,
                                const Border_Type b_type);


//================================= CLASE PARA MANEJAR LA ESTRUCTURA DE DIRECTORIOS ===========
class StructTestDirectories
{
    public:
        StructTestDirectories() {};
        StructTestDirectories( StructTestDirectories&other)
        {
            _path		= other._path;
            _path_output	= other._path_output;
            _path_file_t_name   = other._path_file_t_name;
            _path_file_output   = other._path_file_output;
            _path_set_directory = other._path_set_directory;
        }
        StructTestDirectories(const std::string&path,const std::string&file_t_name,
                                const std::string&set_name,const std::string&path_output,
                                const std::string&file_output)
        {
            const std::string ext(".txt");

            _path		= path;
            _path_output	= path_output;

            //Ruta completa y nombre del archivo de transformaciones
            _path_file_t_name = path + KP::DIAG + file_t_name+ext;

            //Ruta completa y nombre del archivo de resultados
            _path_file_output = path_output + KP::DIAG + file_output;

            //Ruta completa del set de las imagenes
            _path_set_directory = path + KP::DIAG + set_name;

        }

        std::string getPath (void)		const {return _path;}
        std::string getOutputPath (void )	const {return _path_output;}
        std::string getSetDirectory (void)	const {return _path_set_directory;}
        std::string getFileTName(void)		const {return _path_file_t_name;}
        std::string getOutputFileName(void)     const {return _path_file_output;}

        void setDirectories(const std::string&path,const std::string&file_t_name,
                            const std::string&set_name,const std::string&path_output,
                            const std::string&file_output)
        {
            const std::string ext(".txt");

            _path		= path;
            _path_output	= path_output;

            //Ruta completa y nombre del archivo de transformaciones
            _path_file_t_name = path + KP::DIAG + file_t_name+ext;

            //Ruta completa y nombre del archivo de resultados
            _path_file_output = path_output + KP::DIAG + file_output;

            //Ruta completa del set de las imagenes
            _path_set_directory = path + KP::DIAG + set_name;
        }

        void setPathFileOutput(const std::string&path_output,const std::string&file_output)
        {
            _path_output        = path_output;
            _path_file_output   = _path_output + KP::DIAG + file_output;
        }

    private:
        std::string _path; //path del conjunto
        std::string _path_output; //path del directorio de resultados
        std::string _path_set_directory; //directorio que contiene las imagenes
        std::string _path_file_t_name; //path y nombre del archivo que contiene las transformaciones
        std::string _path_file_output; //Nombre del archivo con los resultados
};


#endif

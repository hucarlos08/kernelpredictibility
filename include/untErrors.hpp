#ifndef untErrorsH
#define untErrorsH

#include <iostream>

using namespace std;

namespace KP{



/*
    Prototipe errors
*/

#define NOT_LOAD_IMAGE	"No se pudo cargar la imagen: "
#define NOT_LOAD_FILE	"No se pudo abrir el archivo: "
#define MISTMATCH_IMAGE_SIZE "Las imagenes son de diferente tamano"
#define IMAGE_MISMATCH "Las caracteristicas de las imagenes no coinciden"
#define NOT_1_CHANNEL "La imagen no es de un solo canal"
#define MISMATCH_SIZE "Las dimenciones de las imagenes no son iguales"
#define MISMATCH_TYPE "No coinciden los tipos de imagenes"
#define NOT_FLOAT_TYPE "La imagen no es de flotantes"
#define NOT_MEMORY "No se pudo reservar memoria"
#define NOT_UCHAR_IMAGE "La imagen no es de unsigned chars"
#define EMPTY_LIST "La lista esta vacia"
#define NOT_VIDEO_CAM "No se pudo inicializar la captura de video"
#define NOT_OPEN_FILE "El archivo no pudo abrirse"

//Some useful string
const std::string DIAG("/");
const std::string GUION("_");

class Error
{
   public:
          Error(const string&str,const string&funcname):message(str),FuncName(funcname) {}
          const string &getMessage()const{return message;}  // error message
          const string &getFuncName()const{return FuncName;} //function where the error occurred
	  void getError(ostream&os)const
        {
            os<<"Error in the function: "<<getFuncName()<<std::endl;
            os<<"Error: "<<getMessage()<<std::endl;
        }
   private:
      string message;
      string FuncName;
};

}
#endif

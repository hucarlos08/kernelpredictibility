#ifndef UNTINTERPOLATE_H_INCLUDED
#define UNTINTERPOLATE_H_INCLUDED

#include <opencv2/core.hpp>
#include <iostream>
#include <untErrors.hpp>

/**=================================================================================================
@brief	Interpolated value. 

@author	Aslan
@date	02/07/2010

@param	Bcoeff			The matrix of b-splines coefficients bcoeff. 
@param	x					The x coordinate. 
@param	y					The y coordinate. 
@param	SplineDegree	The spline degree. 

@return	. 
 *===============================================================================================**/
double	InterpolatedValue(const cv::Mat&Bcoeff,double x,double y,long	SplineDegree);

#endif // UNTINTERPOLATE_H_INCLUDED

      

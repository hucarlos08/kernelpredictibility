#ifndef UNTMATFUNCTIONS_H_INCLUDED
#define UNTMATFUNCTIONS_H_INCLUDED

//Standar includes
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <ctime>
#include <random>

//OpenCV includes
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>

//Other includes
#include <untImageOperations.hpp>
#include <untErrors.hpp>
#include <untCorrelations.hpp>

enum Border_Type{CANNY=0,STD_DEV,GRADIENT};

//Some useful defines
#ifndef M_PI
#define M_PI 3.1415926
#endif

#define DEGTORAD(Deg) ((Deg * M_PI) / 180.0)
#define RADTODEG(Rad) ((180.0 * Rad) / M_PI)

/**
 * Group: core_software
 * Descripcion: Extracts a value of type T from s.
 */
template <class T>
inline T String_To_Value(const std::string& s)
{
    T x;
    std::istringstream str(s);
    str >> x;
    return x;
}

/**
 * Group: core_software
 * Descripcion: Returns a std::string representation of value x.
 */
template <class T>
inline std::string Value_To_String(const T& x)
{
    std::ostringstream s;
    s << x;
    return s.str();
}


template<class T>
inline T SQR(const T a) {return a*a;}

template<class T>
inline void SWAP(T &a, T &b) {T dum=a; a=b; b=dum;}

template<class T>
inline T SIGN(const T &a, const T &b)
{return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}


namespace cv
{


    template<class T,class P>
    cv::Mat NormalizeRange(const cv::Mat&image,double smin=0.0,double smax=255.0)
    {
        CV_Assert( cv::DataType<T>::type == image.type() );
        int rows    = image.rows;
        int cols    = image.cols;
        cv::Mat_<P>normalize   = cv::Mat_<P>::zeros(rows,cols);
        double emax = 0.0;
        double emin = 0.0;
        double tmp  = 0.0;
        cv::minMaxLoc(image,&emin,&emax);

        if(image.isContinuous())
        {
            cols *= rows;
            rows = 1;
        }
        for(int i = 0; i < rows; i++)
        {
            const T *Mi = image.ptr<T>(i);
            P *Ni = normalize[i];

            for(int j = 0; j < cols; j++)
            {
                tmp=((((double)Mi[j]-emin)/(emax-emin))*(smax-smin))+smin;
                Ni[j]=static_cast<P>(tmp);
            }
        }
        return normalize;
    }

    template<class T,class P>
    void ConvertTo(const cv::Mat_<T>&src, cv::Mat_<P>&dst)
    {
        int rows = src.rows;
        int cols = src.cols;
        dst = cv::Mat_<P>::zeros(rows,cols);
        if(src.isContinuous()&&dst.isContinuous())
        {
            cols *= rows;
            rows = 1;
        }
        for(int i = 0; i < rows; i++)
        {
            const T *ptr_src = src[i];
            P *ptr_dts = dst[i];

            for(int j = 0; j < cols; j++)
                ptr_dts[j] = P(ptr_src[j]);
        }
    }

    template <class T>
    void DrawCross(cv::Mat_<T>&_image,const cv::Point2f&center,const Scalar& color,int d=5)
    {
        cv::line(_image,cv::Point(center.x-d,center.y-d),cv::Point(center.x+d,center.y+d ),color,1,0);
        cv::line(_image,cv::Point(center.x+d,center.y-d),cv::Point(center.x-d,center.y+d ),color,1,0);
    }


    template<class T>
    void DrawArrow(cv::Mat_<T>&_image,const cv::Point2f&p1,const cv::Point2f&p2,
                   const Scalar& color,int thickness=2,int lineType=8, int shift=0)
    {
        double pi=3.1415926535;
        double angle; angle = atan2( (double) p1.y - p2.y, (double) p1.x - p2.x );
        double hypotenuse; hypotenuse = sqrt(1.0*(p1.y - p2.y)*(p1.y - p2.y) + (p1.x - p2.x)*(p1.x - p2.x) );
        cv::line(_image,p1,p2,color,thickness,lineType,shift);

        /*  Now draw the tips of the arrow. I do some scaling so that the
        tips look proportional to the main line of the arrow.
    */
        cv::Point p;
        p.x = (int) (p2.x + 9 * cos(angle + pi / 4));
        p.y = (int) (p2.y + 9 * sin(angle + pi / 4));
        cv::line(_image,p,p2,color,thickness,lineType,shift);
        p.x = (int) (p2.x + 9 * cos(angle - pi / 4));
        p.y = (int) (p2.y + 9 * sin(angle - pi / 4));
        cv::line(_image,p,p2,color,thickness,lineType,shift);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// @fn	template<class T> void CombineImages(const cv::Mat_<T>&src1,const cv::Mat_<T>&src2,
    /// cv::Mat_<T>&result)
    ///
    /// @brief	Combine images.
    ///
    /// @author	Aslan
    /// @date	09/09/2010
    ///
    /// @exception	Error	Thrown when error.
    ///
    /// @param	src1			Source 1.
    /// @param	src2			Source 2.
    /// @param [in,out]	result	The result.
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    template<class T>
    void CombineImages(const cv::Mat_<T>&src1,const cv::Mat_<T>&src2,cv::Mat_<T>&result)
    {
        if(src1.size() != src2.size())
            throw KP::Error(IMAGE_MISMATCH,__FUNCTION__);

        result.create(src1.rows, 2*src1.cols);
        cv::Mat mresult1 = result(cv::Range::all(),cv::Range(0,src1.cols));
        cv::Mat mresult2 = result(cv::Range::all(),cv::Range(src1.cols,result.cols));
        src1.copyTo(mresult1);
        src2.copyTo(mresult2);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// @fn	template<class T> void DrawCorrelations(const cv::Mat_<T>&image1,const cv::Mat_<T>&image2,
    /// cv::Mat_<T>&result, const cv::vector< Correlation<double> >&correlations,
    /// const std::vector<uchar>&mask=std::vector<uchar>(0))
    ///
    /// @brief	Draw correlations.
    ///
    /// @author	Aslan
    /// @date	09/09/2010
    ///
    /// @exception	Error	Thrown when error.
    ///
    /// @param	image1			The first image.
    /// @param	image2			The second image.
    /// @param [in,out]	result	The result.
    /// @param	correlations	The correlations.
    /// @param	mask			The mask.
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    template<class T>
    void DrawCorrelations(const cv::Mat_<T>&image1,const cv::Mat_<T>&image2,cv::Mat_<T>&result,
                          const std::vector< Correlation<double> >&correlations,
                          const std::vector<uchar>&mask=std::vector<uchar>(0))
    {
        if(image1.size() != image2.size())
            throw KP::Error(IMAGE_MISMATCH,__FUNCTION__);

        int columns1 = image1.cols;

        bool b_mask = (mask.size() != 0);
        if(b_mask)
            if(correlations.size() != mask.size())
                throw KP::Error("MISMATCH MASK SIZES",__FUNCTION__);

        CombineImages(image1,image2,result);
        std::vector< Correlation<double> >::const_iterator it   = correlations.begin();
        std::vector< Correlation<double> >::const_iterator end  = correlations.end();
        cv::Point2f p1;
        cv::Point2f p2;
        int i=0;
        for(it = correlations.begin(),i=0; it!=end; it++,i++)
        {

            if(b_mask)
            {
                int m   =   static_cast<int>(mask[i]);
                if(m == 0) { continue; }
            }
            p1 = (*it).getCorrelationFirst();
            p2 = (*it).getCorrelationSecond();
            p2.x += columns1;
            DrawArrow(result,p1,p2,cv::Scalar_<uchar>(255,255,255),2);
        }

    }

    /*
    Duplica el borde de una imagen en las 4 direcciones
*/
    template<class T>
    void DuplicateBorder(const cv::Mat&image, cv::Mat&borderimage, const unsigned int border=10)
    {
        cv::Mat_<T>tmp1(image);

        // constructs a larger image to fit both the image and the border
        borderimage.create(tmp1.rows + (border*2), tmp1.cols + (border*2),cv::DataType<T>::type);
        // form a border in-place
        cv::copyMakeBorder(tmp1, borderimage, border, border,border, border, cv::BORDER_REPLICATE);

    }


    template<class T>
    void DuplicateBorder2(const cv::Mat_<T>&image, cv::Mat_<T>&borderimage, const int border=10)
    {
        // constructs a larger image to fit both the image and the border
        int col=image.cols;
        int ren=image.rows;
        borderimage=cv::Mat_<T>::zeros(image.rows + (border*2), image.cols + (border*2));
        int i,j;

        for (i=0;i<ren;i++)
            for (j=0;j<col;j++)
                borderimage(i+border,j+border)=image(i,j);

        for (i=0;i<border;i++)
            for (j=0;j<col;j++)
            {
                borderimage(border-i-1,j+border)     = borderimage(i+border,j+border);
                borderimage(ren+i+border,j+border)   = borderimage(ren-1-i+border,j+border);
            }

        for (j=0;j<border;j++)
            for (i=0;i<borderimage.rows;i++)
            {
                borderimage(i,border-j-1)     = borderimage(i,j+border);
                borderimage(i,col+j+border)   = borderimage(i,col-1-j+border);
            }
    }


    /*
    This section of code is just for check the improve of descriptor
    This comprobation assumes the images are from the same scene and the positon (x1,y1) and (x2,y2)
    must be equal.
*/
    template<class T>
    void EvalueMetric(const std::vector< Correlation<double> >&correlations,
                      const T&mytreshold,double&recall,double&lesspresition)
    {
        unsigned int Positives          = 0;
        unsigned int Falses             = 0;
        unsigned int correctpositive    = 0;
        unsigned int falsepositive      = 0;
        for(unsigned int i=0; i<correlations.size();i++)
        {
            if(correlations[i].getCorrelationFirst()==correlations[i].getCorrelationSecond())
                Positives++;
            else
                Falses++;
        }
        for(unsigned int i=0; i<correlations.size();i++)
        {
            if(correlations[i].getValue() < mytreshold)
            {
                if(correlations[i].getCorrelationFirst()==correlations[i].getCorrelationSecond())
                {
                    correctpositive++;
                }
                else
                {
                    falsepositive++;
                }
            }
        }
        //sort(correlations.begin(),correlations.end());
        recall          = (correctpositive/double (Positives));
        lesspresition   = (falsepositive/(double (Positives+Falses)));
    }

    /*
    This section of code is just for check the improve of descriptor
    This comprobation assumes the inliers vector contain the true an false
    points.
*/
    template<class T>
    void EvalueMetric(const std::vector< Correlation<double> >&correlations,
                      const std::vector<uchar>&inliers,
                      const T&mytreshold,double&recall,double&lesspresition)
    {
        unsigned int Positives          = 0;
        unsigned int Falses             = 0;
        unsigned int correctpositive    = 0;
        unsigned int falsepositive      = 0;

        if(correlations.size() != inliers.size() )
            throw KP::Error("Mismatch size vectors",__FUNCTION__);

        //Check the total of correct and incorrect matches
        for(unsigned int i=0; i<inliers.size();i++)
        {
            if(static_cast<int>(inliers[i]) == 1)
                Positives++;
            else
                Falses++;
        }
        for(unsigned int i=0; i<correlations.size();i++)
        {
            if(correlations[i].getValue() < mytreshold)
            {
                if(static_cast<int>(inliers[i]) == 1)   { correctpositive++;}
                else                                    {falsepositive++;}
            }
        }
        recall          = (correctpositive/double (Positives));
        lesspresition   = (falsepositive/(double (Positives+Falses)));
    }

    /*
    Write a cv::Mat in file text
*/
    template<class T>
    void WriteinFile(const cv::Mat&mat,const std::string&filename)
    {
        if(mat.type() != cv::DataType<T>::type)
            throw KP::Error("Error in MAT type",__FUNCTION__);

        ofstream out(filename.c_str(),ios::out);
        if(!out.is_open())
            throw KP::Error("File not could be opened",__FUNCTION__);

        const T *ptr_mat = NULL;
        for(int i=0; i<mat.rows; i++)
        {
            ptr_mat = mat.ptr<T>(i);
            for(int j=0; j<mat.cols; j++)
                out<<ptr_mat[j]<<'\t';
            out<<std::endl;
        }
        out.close();
    }


    /*
    Find the homography using the correlation vector
*/
    template<class T>
    void HomographyfromCorrelation(const  std::vector< Correlation<T> >&correlations, cv::Mat_<float>&homography,
                                   std::vector<uchar>&status,int method=0,double ransacReprojThreshold=0)
    {
        cv::Mat_<float>mat1(correlations.size(),2,0.0);
        cv::Mat_<float>mat2(correlations.size(),2,0.0);

        typename std::vector< Correlation<T> >::const_iterator it=correlations.begin();

        for(int i=0; i<mat1.rows; i++, it++)
        {
            mat1(i,0)=(*it).getCorrelationFirst().x;
            mat1(i,1)=(*it).getCorrelationFirst().y;
            mat2(i,0)=(*it).getCorrelationSecond().x;
            mat2(i,1)=(*it).getCorrelationSecond().y;
        }
        homography= cv::findHomography(mat1,mat2,status,method,ransacReprojThreshold);
    }

    template<class T>
    cv::Mat_<T> ReadHomography(const std::string&filename)
    {
        std::ifstream in(filename.c_str(),ios::in);
        std::string error = std::string("Not could open the file: ") + filename;
        if(!in.is_open()) throw KP::Error(error,__FUNCTION__);
        cv::Mat_<double>result(3,3,0.0);
        int rows = result.rows;
        int cols = result.cols;

        if(result.isContinuous())
        {
            cols *= rows;
            rows = 1;
        }
        for(int i=0; i<rows; i++)
        {
            T *ptr = result[i];
            for(int j=0; j<cols; j++)
                in>>ptr[j];
        }
        return result;
    }

    template<class T,class P>
    cv::Mat_<double> ToneFunction(const cv::Mat&src, P&func)
    {
        int rows = src.rows;
        int cols = src.cols;
        cv::Mat_<double> dst = cv::Mat_<double>::zeros(rows,cols);

        const T *ptr_src = nullptr;
        double *ptr_dst  = nullptr;
        double temp = 0.0;

        if(src.isContinuous())
        {
            cols *= rows;
            rows = 1;
        }
        for(int i = 0; i < rows; i++)
        {
            ptr_src  = src.ptr<T>(i);
            ptr_dst  = dst[i];

            for(int j = 0; j < cols; j++)
            {
                temp = static_cast<double>(ptr_src[j]);
                ptr_dst[j] = func(temp);
            }
        }
        return dst;
    }

    template<class T>
    void Write(const cv::Mat&src,std::ostream&out)
    {
        const T* ptr_mat = NULL;
        for(int i=0; i<src.rows; i++)
        {
            ptr_mat = src.ptr<T>(i);
            for(int j=0; j<src.cols; j++)
            {
                out.width(5);
                out<<ptr_mat[j]<<'\t';
            }
            out<<std::endl;
        }
    }

    /** La funcion devuelve en dst las coordenadas de los pixeles de una imagen binarizada de bordes
    @param [in]  const cv::Mat&src: imagen de bordes
    @param [out] cv::Mat&dst: matriz de coordenadas de los bordes
*/
    template<class T,class P>
    void getBordersCoordinates(const cv::Mat&src,cv::Mat&dst)
    {
        CV_Assert(src.type()==cv::DataType<T>::type);

        //Llenamos la matriz de muestras
        int count = cv::countNonZero(src);

        dst         = cv::Mat::zeros(count,2,cv::DataType<P>::type);
        int rows    = src.rows;
        int cols    = src.cols;
        const T *ptr_src = NULL;
        P *ptr_dst = NULL;

        int temp = 0;
        int i=0,j=0;
        for(i=0; i<rows; i++)
        {
            ptr_src = src.ptr<T>(i);
            for(j=0; j<cols; j++)
            {
                if(ptr_src[j] != 0.0)
                {

                    ptr_dst = dst.ptr<P>(temp);
                    ptr_dst[0] = i;
                    ptr_dst[1] = j;
                    temp++;
                }

            }
        }
        if(temp!=count)
            throw KP::Error("Not good coordenates",__FUNCTION__);
    }

    /**
    Obtiene muestras de una matriz de coordenadas muestreadas uniformemente
    regresa las muestras en otra matriz
    @param[in]  const cv::Mat&coordenates: matriz de coordenadas
    @param[out] cv::Mat&samples_coordenates: matriz con las muestras
    @param[out] int&samples: numero efectivo de muestras
*/
    template<class T,class P>
    void getSamplesCoordenates(const cv::Mat&coordenates,cv::Mat&samples_coordenates, int&samples)
    {

        CV_Assert(coordenates.type()==cv::DataType<T>::type);

        samples = (samples>coordenates.rows)?coordenates.rows:samples;

        // linear congruential generator
        std::mt19937 gen;
        // initialize the generator
        gen.seed((unsigned int)time(NULL));

        std::uniform_int_distribution<> samples_row(0,coordenates.rows-1);
        samples_coordenates = cv::Mat::zeros(samples,2,cv::DataType<P>::type);

        //Variables temporales necesarias
        int i = 0;
        const T *ptr_coordenates            = NULL;
        P *ptr_samples_coordenates    = NULL;
        for(i=0;i<samples; i++)
        {
            int r = samples_row(gen);

            //Tomamos la fila y columna de coordenates
            ptr_coordenates             = coordenates.ptr<T>(r);
            ptr_samples_coordenates     = samples_coordenates.ptr<P>(i);

            ptr_samples_coordenates[0]  = ptr_coordenates[0];
            ptr_samples_coordenates[1]  = ptr_coordenates[1];

        }
        int j;
        cv::Mat msamples = cv::Mat::zeros(128,128,CV_8U);
        for(int p=0; p<samples_coordenates.rows; p++)
        {
            ptr_samples_coordenates = samples_coordenates.ptr<P>(p);
            i=ptr_samples_coordenates[0];
            j=ptr_samples_coordenates[1];
            msamples.at<uchar>(i,j)=255;
        }
        cv::imwrite("Samples_from_borders.bmp",msamples);
    }

    /**
    Invierte una transformación affin
    @param const cv::Mat&taffin: transformacion afin original
    @param cv::Mat&inverse: matriz que contendra la inversa
*/
    template<class T>
    inline void getInverseAffin(const cv::Mat&taffin,cv::Mat&inverse)
    {
        CV_Assert(taffin.type()==cv::DataType<T>::type);
        CV_Assert(taffin.rows==1&&taffin.cols==6);

        cv::Mat temp = taffin.reshape(1,2);
        cv::invertAffineTransform(temp,inverse);
        inverse = inverse.reshape(1,1);
    }


    template<class T,class P>
    void getSamplesCoordenates(const cv::Mat&coordenates,const cv::Mat&taffin,const cv::Vec2d&xc,
                               const cv::Size&size,cv::Mat&samples_coordenates, int&samples,
                               bool inverse=false,bool verbose=false)
    {

        CV_Assert(coordenates.type()==cv::DataType<T>::type);
        CV_Assert(taffin.cols==6&&taffin.rows==1&&taffin.type()==CV_64F);

        samples = (samples>coordenates.rows)?coordenates.rows:samples;

        cv::Mat temp;

        if(inverse)
        {
            getInverseAffin<double>(taffin,temp);
        }
        else
        {
            taffin.copyTo(temp);
        }

        // linear congruential generator
        std::mt19937 gen;

        // initialize the generator
        gen.seed((unsigned int)time(NULL));

        std::uniform_int_distribution<> samples_row(0,coordenates.rows-1);
        samples_coordenates = cv::Mat::zeros(samples,2,cv::DataType<P>::type);

        //Apuntadores necesarios
        const T *ptr_coordenates            = NULL;
        double  *ptr_affin                  = temp.ptr<double>(0);
        P *ptr_samples_coordenates    = NULL;


        //Variables temporales necesarias
        int i = 0;
        int j = 0;
        int p = 0;
        int rows = size.height;
        int cols = size.width;
        for(p=0;;)
        {
            int r = samples_row(gen);

            //Tomamos la fila y columna de coordenates
            ptr_coordenates             = coordenates.ptr<T>(r);
            ptr_samples_coordenates     = samples_coordenates.ptr<P>(p);

            i = ptr_samples_coordenates[0]  = ptr_coordenates[0];
            j = ptr_samples_coordenates[1]  = ptr_coordenates[1];

            double x = (i-xc[0])*ptr_affin[0] + (j-xc[1])*ptr_affin[1] + ptr_affin[2] + xc[0];
            double y = (i-xc[0])*ptr_affin[3] + (j-xc[1])*ptr_affin[4] + ptr_affin[5] + xc[1];

            if(x>=0&&x<rows&&y>=0&&y<cols)
                p++;

            if(p==samples) {break;}
        }

        if(verbose)
        {
            cv::Mat msamples = cv::Mat::zeros(128,128,CV_8U);
            for(int p=0; p<samples_coordenates.rows; p++)
            {
                ptr_samples_coordenates = samples_coordenates.ptr<P>(p);
                i=ptr_samples_coordenates[0];
                j=ptr_samples_coordenates[1];
                msamples.at<uchar>(i,j)=255;
            }
            cv::imwrite("Hugo.bmp",msamples);
        }
    }




    /*
        Genera el laplaciano para una imagen de bordes
        estimada con Canny
*/
    template<class T>
    void LaplacianCanny(const cv::Mat&src, cv::Mat&dst,int window=3)
    {
        if(src.type() != cv::DataType<T>::type || src.channels() != 1)
            throw KP::Error("El tipo imagen no coincide",__FUNCTION__);

        cv::Mat temp;
        DuplicateBorder<T>(src,temp,window);



        int wsize = (window%2)==1?(window):(window+1);
        int N			 = (wsize-1)/2;

        int rows = temp.rows - (1*wsize);
        int cols = temp.cols - (1*wsize);

        int x0    	= 0;
        int y0    	= 0;

        //Creamos la matriz resultante
        dst 		= cv::Mat::zeros(src.size(),src.type());
        T *ptr_dst 	= NULL;
        T *ptr_temp = NULL;

        for(int i=wsize; i<rows; i++)
        {
            ptr_dst		= dst.ptr<T>(i-wsize);
            ptr_temp	= temp.ptr<T>(i);
            for(int j=wsize; j<cols; j++)
            {
                x0 = (j - N);
                y0 = (i - N);
                cv::Scalar suma(0.0);

                cv::Mat ROI(temp,cv::Rect(x0,y0,wsize,wsize));
                suma		= cv::sum(ROI);

                ptr_dst[j-wsize] = (wsize * ptr_temp[j]) - suma[0];

            }

        }
    }

    /**=================================================================================================
@fn	template<class T> void BorderDifusion(cv::Mat&src,cv::Mat&dst,int iters=50,
double lambda=1.0e-3,double threshold1=50, double threshold2=100,int apertureSize=3)

@brief	Border difusion. 

@author	Aslan
@date	29/06/2010

@exception	Error	Thrown when error. 

@param [in,out]	src	Source for the. 
@param [in,out]	dst	Destination for the. 
@param	iters				The iters. 
@param	lambda			The lambda. 
@param	threshold1		The first threshold. 
@param	threshold2		The second threshold. 
@param	apertureSize	Size of the aperture. 
 *===============================================================================================**/
    template<class T>
    void BorderDifusion(cv::Mat&src,cv::Mat&dst,int iters=50,double lambda=1.0e-3,double threshold1=50,
                        double threshold2=100,int apertureSize=3)
    {
        if(src.type() != cv::DataType<uchar>::type || src.channels() != 1)
            throw KP::Error("SRC image must be CV_8U1C",__FUNCTION__);

        cv::Mat B2_border;
        cv::Canny(src,B2_border,threshold1,threshold2,apertureSize);
        cv::threshold(B2_border,dst,50,255.0,THRESH_BINARY);
        B2_border.copyTo(src);
        cv::imwrite("Canny_border.bmp",src);

        cv::Mat_<T>B2(B2_border);

        cv::Mat_<T>B_new 	= cv::Mat_<T>::zeros(B2.size()); //Matriz en el tiempo t
        cv::Mat_<T>B_old;

        //En la iteracion 0 B_old = B2
        B2.copyTo(B_old);

        //Matriz temporal del laplaciano
        cv::Mat_<T>laplacian;

        int rows = B2.rows;
        int cols = B2.cols;


        for(int t=0; t<iters; t++)
        {

            LaplacianCanny<T>(B_old,laplacian);

            for(int i=0; i<rows;i++)
            {
                const 	T *ptr_b	 	 = B2[i];
                T *ptr_b_new 	 = B_new[i];
                T *ptr_b_old 	 = B_old[i];
                T *ptr_laplacian = laplacian[i];

                for(int j=0; j<cols; j++)
                {
                    if(ptr_b[j]==0)
                    {
                        ptr_b_new[j] = ptr_b_old[j] - (lambda*ptr_laplacian[j]);
                    }

                    else
                    {
                        ptr_b_new[j] = ptr_b_old[j];
                    }
                }

            }
            B_new.copyTo(B_old);

        }
        cv::Mat_<float>tresh(B_new);
        cv::threshold(tresh,dst,50,255.0,THRESH_BINARY);
        dst = cv::Mat_<T>(dst);
    }

    template<class T,class P>
    void BordersFromType(const cv::Mat&src,cv::Mat&dst,const int apertureSize,const double threshold1,
                         const double threshold2,const Border_Type border_type)
    {
        if(src.type()!=cv::DataType<T>::type)
            throw KP::Error(MISMATCH_TYPE,__FUNCTION__);

        if(src.channels()!=1)
            throw KP::Error(NOT_1_CHANNEL,__FUNCTION__);

        switch(border_type)
        {
            case CANNY:
                {
                    //Bordes de la primera imagen
                    if(src.type()!=cv::DataType<uchar>::type)
                    {
                        cv::Mat_<uchar> temp1(src);
                        cv::Canny(temp1,dst,threshold1,threshold2,apertureSize,true);
                    }
                    else
                    {
                        cv::Canny(src,dst,threshold1,threshold2,apertureSize,true);
                    }
                }
                break;
            case STD_DEV:
                {
                    cv::Mat std_dev;
                    cv::Mat mean;
                    cv::Mat temp;
                    cv::MeanStdDevImages<T,double>(src,mean,std_dev,apertureSize);
                    temp = cv::NormalizeRange<double,uchar>(std_dev,0.0,255.0);
                    cv::BinaryThreshold<uchar,P>(temp,dst,threshold1,255.0);

                }
                break;
            case GRADIENT:
                {
                    cv::Mat dev_x;
                    cv::Mat dev_y;
                    cv::Mat temp;
                    cv::Sobel(src,dev_x,cv::DataType<double>::type,1,0);
                    cv::Sobel(src,dev_y,cv::DataType<double>::type,0,1);

                    //Estimamos la magnitud del gradiente
                    cv::Mat dev_x_2;
                    cv::Mat dev_y_2;

                    cv::pow(dev_x,2,dev_x_2);
                    cv::pow(dev_y,2,dev_y_2);
                    cv::Mat sum = dev_x_2 + dev_y_2;
                    cv::sqrt(sum,temp);
                    temp = cv::NormalizeRange<double,uchar>(temp,0.0,255.0);
                    cv::BinaryThreshold<uchar,P>(temp,dst,threshold1,255.0);
                }
                break;
            default:
                throw KP::Error("Border type dont exist",__FUNCTION__);
                break;

        }
    }

    //=================FUNCIONES PARA INCORPORAR LA DISTANCIA===================//
    template<class T,class P>
    void Distance(const cv::Mat_<T>&g,cv::Mat_<P>&d,double umb)
    {
        int i,j,ii;
        double aux,s2,aux1,uxm,uym;

        const int nr = g.rows;
        const int nc = g.cols;

        umb = static_cast<T>(umb);

        //Creamos el espacio en las matrices
        cv::Mat_<char>q = cv::Mat_<char>::zeros(nr,nc);
        d = cv::Mat_<P>::zeros(nr,nc);

        s2 = std::sqrt(2.0f);

        for (i = 0 ; i < nr ; i++)
            for (j = 0 ; j < nc ; j++)
            {
                //CondiciÃ³n de frontera, se etiqueta el punto
                //y se inicia la distancia en cero
                if (g[i][j] >= umb)
                {
                    q[i][j]		= 'c';
                    d[i][j]		= 0;
                }
                //En caso contrario se marca como no perteneciente a la frontera
                //y se inicia la distancia en un valor muy alto
                else {q[i][j] = 'n' ;  d[i][j] = 10000 ;}
            }
        //Se calculan condiciones iniciales
        for (i = 0 ; i < nr ; i++)
            for (j = 0 ; j < nc ; j++)
                //Si el punto no es un punto en el contorno, se buscan vecinos que sÃ­ lo estÃ©n
                //y se inicia la distancia como la distancia hacia el vecino en contorno mÃ¡s cercano
                //Se marca ademÃ¡s el punto como 't'
                if (q[i][j] == 'n')
                {
                    aux = 1.e10 ;
                    if ((i > 0 && q[i-1][j] == 'c'))
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i-1][j]+1) < aux) aux = aux1 ;
                    }
                    if (j > 0 && q[i][j-1] == 'c')
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i][j-1]+1) < aux) aux = aux1 ;
                    }
                    if (i <nr-1 && q[i+1][j] == 'c')
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i+1][j]+1) < aux) aux = aux1 ;
                    }
                    if (j <nc-1 && q[i][j+1] == 'c')
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i][j+1]+1) < aux) aux = aux1 ;
                    }
                    if ((i > 0 && j > 0 && q[i-1][j-1] == 'c'))
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i-1][j-1]+s2) < aux) aux = aux1 ;
                    }
                    if (j > 0 && i < nr-1 && q[i+1][j-1] == 'c')
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i+1][j-1]+s2) < aux) aux = aux1 ;
                    }
                    if (i <nr-1 && j<nc-1 && q[i+1][j+1] == 'c')
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i+1][j+1]+s2) < aux) aux = aux1 ;
                    }
                    if (j <nc-1 && i>0 &&q[i-1][j+1] == 'c')
                    {
                        q[i][j] = 't' ;
                        if ((aux1=d[i-1][j+1]+s2) < aux) aux = aux1 ;
                    }
                    if (q[i][j] == 't') d[i][j] = aux ;
                }
        //Realiza el sweeping
        for(ii = 0 ; ii < 5 ; ii++)
        {
            for (i = 0 ; i < nr ; i++)
                for (j = 0 ; j < nc ; j++)
                    //Resuelve la ecuaciÃ³n de Eikonal para cada pixel, exceptuando los que pertenezcan al contorno
                    if (q[i][j] != 'c')
                    {
                        if (i == 0) uxm = d[1][j] ;
                        else if (i == nr-1) uxm = d[nr-2][j] ;
                        else uxm = d[i-1][j] < d[i+1][j] ? d[i-1][j] : d[i+1][j] ;
                        if (j == 0) uym = d[i][1] ;
                        else if (j == nc-1) uym = d[i][nc-2] ;
                        else uym = d[i][j-1] < d[i][j+1] ? d[i][j-1] : d[i][j+1] ;
                        if ((aux1=fabs(uxm-uym)) >= 1)
                            aux = (uxm < uym ? uxm : uym) + 1 ;
                        else
                            aux = 0.5f*(uxm+uym+std::sqrt(2.0f- aux1*aux1)) ;
                        if (aux < d[i][j]) d[i][j] = aux ;
                    }
            for (i = nr-1 ; i >= 0 ; i--)
                for (j = 0 ; j < nc ; j++)
                    //Resuelve la ecuaciÃ³n de Eikonal para cada pixel, exceptuando los que pertenezcan al contorno
                    if (q[i][j] != 'c')
                    {
                        if (i == 0) uxm = d[1][j] ;
                        else if (i == nr-1) uxm = d[nr-2][j] ;
                        else uxm = d[i-1][j] < d[i+1][j] ? d[i-1][j] : d[i+1][j] ;
                        if (j == 0) uym = d[i][1] ;
                        else if (j == nc-1) uym = d[i][nc-2] ;
                        else uym = d[i][j-1] < d[i][j+1] ? d[i][j-1] : d[i][j+1] ;
                        if ((aux1=fabs(uxm-uym)) >= 1)
                            aux = (uxm < uym ? uxm : uym) + 1 ;
                        else
                            aux = 0.5f*(uxm+uym+std::sqrt(2.0f- aux1*aux1)) ;
                        if (aux < d[i][j]) d[i][j] = aux ;
                    }
            for (i = 0 ; i < nr ; i++)
                for (j = nc-1 ; j >= 0 ; j--)
                    if (q[i][j] != 'c')
                    {
                        if (i == 0) uxm = d[1][j] ;
                        else if (i == nr-1) uxm = d[nr-2][j] ;
                        else uxm = d[i-1][j] < d[i+1][j] ? d[i-1][j] : d[i+1][j] ;
                        if (j == 0) uym = d[i][1] ;
                        else if (j == nc-1) uym = d[i][nc-2] ;
                        else uym = d[i][j-1] < d[i][j+1] ? d[i][j-1] : d[i][j+1] ;
                        if ((aux1=fabs(uxm-uym)) >= 1)
                            aux = (uxm < uym ? uxm : uym) + 1 ;
                        else
                            aux = 0.5f*(uxm+uym+sqrt(2.0f- aux1*aux1)) ;
                        if (aux < d[i][j]) d[i][j] = aux ;
                    }
            for (i = nr-1 ; i >= 0 ; i--)
                for (j = nc-1 ; j >= 0 ; j--)
                    if (q[i][j] != 'c')
                    {
                        if (i == 0) uxm = d[1][j] ;
                        else if (i == nr-1) uxm = d[nr-2][j] ;
                        else uxm = d[i-1][j] < d[i+1][j] ? d[i-1][j] : d[i+1][j] ;
                        if (j == 0) uym = d[i][1] ;
                        else if (j == nc-1) uym = d[i][nc-2] ;
                        else uym = d[i][j-1] < d[i][j+1] ? d[i][j-1] : d[i][j+1] ;
                        if ((aux1=fabs(uxm-uym)) >= 1)
                            aux = (uxm < uym ? uxm : uym) + 1 ;
                        else
                            aux = 0.5f*(uxm+uym+sqrt(2.0f- aux1*aux1));
                        if (aux < d[i][j]) d[i][j] = aux ;
                    }
        }
    }

    template<class T>
    void GenerateHist2D(const cv::Mat&src1,const cv::Mat&src2,cv::Mat&dst)
    {
        if(src1.size()!=src2.size())
            throw KP::Error(MISTMATCH_IMAGE_SIZE,__FUNCTION__);

        int rows        = src1.rows;
        int cols        = src1.cols;
        int hist_size   = rows*cols;
        dst = cv::Mat(hist_size,2,cv::DataType<double>::type);

        const T *ptr_src1 = NULL;
        const T *ptr_src2 = NULL;
        double  *ptr_dst  = NULL;

        int hist_steap = 0;

        for(int i = 0; i < rows; i++)
        {
            ptr_src1 = src1.ptr<T>(i);
            ptr_src2 = src2.ptr<T>(i);

            for(int j = 0; j < cols; j++)
            {
                ptr_dst     = dst.ptr<double>(hist_steap);
                ptr_dst[0]  = ptr_src1[j];
                ptr_dst[1]  = ptr_src2[j];

                hist_steap++;
            }
        }
    }

}//end CV plus namespace

template<class T>
std::ostream& operator<<(std::ostream&out, const cv::Mat_<T>&mat)
{

    for(int i=0; i<mat.rows; i++)
    {
        for(int j=0; j<mat.cols; j++)
        {
            out.width(5);
            out<<mat(i,j)<<'\t';
        }
        out<<std::endl;
    }
    return out;
}

template<class T>
std::istream& operator>>(std::istream&in,cv::Mat_<T>&mat)
{
    for(int i=0; i<mat.rows; i++)
        for(int j=0; j<mat.cols;j++)
        {
            in>>mat(i,j);
        }
    return in;
}
#endif // UNTMATFUNCTIONS_H_INCLUDED

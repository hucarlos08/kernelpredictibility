#ifndef UNTKPPERSPECTIVE_H_INCLUDES
#define UNTKPPERSPECTIVE_H_INCLUDES

#include <iostream>

//Kernel predictibility include
#include <KernelP.hpp>
#include <untOptimization.hpp>

inline void ConvertTPerspective2CVFormat(const cv::Mat&src,cv::Mat&dst)
{
	if( (src.rows!= 1) || (src.cols!=9 ) )
		throw KP::Error("Mismatch size",__FUNCTION__);

	dst = cv::Mat::zeros(1,9,CV_64F);

	const double *ptr_src = src.ptr<double>(0);
			double *ptr_dst = dst.ptr<double>(0);

	//Copiamos los parametros en el buen orden
	ptr_dst[0] = ptr_src[3];
	ptr_dst[1] = ptr_src[2];
	ptr_dst[2] = ptr_src[5];
	ptr_dst[3] = ptr_src[1];
	ptr_dst[4] = ptr_src[0];
	ptr_dst[5] = ptr_src[4];
	ptr_dst[6] = ptr_src[7];
	ptr_dst[7] = ptr_src[6];
	ptr_dst[8] = 1.0;
}

inline void TransformCoordenatesPerspective(const double r,const double c,const cv::Mat&Tpers,double&x, double&y)
{
	const double *ptr_p	= Tpers.ptr<double>(0);

	double den	= (c*ptr_p[6])  + (r*ptr_p[7]) + ptr_p[8];
	x 				= ((c*ptr_p[0]) + (r*ptr_p[1]) + ptr_p[2])/den;
	y 				= ((c*ptr_p[3]) + (r*ptr_p[4]) + ptr_p[5])/den;

}

double PerspectiveMeanError(const cv::Size&,const cv::Mat&,const cv::Mat&);

void TestPerspectiveFunction();

double FindPerspectiveTransform(const cv::Mat&src,const cv::Mat&transform,cv::Mat&tperspective,
                                const int type,const int npixs,const cv::Vec2d&sigmas,const int method  = DGE,
                                const int estimator = 1);

void ApplyPerspectiveTransform(const cv::Mat&src,cv::Mat&dst,const cv::Mat&perspective_transformation,
											const long spline_degree = 3,const bool inverse=false);

class KPPerspective:public KernelP
{
	public:

		KPPerspective(const unsigned long seed,const bool verbose):KernelP(seed,verbose){}
		~KPPerspective(){};
		void setTransformation(const cv::Mat&transformation) {transformation.copyTo(_transformation);}

		// Retorna una matriz identidad de acuerdo al tipo de transformacion
		cv::Mat GetInicializeT(void)
		{
			cv::Mat result = cv::Mat::zeros(1,9,CV_64F);
			result.at<double>(0,0) = 1.0;
			result.at<double>(0,3) = 1.0;
			return result;
		}

		//==========DECLARACIONES PARTICULARES DE LAS FUNCIONES VIRTUALES========
		void setNumberofParameters(){_parameters=9;};
		double KPFunction(const cv::Mat&tperspective,const cv::Mat&samples);
		int takeSamplesGradient(std::vector<cv::Mat>&M,const cv::Mat&tperspective);
		int takeSamplesT(cv::Mat&samples,const cv::Mat&tperspective, const int nsamples=0) {return 0;}
		int takeSamplesBetweenT(cv::Mat&samples,const cv::Mat&tperspective,const cv::Mat&tperspective2) {return 0;}
		
	private:
		//Transforma (perspectiva) las coordenadas del pixel muestreado
		inline void CordPerspective(const double r,const double c,const cv::Mat&Tpers,double&x, double&y)
			{
				const double *ptr_p	= Tpers.ptr<double>(0);

				double den	= (c*ptr_p[6])  + (r*ptr_p[7]) + ptr_p[8];
				x 				= ((c*ptr_p[0]) + (r*ptr_p[1]) + ptr_p[2])/den;
				y 				= ((c*ptr_p[3]) + (r*ptr_p[4]) + ptr_p[5])/den;

			}
		void GetSamples(const cv::Mat&tperspective,const int rt, const int ct,cv::Mat&mre,cv::Mat&mco);
		cv::Mat _transformation;
};


#endif

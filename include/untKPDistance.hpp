#ifndef UNTKPDISTANCE_INCLUDED_H
#define UNTKPDISTANCE_INCLUDED_H

#include <KernelP.hpp>
#include <KPAffin.hpp>
#include <untOptimization.hpp>
#include <untImageOperations.hpp>

class KPDistance;

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @class	Membrecy
///
/// @brief	Membrecy. 
///
/// @author	Aslan
/// @date	09/09/2010
////////////////////////////////////////////////////////////////////////////////////////////////////

class Membrecy
{
	public:
            Membrecy(const double alpha):_alpha(alpha){_type=0;};
            Membrecy(const Membrecy&other):_alpha(other._alpha){}

            //Funciones para modificar el parametro
            inline void setAlpha(const double alpha) {_alpha=alpha;}

            //Funciones para obtener el parametro
            inline double   getAlpha()	const {return _alpha;}
            inline int      getType()	const {return _type;}

            //Operator
            virtual double operator() (const double value)
            {
                    return value;
            }

	protected:
            int _type;
            double _alpha;

};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @class	Membrecy1
///
/// @brief	Membrecy 1. 
///
/// @author	Aslan
/// @date	13/09/2010
////////////////////////////////////////////////////////////////////////////////////////////////////

class Membrecy1: public Membrecy
{
	public:
	    Membrecy1(const double alpha):Membrecy(alpha) {_type=1;}
            double operator() (const double value)
            {
                    return 1.0/(1.0 + (_alpha*value));
            }

	private:

};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @class	Membrecy2
///
/// @brief	Membrecy 2. 
///
/// @author	Aslan
/// @date	13/09/2010
////////////////////////////////////////////////////////////////////////////////////////////////////

class Membrecy2: public Membrecy
{
	public:
	    Membrecy2(const double alpha):Membrecy(alpha) {_type=2;}
            double operator() (const double value)
            {
                    return std::exp(-(_alpha*value));
            }

	private:

};

inline Membrecy* setMembrecy(const int type,const double alpha)
{
    Membrecy *ptr_result = NULL;
    switch(type)
    {
        case 0:
                ptr_result = new Membrecy(alpha);
        break;
        case 1:
                ptr_result = new Membrecy1(alpha);
        break;
        case 2:
                ptr_result = new Membrecy2(alpha);
        break;
        default:
                throw KP::Error("Not membrecy function type",__FUNCTION__);
    }
    return ptr_result;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	void RegisterDistance(KPDistance&kp,cv::Mat&taffin,const double lambda=0.1,
/// const int maxiters=500,const int method=3)
///
/// @brief	Register distance. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param [in,out]	kp		The kp. 
/// @param [in,out]	taffin	The taffin. 
/// @param	lambda			The lambda. 
/// @param	maxiters		The maxiters. 
/// @param	method			The method. 
////////////////////////////////////////////////////////////////////////////////////////////////////

void RegisterDistance(KPDistance&kp,cv::Mat&taffin,const double lambda=0.1,const int maxiters=500,
                        const int method=DGE);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	cv::Mat RegisterDistance(const cv::Mat&original,const cv::Mat&transform,
/// const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD, const double&factor,const double&factorD,
/// const int npixs,const double&lambda, const int&maxiters,const int&pyr_levels)
///
/// @brief	Register distance. 
///
/// @author	Aslan
/// @date	09/09/2010
///
/// @exception	Error	Thrown when error. 
///
/// @param	original	The original. 
/// @param	transform	The transform. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	factor		The factor. 
/// @param	factorD		The factor d. 
/// @param	npixs		The npixs. 
/// @param	lambda		The lambda. 
/// @param	maxiters	The maxiters. 
/// @param	pyr_levels	The pyr levels. 
///
/// @return	. 
////////////////////////////////////////////////////////////////////////////////////////////////////

cv::Mat RegisterDistance(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,
                        const cv::Vec2d&sigmasD,const double&factor,const double&factorD,const int npixs,
                        const double&lambda,const int&maxiters,const int&pyr_levels,const int membresia,
                        const double alpha,const int apertureSize, const double threshold1,
                        const double threshold2,const Border_Type b_type);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @class	KPDistance
///
/// @brief	Kp distance. 
///
/// @author	Aslan
/// @date	09/09/2010
////////////////////////////////////////////////////////////////////////////////////////////////////

class KPDistance : public KPAffin
{
    public:
        KPDistance(const unsigned long seed, const bool verbose,
                    const int apertureSize, const double threshold1,
                    const double threshold2,const Border_Type b_type=CANNY
                    ):KPAffin(seed,verbose)
        {
                _max_distance   = _sigmamD = _sigmacD = 0.0;
                _nbinsD         = 0;
                _apertureSize	= apertureSize;
                _threshold1	= threshold1;
                _threshold2	= threshold2;
                _b_type         = b_type;
        };

        ~KPDistance(){}

        void InitializeDistance(const cv::Mat&original,const cv::Mat&transform);

        //Funciones para la modificacion de parametros
        void setMaxDistance(const double maxdistance) {_max_distance = maxdistance;}
        void setBinsD(const int nbinsD) {_nbinsD = nbinsD;}
        void setSigmaMDistance(const double sigmaMD) {_sigmamD = sigmaMD;}
        void setSigmaCDistance(const double sigmaCD) {_sigmacD = sigmaCD;}
        void setSigmasDistance(const cv::Vec2d&sigmasD) {_sigmamD = sigmasD[0]; _sigmacD = sigmasD[1];}

        void setDistanceParameters(const int nbinsD,const cv::Vec2d&sigmasD,const double maxdistance);

        void setMembrecy(Membrecy *membrecy) {_membrecy = membrecy;}

        inline cv::Mat GetInicializeT(void)
        {
                cv::Mat result = cv::Mat::eye(2,3,CV_64F);
                return result;
        };


        inline virtual void setEstimatorType(const int estimator_type)
        {
                _estimator_type = estimator_type;

                switch(_estimator_type)
                {
                        case 1: _const_estimator_normalize = 4.0/(4 * _npixs * _npixs); break;
                        case 2: _const_estimator_normalize = 1.0/(4 * _npixs * _npixs); break;
                        case 3: _const_estimator_normalize = 2.0/( (2*_npixs) * ((2*_npixs)-1) ); break;
                        default: throw KP::Error("Estimator dont exist",__FUNCTION__); break;
                }

                _const_estimator_normalize *=_const_estimator_normalize;
            }

            //Funciones modificadas
        int takeSamplesGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2,const cv::Mat&transform);
        void EvalKernels(const cv::Mat&M1,const cv::Mat&M2,cv::Mat&pred);

        //Funciones para estimacion del gradiente
        int df(const cv::Mat&transform,cv::Mat&gradient)
        {
                std::vector<cv::Mat>M1;
                std::vector<cv::Mat>M2;
                int tmp_samples = KPDistance::takeSamplesGradient(M1,M2,transform);
                KPDistance::EstimateGradient(M1,M2).copyTo(gradient);
                return tmp_samples;
        }


        cv::Mat EstimateGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2);

    private:

        int _nbinsD;
        int _apertureSize;

        double _threshold1;
        double _threshold2;

        double _max_distance;
        double _sigmamD;
        double _sigmacD;

        Membrecy *_membrecy;

        Border_Type _b_type;

        //Matrices de KP con distancia
        cv::Mat KMD; //Kernel marginal de distancia
        cv::Mat KCD; //Kernel conjunta de distancia
        cv::Mat matBinsD; //Matriz de bins para la imagen transformada
        cv::Mat _dist_original; //Contiene la transformada de distanca de original
        cv::Mat _dist_transform; //Contiene la transformada de distanca de transform
        cv::Mat CoeffIpD; //Matriz de interpolacion para la distancia

};

#endif

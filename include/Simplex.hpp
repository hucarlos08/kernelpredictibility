#ifndef SIMPLEX_H_INCLUDED
#define SIMPLEX_H_INCLUDED

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <untErrors.hpp>

class Simplex
{
	public:
		Simplex(const double ftoll):ftol(ftoll){};
		template<class T>
		cv::Mat minimize(const cv::Mat&point, const double del, T&func);
		template<class T>
		cv::Mat minimize(const cv::Mat&point, const cv::Mat &mdels,T&func);
		template<class T>
		cv::Mat minimize(const cv::Mat&mpp, T&func);
		template<class T>
		double amotry(cv::Mat&mp, cv::Mat&vyy, cv::Mat&vpsum,const int ihi, const double fac, T&func);

		/* Funcion auxiliar*/
		void get_psum(const cv::Mat &mp, cv::Mat &vpsum)
		{
			if( (!mp.isContinuous()) || (!vpsum.isContinuous()) )
				throw KP::Error("Matrix not continues",__FUNCTION__);
			if( vpsum.rows != 1)
				throw KP::Error("MISMATCH SIZE",__FUNCTION__);
			const double *pp 	= NULL;
				  double *psum	= vpsum.ptr<double>(0);
			for (int j=0;j<ndim;j++)
				{
					double sum=0.0;
					for (int i=0;i<mpts;i++)
					{
						pp = mp.ptr<double>(i);
						sum += pp[j];
					}
					psum[j]=sum;
				}
		}
		~Simplex(void){};

	//Variables
		const double ftol;
		int nfunc;
		int mpts;
		int ndim;
		double fmin;
		cv::Mat_<double>vy;
		cv::Mat_<double>p;
};

/* Multidimensional minimization of the function or functor func(x), where x[0..ndim-1]
	is a vector in ndim dimensions, by the downhill simplex method of Nelder and Mead.
	The initial simplex is specified as in equation (10.5.1) by a point[0..ndim-1] and a
	constant displacement del along each coordinate direction. Returned is the location of the
	minimum.
*/
template<class T>
cv::Mat Simplex::minimize(const cv::Mat&point, const double del, T&func)
{
	cv::Mat dels(1,point.cols,CV_64F,cv::Scalar(del));
	return minimize(point,dels,func);
}

/* Alternative interface that takes different displacements dels[0..ndim-1] in different directions
	for the initial simplex.
*/
template<class T>
cv::Mat Simplex::minimize(const cv::Mat&vpoint, const cv::Mat &mdels,T&func)
{
	ndim=vpoint.cols;
	cv::Mat mpp(ndim+1,ndim,CV_64F,cv::Scalar(0.0));
	for (int i=0;i<ndim+1;i++)
		{
					double *pp 		= mpp.ptr<double>(i);
			const   double *dels	= mdels.ptr<double>(0);
			for (int j=0;j<ndim;j++)
			{
				const double *point = vpoint.ptr<double>(0);
				pp[j]=point[j];
			}
			if (i !=0 ) pp[i-1] += dels[i-1];
		}
	return minimize(mpp,func);
}

/* Most general interface: initial simplex specified by the matrix pp[0..ndim][0..ndim-1].
	Its ndim+1 rows are ndim-dimensional vectors that are the vertices of the starting simplex.
*/
template<class T>
cv::Mat Simplex::minimize(const cv::Mat&mpp, T&func)
{
	const int 	 NMAX = 5000;
	const double TINY = 1.0e-10;
	int ihi,ilo,inhi;
	mpts = mpp.rows;
	ndim = mpp.cols;

	//VecDoub psum(ndim),pmin(ndim),x(ndim);
	cv::Mat vpsum(1,ndim,CV_64F,cv::Scalar(0.0));
	cv::Mat vpmin(1,ndim,CV_64F,cv::Scalar(0.0));
	cv::Mat vx(1,ndim,CV_64F,cv::Scalar(0.0));

	double *psum	= vpsum.ptr<double>(0);
	double *pmin	= vpmin.ptr<double>(0);
	double *x		= vx.ptr<double>(0);

	mpp.copyTo(p);
	//y.resize(mpts);
	vy 			= cv::Mat_<double>::zeros(1,mpts);
	double *y 	= vy[0];

	for (int i=0;i<mpts;i++)
		{
			for (int j=0;j<ndim;j++)
				x[j]=p[i][j];
			y[i]=func(vx);
		}
	nfunc=0;
	get_psum(p,vpsum);
	for (;;)
		{
			ilo=0;
			ihi = y[0]>y[1] ? (inhi=1,0) : (inhi=0,1);
			for (int i=0;i<mpts;i++)
				{
					if (y[i] <= y[ilo]) ilo=i;
					if (y[i] > y[ihi])
						{
							inhi= ihi;
							ihi	= i;
						}
					else if (y[i] > y[inhi] && i != ihi) inhi=i;
				}
			double rtol=2.0*abs(y[ihi]-y[ilo])/(abs(y[ihi])+abs(y[ilo])+TINY);
			if (rtol < ftol)
				{
					std::swap(y[0],y[ilo]);
					for (int i=0;i<ndim;i++)
						{
							std::swap(p[0][i],p[ilo][i]);
							pmin[i]=p[0][i];
						}
					fmin=y[0];
					return vpmin;
				}
			if (nfunc >= NMAX) return vpmin;//throw Error("NMAX exceeded",__FUNCTION__);
			nfunc += 2;
			double ytry=amotry(p,vy,vpsum,ihi,-1.0,func);
			if (ytry <= y[ilo])
				ytry=amotry(p,vy,vpsum,ihi,2.0,func);
			else if (ytry >= y[inhi])
				{
					double ysave=y[ihi];
					ytry=amotry(p,vy,vpsum,ihi,0.5,func);
					if (ytry >= ysave)
						{
							for (int i=0;i<mpts;i++)
								{
									if (i != ilo)
										{
											for (int j=0;j<ndim;j++)
												p[i][j]=psum[j]=0.5*(p[i][j]+p[ilo][j]);
											y[i]=func(vpsum);
										}
								}
							nfunc += ndim;
							get_psum(p,vpsum);
						}
				}
			else --nfunc;
		}

}

/* Helper function: Extrapolates by a factor fac through the face of the simplex across from
	the high point, tries it, and replaces the high point if the new point is better.
*/
template<class T>
double Simplex::amotry(cv::Mat&mp, cv::Mat&vyy, cv::Mat&vpsum,const int ihi, const double fac, T&func)
{
	//VecDoub ptry(ndim);
	cv::Mat vptry 	= cv::Mat::zeros(1,ndim,CV_64F);
	//Definimos los apuntadores
	double *ptry	= vptry.ptr<double>(0);
	double *y		= vyy.ptr<double>(0);
	double *psum	= vpsum.ptr<double>(0);
	double *pp		= NULL;
	double fac1=(1.0-fac)/ndim;
	double fac2=fac1-fac;

	pp = mp.ptr<double>(ihi);
	for(int j=0;j<ndim;j++)
		ptry[j]=psum[j]*fac1-pp[j]*fac2;
	double ytry=func(vptry);
	if (ytry < y[ihi])
		{
			y[ihi]=ytry;
			for (int j=0;j<ndim;j++)
				{
					pp = mp.ptr<double>(ihi);
					psum[j] += ptry[j]-pp[j];
					pp[j]=ptry[j];
				}
		}
	return ytry;
}

#endif // SIMPLEX_H_INCLUDED

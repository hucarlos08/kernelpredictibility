#ifndef UNTIMAGEOPERATIONS_H_INCLUDED
#define UNTIMAGEOPERATIONS_H_INCLUDED

#include <iostream>
#include <untErrors.hpp>
#include <opencv2/core.hpp>

namespace cv{


    template<class T,class P>
    void BinaryThreshold(const cv::Mat&src,cv::Mat&dst,const double threshold,const double value)
    {
        if(src.type()!=cv::DataType<T>::type)
            throw KP::Error(MISMATCH_TYPE,__FUNCTION__);

        if(src.channels()!=1)
            throw KP::Error(NOT_1_CHANNEL,__FUNCTION__);

        int rows    = src.rows;
        int cols    = src.cols;

        dst = cv::Mat::zeros(src.size(),cv::DataType<P>::type);

        if(src.isContinuous())
        {
            cols *= rows;
            rows = 1;
        }
        for(int i = 0; i < rows; i++)
        {
            const T *ptr_src = src.ptr<T>(i);
            P *ptr_dst = dst.ptr<P>(i);

            for(int j = 0; j < cols; j++)
            {
                if(threshold != 0.0)
                {
                    if(ptr_src[j] >= threshold )
                        ptr_dst[j] = static_cast<P>(value);
                }
                else
                {
                    ptr_dst[j] = static_cast<P>(ptr_src[j]);
                }
            }
        }
    }

    template<class T,class P>
    void MeanStdDevImages(const cv::Mat&src,cv::Mat&dst_mean,cv::Mat&dst_std,const int wsize)
    {
        if(src.type()!=cv::DataType<T>::type)
            throw KP::Error(MISMATCH_TYPE,__FUNCTION__);

        if(src.channels()!=1)
            throw KP::Error(NOT_1_CHANNEL,__FUNCTION__);

        dst_mean	= cv::Mat::zeros(src.size(),cv::DataType<P>::type);
        dst_std	= cv::Mat::zeros(src.size(),cv::DataType<P>::type);

        int i,j,j0,j1,i0,i1,ii,jj,nkrlo,nkclo,nkrhi,nkchi ;
        double sum1	= 0.0, sum2		= 0.0 ;
        double mean	= 0.0, std_dev	= 0.0;
        double tmp	= 0.0;

        int rows = src.rows;
        int cols = src.cols;
        int cardinality = 0;

        //Apuntadores a los elementos de las imagenes
        const T *ptr_src	= NULL;
        P *ptr_dst_mean     = NULL;
        P *ptr_dst_std	= NULL;

        nkrlo = nkclo = (wsize-1) / 2 ;
        nkrhi = wsize - nkrlo;
        nkchi = wsize - nkclo;
        for (i = 0; i < rows; i++)
        {
            ptr_dst_mean	= dst_mean.ptr<P>(i);
            ptr_dst_std	= dst_std.ptr<P>(i);
            for (j = 0; j < cols; j++)
            {
                i0 = i - nkrlo ; if (i0 < 0)        i0 = 0 ;
                i1 = i + nkrhi ; if (i1 > rows)     i1 = rows ;
                j0 = j - nkclo ; if (j0 < 0)        j0 = 0 ;
                j1 = j + nkchi ; if (j1 > cols)		 j1 = cols ;
                cardinality=0;
                mean=std_dev=sum1=sum2=0.0;
                for (ii = i0 ; ii < i1 ; ii++)
                {
                    ptr_src			= src.ptr<T>(ii);
                    for (jj = j0 ; jj < j1 ; jj++)
                    {
                        cardinality++;
                        tmp	=	static_cast<double>(ptr_src[jj]);
                        sum1 += tmp;
                        sum2 += tmp * tmp;
                    }
                }
                mean             = (cardinality!=0)?(sum1/cardinality):0.0;
                std_dev          = (sum2-mean*mean*cardinality)/(cardinality-1.0);
                std_dev          = std::sqrt(std::fabs(std_dev));
                ptr_dst_mean[j]  = static_cast<P>(mean);
                ptr_dst_std[j]   = static_cast<P>(std_dev);
            }
        }

    }


}//End my cv space

#endif

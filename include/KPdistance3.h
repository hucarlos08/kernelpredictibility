#ifndef KPDISTANCE3_H
#define	KPDISTANCE3_H

#include <iostream>
#include <KernelP.hpp>
#include <KPAffin.hpp>
#include <untKPDistance.hpp>
#include <untOptimization.hpp>
#include <untImageOperations.hpp>

using namespace std;

class KPDistance3; //Forward declaration

class WeightFunction
{
    public:
        WeightFunction(const double alpha):_alpha(alpha){_type=0;};
        WeightFunction(const WeightFunction&other):_alpha(other._alpha){}

        //Funciones para modificar el parametro

        inline void setAlpha(const double alpha) {_alpha=alpha;}

        //Funciones para obtener el parametro
        inline double   getAlpha()	const {return _alpha;}
        inline int      getType()	const {return _type;}

        //Operator
        virtual double operator() (const double value1,const double value2)
        {
            return std::abs(value1-value2);
        }

    protected:
        double _alpha;
        int _type;
};


class WeightFunction1: public WeightFunction
{
	public:
            WeightFunction1(const double alpha):WeightFunction(alpha) {_type=1;};
            double operator() (const double value1,const double value2)
            {
                double temp = std::abs(value1-value2);
                return 1.0 + std:: exp(-temp);
            }

	private:

};


inline WeightFunction* setWeightFunction(const int type,const double alpha)
{
    WeightFunction *ptr_result = NULL;
    switch(type)
    {
        case 0:
                ptr_result = new WeightFunction(alpha);
        break;
        case 1:
                ptr_result = new WeightFunction(alpha);
        break;
        case 2:
                ptr_result = new WeightFunction(alpha);
        break;
        default:
                throw KP::Error("Not WeightFunction function type",__FUNCTION__);
    }
    return ptr_result;
}

//============================================================================================================
void RegisterDistance3(KPDistance3&kp,cv::Mat&taffin,const double lambda=0.1,const int maxiters=500,
                        const int method=DGE);

cv::Mat RegisterDistance3(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,
                         const cv::Vec2d&sigmasD,const double&factor,const double&factorD,const int npixs,
                         const double&lambda,const int&maxiters,const int&pyr_levels,const int membresia,
                         const double alpha,const int apertureSize, const double threshold1,
                         const double threshold2,const Border_Type b_type);


//============================================================================================================
class KPDistance3 : public KPAffin
{
    public:
        KPDistance3(const unsigned long seed, const bool verbose,
                    const int apertureSize, const double threshold1,
                    const double threshold2,const Border_Type b_type=CANNY
                    ):KPAffin(seed,verbose)
        {
                _max_distance   = _sigmamD = _sigmacD = 0.0;
                _nbinsD         = 0;
                _apertureSize	= apertureSize;
                _threshold1	= threshold1;
                _threshold2	= threshold2;
                _b_type         = b_type;
        };

        ~KPDistance3(){};

        void InitializeDistance(const cv::Mat&original,const cv::Mat&transform);

        //Funciones para la modificacion de parametros
        void setMaxDistance(const double maxdistance) {_max_distance = maxdistance;}
        void setBinsD(const int nbinsD) {_nbinsD = nbinsD;}
        void setSigmaMDistance(const double sigmaMD) {_sigmamD = sigmaMD;}
        void setSigmaCDistance(const double sigmaCD) {_sigmacD = sigmaCD;}
        void setSigmasDistance(const cv::Vec2d&sigmasD) {_sigmamD = sigmasD[0]; _sigmacD = sigmasD[1];}

        void setDistanceParameters(const int nbinsD,const cv::Vec2d&sigmasD,const double maxdistance);

        void setWeightFunction(WeightFunction *weightfunction) {_weightfunction = weightfunction;}
         void setMembrecy(Membrecy *membrecy) {_membrecy = membrecy;}

        inline cv::Mat GetInicializeT(void)
        {
                cv::Mat result = cv::Mat::eye(2,3,CV_64F);
                return result;
        };


        inline virtual void setEstimatorType(const int estimator_type)
        {
                _estimator_type = estimator_type;

                switch(_estimator_type)
                {
                        case 1: _const_estimator_normalize = 4.0/(4 * _npixs * _npixs); break;
                        case 2: _const_estimator_normalize = 1.0/(4 * _npixs * _npixs); break;
                        case 3: _const_estimator_normalize = 2.0/( (2*_npixs) * ((2*_npixs)-1) ); break;
                        default: throw KP::Error("Estimator dont exist",__FUNCTION__); break;
                }

                _const_estimator_normalize *=_const_estimator_normalize;
            }

            //Funciones modificadas
        int takeSamplesGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2,const cv::Mat&transform);
        void EvalKernels(const cv::Mat&M1,const cv::Mat&M2,cv::Mat&pred);

        //Funciones para estimacion del gradiente
        int df(const cv::Mat&transform,cv::Mat&gradient)
        {
                std::vector<cv::Mat>M1;
                std::vector<cv::Mat>M2;
                int tmp_samples = KPDistance3::takeSamplesGradient(M1,M2,transform);
                KPDistance3::EstimateGradient(M1,M2).copyTo(gradient);
                return tmp_samples;
        }


        cv::Mat EstimateGradient(std::vector<cv::Mat>&M1,std::vector<cv::Mat>&M2);

    private:

        int _nbinsD;
        int _apertureSize;

        double _threshold1;
        double _threshold2;

        double _max_distance;
        double _sigmamD;
        double _sigmacD;

        Membrecy *_membrecy;
        WeightFunction *_weightfunction;

        Border_Type _b_type;

        //Matrices de KP con distancia
        cv::Mat KMD; //Kernel marginal de distancia
        cv::Mat KCD; //Kernel conjunta de distancia
        cv::Mat matBinsD; //Matriz de bins para la imagen transformada
        cv::Mat _membrecy_original; //Contiene la transformada de distanca de original
        cv::Mat _membrecy_transform; //Contiene la transformada de distanca de transform
        cv::Mat CoeffIpD; //Matriz de interpolacion para la distancia

};

#endif	/* KPDISTANCE3_H */


#ifndef UNTCORRELATIONS_INCLUDED
#define UNTCORRELATIONS_INCLUDED

#include <opencv2/core.hpp>

/*
    Clase para el manejo de correlaciones
*/
//Define type for pair correlations
typedef std::pair<cv::Point2f,cv::Point2f>correspondence;

template<class T>
class Correlation
{
    public:
        Correlation(){ _value = 0.;}
        Correlation(const T&value, const correspondence&corr)
        {
            _value      = value;
            _correlation= corr;
        }

        //Functions for set parameters
        void setValue(const T&value){_value = value;}
        void setCorrelation(const correspondence&corr){_correlation = corr;}
        void setCorrelationFirst(const cv::Point2f&first){_correlation.first = first;}
        void setCorrelationSecond(const cv::Point2f&second){_correlation.second = second;}

        //Functions for gets the parameters
        T getValue()             const {return _value;}
        correspondence getCorrelation() const {return _correlation;}
        cv::Point2f getCorrelationFirst() const {return _correlation.first;}
        cv::Point2f getCorrelationSecond() const {return _correlation.second;}
    private:
        T _value;
        correspondence _correlation;

};

template<class T>
bool operator < (const Correlation<T>&c1,const Correlation<T>&c2) {return c1.getValue() < c2.getValue();}

#endif

#ifndef UNTKPDISTANCE2_INCLUDED_H
#define UNTKPDISTANCE2_INCLUDED_H

#include <iostream>
#include <KernelP.hpp>
#include <KPAffin.hpp>
#include <untPowell.hpp>
#include <untKPDistance.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @fn	cv::Mat RegisterDistance2(const cv::Mat&original,const cv::Mat&transform,
/// const cv::Vec2d&sigmas,const cv::Vec2d&sigmasD, const double&factor,const double&factorD,
/// const int npixs,const double&lambda, const int&maxiters,const int&pyr_levels)
///
/// @brief	Register distance 2. 
///
/// @author	Aslan
/// @date	13/09/2010
///
/// @param	original	The original. 
/// @param	transform	The transform. 
/// @param	sigmas		The sigmas. 
/// @param	sigmasD		The sigmas d. 
/// @param	factor		The factor. 
/// @param	factorD		The factor d. 
/// @param	npixs		The npixs. 
/// @param	lambda		The lambda. 
/// @param	maxiters	The maxiters. 
/// @param	pyr_levels	The pyr levels. 
///
/// @return	. 
////////////////////////////////////////////////////////////////////////////////////////////////////

cv::Mat RegisterDistance2(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,
                          const cv::Vec2d&sigmasD,const double&factor,const double&factorD,const int npixs,
                          const double&lambda,const int&maxiters,const int&pyr_levels,const int membresia,
                          const double alpha,const int apertureSize, const double threshold1,
                          const double threshold2,const Border_Type b_type);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// @class	KPDistance2
///
/// @brief	Kp distance 2. 
///
/// @author	Aslan
/// @date	13/09/2010
////////////////////////////////////////////////////////////////////////////////////////////////////

class KPDistance2
{
	public:
            KPDistance2(const unsigned long seed,const int apertureSize,const double threshold1,
                        const double threshold2,const Border_Type b_type=CANNY,
                        const bool verbose=false):_kp(seed,verbose),_kpD(seed,verbose)
            {
                    _verbose            = verbose;
                    _apertureSize	= apertureSize;
                    _threshold1         = threshold1;
                    _threshold2         = threshold2;
                    _b_type             = b_type;
                    _alpha              = 1.0;
                    _beta               = 1.0;
            }
            void Inicialize(const cv::Mat&image,const cv::Mat&transform,const cv::Vec2d&sigmas,
                            const cv::Vec2d&sigmasD,const double&factor,const double&factorD,
                            const int npixs,const double epsdiff);

            void setMembrecy(Membrecy *membrecy) {_membrecy = membrecy;}

            void setAplha(const double alpha) {_alpha=alpha;}
            void setBeta(const double beta) {_beta=beta;}

            double operator () (const cv::Mat&transformation)
            {
                    return _kp(transformation) + _kpD(transformation);
            };

            int df(const cv::Mat&transformation,cv::Mat&result)
            {

                    std::vector<cv::Mat>vector_samples;
                    int tmp_samples	= _kp.takeSamplesGradient(vector_samples,transformation);
                    cv::Mat gradient	= _kp.EstimateGradient(vector_samples);
                    cv::Mat gradientD	= _kpD.EstimateGradient(vector_samples);
                    result = (_alpha*gradient) + (_beta*gradientD);
                    return tmp_samples;
            };
	private:

            //For write results
            bool _verbose;

            int _apertureSize;

            double _threshold1;
            double _threshold2;

            double _alpha;
            double _beta;

            Membrecy *_membrecy;

            Border_Type _b_type;

            KPAffin _kp;
            KPAffin _kpD;
};


#endif
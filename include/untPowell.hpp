#ifndef UNTPOWELL_H_INCLUDED
#define UNTPOWELL_H_INCLUDED

//Standar includes
#include <iostream>

//OpenCV includes
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

//Line serach includes
#include <Bracketmethod.hpp>

template <class T>
class F1dim
{
	public:

		F1dim(const cv::Mat &vpp, const cv::Mat &vxii, T &funcc) : vp(vpp),
			vxi(vxii), n(vpp.cols), func(funcc) {vxt=cv::Mat::zeros(1,n,CV_64F);}
		double operator() (const double x)
		{
			const double *p	= vp.ptr<double>(0);
			const double *xi	= vxi.ptr<double>(0);
				  double  *xt	= vxt.ptr<double>(0);
			for (int j=0;j<n;j++)
				xt[j]=p[j]+x*xi[j];
			return func(vxt);
		}
    private:
      const cv::Mat &vp;
		const cv::Mat &vxi;
		int n;
		T &func;
		cv::Mat vxt;
};

template<class T>
class Linemethod
{

	public:
		Linemethod(T &funcc) : func(funcc) {}
		double linmin()
		{
			double ax,xx,xmin;
			n=vp.cols;
			F1dim<T> f1dim(vp,vxi,func);
			ax=0.0;
			xx=1.0;

			////Buscamos pixeles en la interseccion
			//cv::Mat transform1;
			//cv::Mat transform2;

			//vxi.copyTo(transform1);
			//cv::Mat temp = (vp+vxi);
			//temp.copyTo(transform2);

			//cv::Mat samples;
			//func.takeSamplesBetweenT(samples,transform1,transform2);
			//func.setSamples(samples);

			Brent brent;
			brent.bracket(ax,xx,f1dim);
			xmin=brent.minimize(f1dim);
			for (int j=0;j<n;j++)
				{
					double *xi 	= vxi.ptr<double>(0);
					double *p	= vp.ptr<double>(0);
					xi[j] *= xmin;
					p[j] += xi[j];
				}
			return brent.fmin;
		}
    //Variables a heredar
    public:
        inline double* getPtrVp()   {return vp.ptr<double>(0);}
        inline double* getPtrVxi()  {return vxi.ptr<double>(0);}

    protected:
      cv::Mat vp;
		cv::Mat vxi;
		T &func;
		int n;
};


template <class T>
class Df1dim
{
public:
	const cv::Mat &vp;
	const cv::Mat &vxi;
	int n;
	T &funcd;
	cv::Mat vxt;
	cv::Mat vdft;

	//Pointers to data
	const double *p;
	const double *xi;
		  double *xt;
		  double *dft;

	Df1dim(const cv::Mat&pp, const cv::Mat&xii, T &funcdd) : vp(pp),
		vxi(xii), n(pp.cols), funcd(funcdd)
	{
		vxt		= cv::Mat::zeros(1,n,CV_64F);
		vdft		= cv::Mat::zeros(1,n,CV_64F);
	}
	void assignptrs(void)
	{
		p		= vp.ptr<double>(0);
		xi		= vxi.ptr<double>(0);
		xt		= vxt.ptr<double>(0);
		dft	= vdft.ptr<double>(0);
	}
	double operator()(const double x)
	{
		assignptrs();
		for (int j=0;j<n;j++)
			xt[j]=p[j]+x*xi[j];
		return funcd(vxt);
	}
	double df(const double x)
	{
		assignptrs();
		double df1=0.0;
		funcd.df(vxt,vdft);
		for (int j=0;j<n;j++)
			df1 += dft[j]*xi[j];
		return df1;
	}
};


template <class T>
class Dlinemethod
{
public:

	Dlinemethod(T &funcc) : func(funcc) {vp=cv::Mat::zeros(1,1,CV_64F);}
	double linmin()
	{
		double ax,xx,xmin;
		double *xi	= vxi.ptr<double>(0);
		double *p	= vp.ptr<double>(0);
		n=vp.cols;
		Df1dim<T> df1dim(vp,vxi,func);
		ax=0.0;
		xx=1.0;
		Dbrent dbrent;
		dbrent.bracket(ax,xx,df1dim);
		xmin=dbrent.minimize(df1dim);
		for (int j=0;j<n;j++)
			{
				xi[j] *= xmin;
				p[j] += xi[j];
			}
		return dbrent.fmin;
	}

	public:
        inline double* getPtrVp()   {return vp.ptr<double>(0);}
        inline double* getPtrVxi()  {return vxi.ptr<double>(0);}

	public://References to variables
        cv::Mat vp;
        cv::Mat vxi;
        T &func;
        int n;

};


template <class T>
class Powell: public Linemethod<T>
{
	public:
		~Powell(void){};
		Powell(T &function, const double ftoll=3.0e-8,const int itmax=200) : 
			Linemethod<T>(function),ftol(ftoll), ITMAX(itmax) {}
		cv::Mat minimize(const cv::Mat&vpp)
			{
				int n=vpp.cols;
				cv::Mat mximat(n,n,CV_64F,cv::Scalar(0.0));
				for (int i=0;i<n;i++)
				{
					double *ximat = mximat.ptr<double>(i);
					ximat[i]=1.0;
				}
				return minimize(vpp,mximat);
			}
		cv::Mat minimize(const cv::Mat &vpp, cv::Mat &mximat);

        //Variables
        using Linemethod<T>::func;
        using Linemethod<T>::linmin;
        using Linemethod<T>::vp;
        using Linemethod<T>::vxi;

		int iter;
		double fret;
		const double ftol;
		const int  ITMAX;
};



template<class T>
cv::Mat Powell<T>::minimize(const cv::Mat &vpp, cv::Mat &mximat)
{
	
	const double    TINY    = 1.0e-25;
	double fptt;
	int n = vpp.cols;
	vpp.copyTo(this->vp);

	cv::Mat vpt(1,n,CV_64F,cv::Scalar(0.0));
	cv::Mat vptt(1,n,CV_64F,cv::Scalar(0.0));

	Linemethod<T>::vxi = cv::Mat::zeros(1,n,CV_64F);

	//Apuntadores a las matrices
	double *pt	= vpt.ptr<double>(0);
	double *ptt	= vptt.ptr<double>(0);

   double *p		= Linemethod<T>::getPtrVp();
	double *xi		= Linemethod<T>::getPtrVxi();
	double *ximat	= NULL;


	fret	= func(vp);

	for (int j=0;j<n;j++) pt[j]=p[j];

	for (iter=0;;++iter)
	{
		double fp	= fret;
		int ibig		= 0;
		double del	= 0.0;
		for (int i=0;i<n;i++)
			{
				for (int j=0;j<n;j++)
				{
					ximat	= mximat.ptr<double>(j);
					xi[j]   = ximat[i];
				}
				fptt=fret;
				fret=linmin();
				if (fptt-fret > del)
					{
						del	= fptt-fret;
						ibig	= i+1;
					}
			}
		if (2.0*(fp-fret) <= ftol*(abs(fp)+abs(fret))+TINY)
			{
				return vp;
			}
		if (iter == ITMAX) return vp;//throw Error("powell exceeding maximum iterations.",__FUNCTION__);
		for (int j=0;j<n;j++)
			{
				ptt[j]=2.0*p[j]-pt[j];
				xi[j]=p[j]-pt[j];
				pt[j]=p[j];
			}
		fptt=func(vptt);
		if (fptt < fp)
			{
				double t=2.0*(fp-2.0*fret+fptt)*SQR(fp-fret-del)-del*SQR(fp-fptt);
				if (t < 0.0)
					{
						fret=linmin();
						for (int j=0;j<n;j++)
							{
								ximat = mximat.ptr<double>(j);
								ximat[ibig-1]	= ximat[n-1];
								ximat[n-1]		= xi[j];
							}
					}
			}
	}

}

//============================GRADIENTE CONJUGADO==============================

template <class T>
class Frprmn : public Linemethod<T>
{
	public:
		Frprmn(T &funcd, const double ftoll=3.0e-8,const int itmax=200) : 
						Linemethod<T>(funcd),ftol(ftoll),ITMAX(itmax) {}
		cv::Mat minimize(const cv::Mat&vpp);
		
	//Variables
		int iter;
		double fret;
		using Linemethod<T>::func;
		using Linemethod<T>::linmin;
		using Linemethod<T>::vp;
		using Linemethod<T>::vxi;
		const double ftol;
		const int ITMAX;
};

/* Given a starting point vpp[0..n-1], performs the minimization on a function whose value
	and gradient are provided by a functor funcd
*/
template<class T>
cv::Mat Frprmn<T>::minimize(const cv::Mat&vpp)
{
	/*Here ITMAX is the maximum allowed number of iterations; EPS is a small number to
	rectify the special case of converging to exactly zero function value; and GTOL is the
	convergence criterion for the zero gradient test.*/
	const double    EPS     = 1.0e-18;
	const double    GTOL    = 1.0e-8;
	double gg,dgg;
	double gg2, dgg2;
	int n = vpp.cols;
	int s = 0;
	vpp.copyTo(vp);

	cv::Mat mg = cv::Mat::zeros(1,n,CV_64F);
	cv::Mat mh = cv::Mat::zeros(1,n,CV_64F);

	//Inicializamos xi
	vxi = cv::Mat::zeros(1,n,CV_64F);

	double *g	= mg.ptr<double>(0);
	double *h	= mh.ptr<double>(0);
	double *xi	= Linemethod<T>::getPtrVxi();
	double *p	= Linemethod<T>::getPtrVp();

	double fp=func(vp);
	func.df(vp,vxi);
	xi = Linemethod<T>::getPtrVxi();

	for (int j=0;j<n;j++)
		{
			g[j]	= -xi[j];
			xi[j]	= h[j] = g[j];
		}

	//Main loop
	for (int its=0;its<ITMAX;its++)
	{
		iter=its;
		fret=linmin();

		if (2.0*abs(fret-fp) <= ftol*(abs(fret)+abs(fp)+EPS))
			return vp;

		fp = fret;
		s	= func.df(vp,vxi);
		xi = Linemethod<T>::getPtrVxi();

		double test=0.0; //Test for convergence on zero gradient.
		double den=MAX(fp,1.0);

		for (int j=0;j<n;j++)
			{
				double temp=abs(xi[j])*MAX(abs(p[j]),1.0)/den;
				if (temp > test) test=temp;
			}

		if ((test < GTOL)) return vp;
		dgg	= gg	= 0.0;
		dgg2	= gg2	= 0.0;
		for (int j=0;j<n;j++)
			{
				//gg		+= (g[j]*g[j]) + (xi[j]*h[j]);
				////dgg	+= xi[j]*xi[j] ; //This statement for Fletcher-Reeves.
				//dgg += (xi[j]+g[j])*xi[j]; //This statement for Polak-Ribiere

				//Nueva propuesta
				dgg	+= xi[j]*xi[j];
				gg		+= h[j]*(xi[j]-g[j]);

				dgg2	+= xi[j]*(xi[j]-g[j]);
				gg2	+= h[j]*(xi[j]-g[j]);
			}

		if (gg == 0.0) //Unlikely. If gradient is exactly zero, thenwe are already done.
			return vp;

		double gam	= dgg/gg;
		double gam2 = dgg2/gg;

		gam = std::max(0.0,std::min(gam,gam2));
		
		if(its%50==0) gam = 0.0;

		
		for (int j=0;j<n;j++)
			{
				g[j]  = -xi[j];
				xi[j] = h[j] = (g[j]+gam*h[j]);
			}
	}
	return vp;
}

//====================================DESCENSO DE GRADIENTE==============================
template<class T>
class DG: public Linemethod<T>
{
	public:
		DG(T &funcd, const double _lambda=0.1,const int itmax=200) : 
		Linemethod<T>(funcd),lambda(_lambda),ITMAX(itmax) {}
		cv::Mat minimize(const cv::Mat&vpp,bool normalize=false);

	//Variables
		int iter;
		double fret;
		const double lambda;
                const int ITMAX;
		using Linemethod<T>::func;
		using Linemethod<T>::linmin;
		using Linemethod<T>::vp;
		using Linemethod<T>::vxi;
};

template<class T>
cv::Mat DG<T>::minimize(const cv::Mat&vpp,bool normalize)
{
    //Copiamos los datos para evitar alterarlos
    cv::Mat result;
    vpp.copyTo(result);

    cv::Mat gradient_t;
    double l    = lambda;

    for(iter=0; iter<ITMAX; iter++)
    {
        int samples = func.df(result,gradient_t);

        if(normalize)
            {
                gradient_t/=cv::norm(gradient_t);
            }

        if(samples == 0)
            continue;

        result += (-l)*gradient_t;

        if(iter == ITMAX-50)
            l /= 10.0;

        if( (cv::norm(gradient_t)<1.0e-8)&&(samples!=0) ) break;
    }
    //fret = func(result);
    return result;

}

#endif //UNTPOWELL_H_INCLUDED

#ifndef UNTINTERPOLATESPLINES_H_INCLUDED
#define UNTINTERPOLATESPLINES_H_INCLUDED

#include <opencv2/core.hpp>
#include <limits>
#include <untErrors.hpp>

/**=================================================================================================
@brief	Samples to coefficients. 

@author	Aslan
@date	01/07/2010

@param	Image					The image. 
@param [in,out]	result	The result matrix of coefficientes. 
@param	SplineDegree		The spline degree. 

@return	. 
 *===============================================================================================**/
extern int SamplesToCoefficients(const cv::Mat&Image,cv::Mat&result,long SplineDegree=3);


#endif // UNTINTERPOLATESPLINES_H_INCLUDED

#ifndef UNTKERNELP_H_INCLUDED
#define UNTKERNELP_H_INCLUDED

//Standar includes
#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <ctime>

//OpenCV includes
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv4/opencv2/core.hpp>

//Includes KP
#include <untErrors.hpp>
#include <untInterpolateSplines.hpp>
#include <untInterpolate.hpp>
#include <untMatFunctions.hpp>


#define SKP1 1
#define SKP2 2

//==================================

class KernelP
{
public:
    KernelP(const unsigned long seed,const bool verbose);
    ~KernelP(void);

    //Funciones para obtener los miembros de la clase
    int 			getPixs() 		const {return _npixs;}
    int 			getBins() 		const {return _nbins;}
    int                         getA()			const {return _a;}
    int                         getB()                  const {return _b;}
    int                         getInliers()		const {return _inliers;}
    int                         getEstimatorType()	const {return _estimator_type;}
    int 			getType() 		const {return _type;}
    int                         getParameters()		const {return _parameters;}
    double 			getImax() 		const {return _max_intensity;}
    double 			getEpsilon() 		const {return _epsdiff;}
    double			getCorrect()		const {return _correct;}
    cv::Rect                    getRect() 		const {return _rect;}

    //Funciones para modificar los miembros de la clase (Solo los parametros de KPï¿½s)
    void setVerbose(const bool&verbose) {_verbose = verbose;}
    void setBins(const int nbins) {_nbins=nbins;}
    void setPixs(const int npixs) {_npixs=npixs;}
    void setType(const unsigned int type) {_type=type;}
    void setSplineDegree(const unsigned int splinedegree) {_spline_degree=splinedegree;}
    void setTransformation(const cv::Mat&transformation) {transformation.copyTo(_transformation);}
    void setRect(const cv::Rect&rect) {_rect = rect;}
    void setImax(const double Imax) {_max_intensity=Imax;}
    void setEpsDiff(const double epsdiff) {_epsdiff = epsdiff;}
    void setSigmas(const cv::Vec2d&sigmas) {_sigmam=sigmas[0]; _sigmac=sigmas[1];}
    void setSigmaM(const double sigmam) {_sigmam = sigmam;}
    void setSigmaC(const double sigmac) {_sigmac = sigmac;}
    void setDefaultValue(const double value) {_default_value = value;}
    void setCentralCoor(const cv::Vec2d&xc) {_xc=xc;}
    void setAB(const int a, const int b) {_a=a;_b=b;}


    //Funcion para especificar TODOS los parametros
    void setAllParameters(const int npixs,const int nbins,const double factor,const int type,
                          const double epsdiff,const cv::Vec2d&sigmas,const int estimator=1,
                          const long splindegree=3);

    //Esta funcion determina la constante de normalizacion
    //Dependiendo del estimador, util el SKP2
    inline virtual void setEstimatorType(const int estimator_type)
    {
      _estimator_type = estimator_type;

      switch(_estimator_type)
        {
        case 1: _const_estimator_normalize = 4.0/(4 * _npixs * _npixs); break;
        case 2: _const_estimator_normalize = 1.0/(4 * _npixs * _npixs); break;
        case 3: _const_estimator_normalize = 2.0/( (2*_npixs) * ((2*_npixs)-1) ); break;
        default: throw KP::Error("Estimator dont exist",__FUNCTION__); break;
        }

    }


    void ReadParametersfromFile(const std::string&filenames); //Lee los parametros de un archivo
    void ValidateParameters(void); //Valida los parametros
    void ClearRect(void) {_rect = cv::Rect(0,0,0,0);}


    //==============================	OPERADORES OPTIMIZACION		==============================================	//

    /**=================================================================================================
        @fn	double KernelP::operator()(const cv::Mat&Transformation)

        @brief	Evaluate the function for the especified transformation.

        @author	Aslan
        @date	29/06/2010

        @exception	Error	Thrown when bad transformation type or size.

        @return	The result of the operation.
         *===============================================================================================**/
    double operator()(const cv::Mat&transform)
    {
        if(msamples.rows==0 || msamples.cols==0)
            throw KP::Error("Inicializing requerided",__FUNCTION__);

        if( transform.cols != _parameters )
            throw KP::Error("Transformation mismatch size",__FUNCTION__);

        return KPFunction(transform,msamples);

    }

    // Estima el gradiente basado en las muestras de las intersecciones de la transformaciï¿½n
    cv::Mat EstimateGradient(std::vector<cv::Mat>&M);

    /**=================================================================================================
        @brief	Estimate the gradient of the function.

        @author	Aslan
        @date	02/07/2010

        @param	Transformation		The transformation.
        @param [in,out]	gradient	The gradient.

        @return	number of samples in traslape area.
         *===============================================================================================**/
    virtual int df(const cv::Mat&transform,cv::Mat&gradient)
    {
        std::vector<cv::Mat>vector_samples;
        int tmp_samples = takeSamplesGradient(vector_samples,transform);
        EstimateGradient(vector_samples).copyTo(gradient);
        return tmp_samples;
    }


    //==================	Funciones para el manejo de las muestras	=======================

    void takeSamples(cv::Mat&samples,const int nsamples=0); //Toma muestras de la imagen de forma uniforme
    void takeSamples(cv::Mat&samples,const cv::Mat&border_samples);
    void setSamples(const cv::Mat&samples) {msamples = samples;}



    //===================   Funciones para realizar el registro =========================================

    /**=================================================================================================
        @brief	Inicializes.

        @author	Aslan
        @date	29/06/2010

        @param	img_original	The image original.
        @param	img_transform	The image transform.
        @param	npixs				The npixs.
        @param	nbins				The nbins.
        @param	Imax				The imax.
        @param	epsdiff			The epsdiff.
        @param	sigmas			The sigmas.
        @param	spline_degree	The spline degree.
        @param	type				The type.
        @param	rect				The rectangle.
        @param	xc					The xc.
         *===============================================================================================**/
    void Inicialize(const cv::Mat&img_original,const cv::Mat&img_transform, const int npixs,const int nbins,
                    const double Imax,const double epsdiff, const cv::Vec2d&sigmas,
                    const unsigned int spline_degree,const unsigned int type,const cv::Rect&rect,
                    const cv::Vec2d&xc);

    /**=================================================================================================
        @brief	Inicializes.

        @author	Aslan
        @date	29/06/2010

        @param	img_original	The image original.
        @param	img_transform	The image transform.
         *===============================================================================================**/
    void Inicialize(const cv::Mat&img_original,const cv::Mat&img_transform);

    //==============================		FUNCIONES VIRTUALES	==============================================	//

    virtual void setNumberofParameters()																	= 0;
    virtual double KPFunction(const cv::Mat&transform,const cv::Mat&samples)					= 0;
    virtual int takeSamplesGradient(std::vector<cv::Mat>&M,const cv::Mat&transform)			= 0;
    virtual int takeSamplesT(cv::Mat&samples,const cv::Mat&transform, const int nsamples=0)         = 0;
    virtual int takeSamplesBetweenT(cv::Mat&samples,const cv::Mat&transform,const cv::Mat&transform2) = 0;

protected:
    //Parametros de KP's
    bool _verbose;
    int _nbins; //Numero de bins para aproximar la distribuciï¿½n
    int _npixs; //Nï¿½mero de pï¿½xeles de muestra.
    int _type; //Tipo de funcion de SKP
    int _spline_degree;  //Grado del spline
    int _parameters; //nï¿½mero de parametros de la transformaciï¿½n
    int _a,_b; //Si las imagenes no son del mismo tamaï¿½o
    int _inliers; //Define el nï¿½mero de pixeles que se encontraron dentro de la transformacion
    int _estimator_type; //Define el estimador usado
    double _max_intensity; //Intensidad mï¿½xima de las imagenes
    double _sigmam; //Sigmas de la marginal
    double _sigmac; //Sigma de la conjunta
    double _epsdiff; //Epsilon de diferencias centradas
    double _correct; //Numero de muestras de la interseccion
    double _default_value; //Sirve para decidir como evaluar los pixeles fuera de la transformacion
    double _const_estimator_normalize;

    //Generador de numeros aleatorios
    std::mt19937 kp_gen;

    cv::Vec2d _xc; //Coordenadas del centro de la imagen
    cv::Mat _transformation; //Matriz vector de la transformacion afin
    cv::Rect _rect; //Region de interes para el muestreo

    // Miembros internos para estimar KPï¿½s
    cv::Size sizeR,sizeT;
    cv::Mat CoeffIp; //Matriz de coeficientes de interpolacion
    cv::Mat matBins; //Matriz de bins de la imagen transformada
    cv::Mat KM; //Kernel de la distribuciï¿½n marginal, this must be matrix whit rows=1
    cv::Mat KC; //Kernel de la distribuciï¿½n conjunta
    cv::Mat msamples; //Matriz de muestras para evaluar KPFunction
    cv::Mat temp; //Matrices temporales para observacion de datos, deben eliminarse en la entrega final

    //==============================	FUNCIONES PROTEJIDAS	=====================================

    // Determina a que bin pertenece la intensidad
    inline int EstimateBin(const double&intensity,const int&nbins,const double&factor)
    {
        int result = int(intensity*(nbins-1)/factor+0.5);
        result = (result >= nbins)? (nbins-1):result;
        result = (result < 0)? 0:result;
        return result;
    }

    // Crea los Kernles de la distribuciï¿½n conjunta y la marginal
    void FillKernels(cv::Mat&km,cv::Mat&kc,const double&sigmam,const double&sigmac,const int&nbins,
                     const double factor,const bool verbose=true);

    // Retorna una matriz que contiene los bins de la distribucion propuesta
    void FillBinsMatrix(const cv::Mat&src, cv::Mat& image_bins,const int&nbins,const double&factor);

    /* Evalua SKP como sumas
                    @param[in] src: Matriz de 1 fila y 3 columnas
                    mat(0,0)=KPT
                    mat(0,1)=KPR
                    mat(0,2)=KPJ
            */
    inline double KPadd(const cv::Mat&src)
    {
        if( (src.type() != CV_64F) || (src.rows != 1) ||(src.cols!=3))
            throw KP::Error("Not matrix type or size",__FUNCTION__);
        const double *ptrmat = src.ptr<double>(0);
        double result =  _const_estimator_normalize*(ptrmat[0] + ptrmat[1] - (2.0*ptrmat[2]));
        return (result);
    }

    /* Evalua SKP como -1.0*KPJ/(KPT+KPR)
                    @param[in] mat: Matriz de 1 fila y 3 columnas
                    mat(0,0)=KPT
                    mat(0,1)=KPR
                    mat(0,2)=KPJ
            */
    inline double KPNormalized(const cv::Mat&src)
    {
        if( (src.type() != CV_64F) || (src.rows != 1) ||(src.cols!=3))
            throw KP::Error("Not matryx type or size",__FUNCTION__);
        const double *ptrmat = src.ptr<double>(0);
        double result = (ptrmat[0]+ptrmat[1] > 1.0e-3) ? ptrmat[2]/(ptrmat[0]+ptrmat[1]) : 0.0;
        return (-result);
    }

    // Evalua los kernles
    void EvalKernels(const cv::Mat&M,cv::Mat&pred);
    void EvalKernels2(const cv::Mat&M,cv::Mat&pred);
    void EvalKernels3(const cv::Mat&M,cv::Mat&pred);

};

#endif

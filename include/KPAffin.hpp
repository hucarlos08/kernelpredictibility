#ifndef KPAFFIN_H_INCLUDED
#define KPAFFIN_H_INCLUDED

#include <KernelP.hpp>
#include <untOptimization.hpp>

//Forward declaration
class KPAffin;

void TAfin(cv::Mat&mtaf, double fact);
void GenerateRandomAffin(const std::string&filename,int samples,double interval_begin,double interval_end,
								double steap_size);

/**
 * @brief RandomAffineT
 * @param angle
 * @param scaling
 * @param shearing
 * @param traslation
 * @param eng
 * @return
 */
cv::Mat RandomAffineT(const double angle[2],const double scaling[2],const double shearing[2],
                      const double traslation[2],std::mt19937&eng);

/**
 * @brief AffinT Returns transformation affin for the params
 * @param theta rotation angle in radians
 * @param alpha scaling factor in x
 * @param betha scaling factor in y
 * @param gamma shearing factor in x
 * @param delta shearing factor in y
 * @param tx traslation in x
 * @param ty traslation in y
 * @return
 */
cv::Mat AffinT (const double&theta,const double&alpha,const double&betha,
                const double&gamma,const double&delta,const double&tx,
                const double&ty);



void ApplyAffinT(const cv::Mat&src,cv::Mat&dst,const cv::Mat&taffin,const cv::Vec2d&xc=cv::Vec2d(0.0,0.0),
                const long SplineDegree=3,bool inverse=false);

/**=================================================================================================
@fn	double AffinMeanError(const cv::Size&size,const cv::Mat&taffin_real,const cv::Mat&taffin,
const cv::Vec2d=cv::Vec2d(0.0,0.0))

@brief	Affin mean error. 

@author	Aslan
@date	01/07/2010

@exception	Error	Thrown when error. 

@param	size			The size. 
@param	taffin_real	The taffin real. 
@param	taffin		The taffin. 
@param	cv::Vec2d	The cv vector 2d. 

@return	. 
 *===============================================================================================**/
double AffinMeanError(const cv::Size&size,const cv::Mat&taffin_real,const cv::Mat&taffin,
							const cv::Vec2d=cv::Vec2d(0.0,0.0));


double BorderError(const cv::Mat&borders1,const cv::Mat&samples,const cv::Mat&border2,
                     const cv::Mat&taffin,const cv::Vec2d&xc,int a, int b,int&count);

//============================================================================================================
void RegisterAffine(KPAffin&kp,cv::Mat&taffin,const double lambda=0.1,const int maxiters=500,const METHODS method=DGE);

cv::Mat RegisterAffine(const cv::Mat&original,const cv::Mat&transform,const cv::Vec2d&sigmas,const double&factor,
                        const int npixs,const double&lambda,const int&maxiters,const int&pyr_levels);


//============================================================================================================
class KPAffin : public KernelP
{
	public:
		KPAffin(const unsigned long seed,const bool verbose):KernelP(seed,verbose){};
		~KPAffin(void){};

		inline cv::Mat GetInicializeT(void)
		{
			cv::Mat result = cv::Mat::eye(2,3,CV_64F);
			return result;
		};

		inline void setNumberofParameters(){_parameters=6;};

		/**=================================================================================================
		@fn	double KPAffin::KPFunction(const cv::Mat&taffin,const cv::Mat&samples)
		
		@brief	Kp function. 
		
		@author	Aslan
		@date	29/06/2010
		
		@param	taffin	The taffin. 
		@param	samples	The samples. 
		
		@return	. 
		 *===============================================================================================**/
		double KPFunction(const cv::Mat&taffin,const cv::Mat&samples);

		// Toma muestras para estimar el gradiente
		int takeSamplesGradient(std::vector<cv::Mat>&M,const cv::Mat&_Taffin);
		int takeSamplesT(cv::Mat&samples,const cv::Mat&taffin, const int nsamples=0); 
		int takeSamplesBetweenT(cv::Mat&samples,const cv::Mat&taffin1,const cv::Mat&taffin2);

		//Toma muestras unicamente sobre los bordes
		void takeSamplesTaffin(cv::Mat&samples,const cv::Mat&border_samples,const cv::Mat&taffin,bool verbose=false);
		

	protected:
            inline void CordAffin(const int orow,const int ocol,const cv::Mat&taffin,double&row, double&col)
            {
                if( (taffin.rows!=1) || (taffin.cols!=6) || (taffin.type()!=CV_64F) )
                        throw KP::Error("Not type or size in TAFFIN",__FUNCTION__);

                //Apuntador a la transformacion affin
                const double *ptr_affin = taffin.ptr<double>(0);

                //Transforma a coordenadas de la imagen de referencia
                double r = orow + _a;
                double c = ocol + _b;

                double rxc = (r - _xc[0]);
                double cxc = (c - _xc[1]);

                        //Transforma las coordenadas del pixel muestreado
                row = rxc*ptr_affin[0]+cxc*ptr_affin[1]+ptr_affin[2] + _xc[0] ;
                col = rxc*ptr_affin[3]+cxc*ptr_affin[4]+ptr_affin[5] + _xc[1];
            };
};

#endif //KPAFFIN_H_INCLUDED
